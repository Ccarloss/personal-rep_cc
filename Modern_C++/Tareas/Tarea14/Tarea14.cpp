// Carlos Andres Cordero Retana

// Tarea 14: Inserting and removing elements from containers


#include <iostream>
#include <deque>
#include <vector>




/*
232. Exercise:
232.1. **Compare back_inserter, front_inserter and inserter. Why are
the first two inappropriate for associative containers and unordered
containers?
 */

namespace Exercise_232_1{


    /* std::front_inserter:  es una funcion template que devuelve un iterador de tipo:
	std::front_insert_iterator .  Este iterador llama a la funcion miembro push_front()
    del contenedor cada vez que se le asigna un contenedor */

    /* std::back_inserter:  es una funcion template que devuelve un iterador de tipo:
    std::back_insert_iterator .  Este iterador llama a la funcion miembro push_back()
    del contenedor cada vez que se le asigna un contenedor */



	void front_back_inserter()
{
        std::vector v{ 1, 2, 3, 4, 5 };
        std::deque front{55,77,15};
        std::deque back{ 55,77,15 };

        //***cada vez que std::copy intenta insertar un elemento en d, en lugar de agregarlo
        //al final del contenedor, lo inserta al principio (si es con front_inserter)
        // o al final (si es con back_inserter)

        std::copy(v.begin(), v.end(), std::front_inserter(front));
        std::copy(v.begin(), v.end(), std::back_inserter(back));
        for (int n : front)
            std::cout << n << ' ';
        std::cout << '\n';
        for (int i : back)
            std::cout << i << ' ';
        std::cout << '\n'; 
}
    //  ***Dado que los associative Containers (set/multiset, map/multimap) y los unordered containers
    // (Unordered Set/Multiset, Unordered Map/Multimap) no contienen la funcion miembro push_back
    // o push_front, no es apropiado usar los templates front_inserter o back_inserter, ya que
    // estos dependen de dichas funciones.





	/* std::inserter:  es una funcion template que recibe un contenedor C y un iterador i de C.
     Esta funcion devuelve un iterador de tipo: std::insert_iterator. En cada asignacion de este iterador,
     se inserta elemento en el contenedor C en la posicion brindada por el iterador i*/
    void inserter()
	{
        std::vector d{ 100, 200, 300 };
        std::vector v{ 1, 2, 3, 4, 5 };

        // Colocamos todo el vector d a partir de la segunda posicion del vector v 
        // El punto de inserci�n avanza porque cada std::insert_iterator::operator= actualiza el iterador objetivo.
        std::copy(d.begin(), d.end(), std::inserter(v, std::next(v.begin())));

        for (int n : v)
            std::cout << n << ' ';
        std::cout << '\n';
	}


   void exercise231_1()
   {
       front_back_inserter();
       inserter();
   }
}





/*
235. Exercise:
235.1. **Removing elements from sequence containers is not as
intuitive as one would wish.


Explain this example:

std::vector<int> vec{ 1,2,3,4,5,6,7,8,9,10 };
auto newEnd = std::remove(vec.begin(),vec.end(), 6);
auto endIt = vec.erase(newEnd, vec.end());
 */


namespace Exercise_235_1
{
   void exercise_235_1()
   {
       std::vector vec{ 1,2,3,4,5,6,7,8,9,10 };


       // Removiendo el valor 6 de manera l�gica (sin cambiar el tama�o f�sico del vector)
   	   // newEnd es un iterador que apunta al nuevo final l�gico del vector, justo despu�s del
       // �ltimo elemento que no es 6.
       auto newEnd = std::remove(vec.begin(), vec.end(), 6);

       for (int i : vec)
           std::cout << i << ' ';
       std::cout << '\n';

       // Eliminamos f�sicamente todos los elementos en el rango [newEnd, vec.end()),
		// que incluye todos los elementos que han sido "removidos l�gicamente" por std::remove.
       auto endIt = vec.erase(newEnd, vec.end());


       for (int n : vec)
           std::cout << n << ' ';
       std::cout << '\n';
   }
}




int main()
{
    std::cout << "----------------------Exercise232.1----------------------------------\n\n";
    Exercise_232_1::exercise231_1();
    std::cout << "\n\n----------------------Exercise235.1----------------------------------\n\n";
    Exercise_235_1::exercise_235_1();
}