// Carlos Andres Cordero Retana

// Tarea 13: STL containers

#include <iostream>
#include <array>
#include <vector>
#include <deque>
#include <list>
#include <forward_list>
#include <set>
#include <unordered_set>
#include <map>
#include <unordered_map>
#include <string>




/*
209.6. **(CP9.6) What is wrong with the following program? How
might you correct it?
std::list<int> lst;
std::list<int>::iterator iter1 = lst.begin(), iter2 = lst.end();
while(iter1 < iter2)
{
///
}
 */

namespace Exercise209_6
{
	void Problema()
	{
		std::list<int> lst = { 3, 5,8 };
		std::list<int>::iterator iter1 = lst.begin(), iter3 = lst.end(), iter2 = ++lst.begin();

		// Imprimir direcciones de memoria
		std::cout << "iter1 en listas : " << &(*iter1) << std::endl;
		std::cout << "iter2 en listas: " << &(*iter2) << std::endl;


		//while (iter1 < iter3)
		//{
		//	///
		//}

		// El problema est� en utilizar el operador < entre dos iteradores del contenedor tipo list.
		// Sabemos que en una lista, los elementos no est�n almacenados de forma contigua en memoria
		// (como en los vectores). Por lo tanto, no tiene sentido comparar dos direcciones de memoria
		// arbitrarias.
	}

	void Solucion()
	{
		// Por ende, la solucion est� en usar vectores! Ah� si tiene sentido comparar iteradores mediante <
		std::vector vec = { 3, 5,8 };
		std::vector<int>::iterator itr1 = vec.begin(), itr3 = vec.end(), itr2 = ++vec.begin();

	
		std:: cout<< "\n\nLos elementos en los vectores se almacenan de forma contigua en memoria!\n\n";
		std::cout << "iter1 en vectores: " << &(*itr1) << std::endl;
		std::cout << "iter2 en vectores: " << &(*itr2) << std::endl;
		std::cout << "\nvec = ";
		while (itr1 < itr3)
		{
			std::cout << *itr1 << ", ";
			++itr1;
		}
	}

	void Exercise209_6()
	{
		Problema();
		Solucion();
	}
	
}












/*211.2. ***Briefly describe the underlying data structure used by each
type. [Hint: see slide 377]*/

namespace Exercise211_2
{

	//____________________________ Sequence Containers_______________________________//

	/* Colecciones ordenadas, donde cada elemento tiene una posici�n espec�fica que depende
	del momento y lugar de inserci�n, pero es independiente de su valor. */


	/* 1) ARRAYS: */

	void arrays()
	{
		// Es un contenedor de tama�o fijo. Se suele especificar el type y el tama�o.

		std::array<double, 3> a1 = { 1, 2.5, 38 };

		// No obstante, el compilador puede deducir 
		std::array a2 = { 11, 2, 3 };

		// Experiment(s) using some members functions from arrays:

		a1.at(0) = 44;

		std::cout << "array a1 size = " << a1.size() << '\n';
		std::cout << "array a1[0] = " << a1[0] << '\n';
		std::cout << "Last element of array a1 = " << a1.back() << '\n';

	}


	/* 2) VECTOR: */

	void vectors()
	{
		// Es un contenedor de tama�o variable hacia la derecha Se suele especificar el type y el tama�o.
		// Es el que mas se usa. Los elementos se almacenan de forma contigua en memoria.

		std::vector<int> v1 = { 82, 4, 5, 9 };  // no es necesario especificar el int en este caso!


		// Experiment(s) using some members functions from arrays:


		v1.push_back(6); // se inserta al final
		v1.push_back(18);

		
		v1[2] = -1;

		auto itr1 = v1.begin() + 1; //iterador que apunta al segundo elemento

		v1.insert(itr1, 44);


		for (int n : v1)
			std::cout << n << ' ';
		std::cout << '\n';
	}


	/* 3) DOUBLE ENDED QUEUE: */

	void Deque()
	{
		// Es un contenedor de tama�o variable tanto por la derecha como por la izquierda.
		// Es decir, puede crecer hacia la izquierda o hacia la derecha.

		std::deque<int> dq1 = { 1, 2, 3 };

		// Experiment(s) using some members functions from deque:

		dq1.push_front(0); // Insertar al principio
		dq1.push_back(4);  // Insertar al final

		std::cout << "deque dq1 = ";
		for (auto elem : dq1) {
			std::cout << elem << ", ";
		}
		std::cout << std::endl;

	}


	/* 4) LISTA DOBLEMENTE ENLAZADA */

	void lista_dobl()
	{
		// Es una lista lineal en la que cada nodo tiene dos enlaces, uno al nodo siguiente, y
		// otro al anterior.


		// Experiment(s) using some members functions from list:

		std::list<char> coll; // list container for character elements

		// append elements from 'a' to 'z'
		for (char c = 'a'; c <= 'z'; ++c) {
			coll.push_back(c);
		}
		// print all elements:
		// - use range-based for loop
		for (auto elem : coll) {
			std::cout << elem << ' ';
		}
		std::cout << std::endl;

	}


	/* 5) LISTA ENLAZADA HACIA DELANTE */

	void Forward_list()
	{
		//El forward_list mantiene internamente solo un enlace al siguiente elemento, mientras que
		//el list mantiene dos enlaces por elemento: uno que apunta al siguiente elemento y otro al
		//elemento precedente, lo que permite una iteraci�n eficiente en ambas direcciones, pero
		//consume almacenamiento adicional por elemento y con un ligero aumento en el tiempo de
		//inserci�n y eliminaci�n de elementos. Los objetos forward_list son por lo tanto, m�s
		//eficientes que los objetos list, aunque solo pueden ser iterados hacia adelante.


		std::forward_list<int> flist = { 10, 20, 30, 40, 50 };


		flist.push_front(60);


		std::cout << "Hacemos push_front (60): ";
		for (int& c : flist)
			std::cout << c << " ";
		std::cout << std::endl;


		// Inserta el valor 70 al principio
		flist.emplace_front(70);

		// Mostrando la lista enlazada hacia adelante
		std::cout << " Hacemos emplace_front(70): ";
		for (int& c : flist)
			std::cout << c << " ";
		std::cout << std::endl;


		// Elimina el valor 70
		flist.pop_front();

		std::cout << "Hacemos pop_front: ";
		for (int& c : flist)
			std::cout << c << " ";
		std::cout << std::endl;

	}


	//____________________________Associative Containers_______________________________//
	/*
	 Colecciones ordenadas en las que la posici�n de un elemento depende de su valor
	 (o key si es un par key/valor).
	 */

	/* 1) SET/MULTISET */

	void Multiset_o_Set()
	{
		// Un set es un ordered container el cual almacena keys �nicos de forma ordenada.
		// Se implementa como una balanced tree structure que garantiza el orden de las llaves.
		// El multiset es lo mismo que el set, solo que este s� acepta valores repetidos.
		// Por default, el orden es de forma ascendiente.

		std::set<int> set1{ 5,4,3,2,1 }; // No es necesario poner int

		// Experiment(s) using some members functions from sets/multisets:

		set1.insert(2);
		set1.insert(-5);

		std::cout << "set1 = ";
		for(auto elem : set1)
		{std::cout << elem << ", ";}
		std::cout << std::endl;

		std::multiset multi_set1{ 5,4,3,2,1 };
		multi_set1.insert(2);
		multi_set1.insert(-5);

		std::cout << "multi_set1 = ";
		for (auto elem : multi_set1)
		{std::cout << elem << ", ";}
		std::cout << std::endl;


		auto itr_found = multi_set1.find(4);
		if(itr_found != multi_set1.end())
		{
			std::cout << "\nExiste y conocemos la posicion del elemento 4 dentro del multiset" << std::endl;
			multi_set1.erase(itr_found);
			std::cout << "\nEliminando elemento = 4.." << std::endl;
		}

		std::cout << "multi_set1 = ";
		for (auto elem : multi_set1)
		{std::cout << elem << ", ";
		}std::cout << std::endl;

	}


	/* 2) MAP/MULTIMAP */

	void Multimap_o_map()
	{
		//En un mapa cada elemento tiene un valor de clave y un valor asignado.
		//No puede haber dos valores asignados con las mismas claves.
		
		//Multimap es similar a un map, con la adici�n de que m�ltiples elementos
		// pueden tener las mismas claves. Adem�s, NO es necesario que el par key-value
		// y el valor asignado sean �nicos en este caso. En un multimap se mantienen
		// todas las claves en orden siempre, como se muestra a continuacion.

		 // create a multimap
		std::multimap<int, std::string> my_multimap = {
			{1, "Un"},{1, "One"},{2, "Two"},
			{2, "Dos"},{1, "Uno"},{2, "Deux"}
		};

		std::cout << "Notemos el orden en el multimap" << std::endl;

		for (const auto& key_value : my_multimap) {
			int key = key_value.first;
			std::string value = key_value.second;

			std::cout << key << " - " << value << std::endl;
		}

	}



	//____________________________Unorderded Containers_______________________________//
	/*
	  La posici�n de un elemento no importa. La �nica pregunta importante es si un
	  elemento espec�fico est� en dicha colecci�n.
	 */
	
	 /* 1) UNORDERED SET/MULTISET */

	void Unordered_Multiset_o_Set()
	{
		// En un unordered set las keys no est�n en orden !!
		// Se implementa utilizando una tabla hash donde las
		// keys se convierten en �ndices de una tabla hash
		// de manera que la inserci�n siempre es aleatoria.

		std::unordered_set myset = { 3, 1, 4, 1, 5 }; // Almacena valores �nicos de forma no ordenada

		// Experiment(s) using some members functions from unordered set/multiset:

		myset.insert(2);
		myset.insert(6);

		std::cout << "Unordered set elements: ";
		for (auto elem : myset) {
			std::cout << elem << ", ";
		}
		std::cout << std::endl;
	}



	/* 2) UNORDERED MAP/MULTIMAP */

	void Unordered_Multimap_o_map()
	{
		// Es un como un multimapa, solo que no almacena los elementos
		// key-value de forma ordenada

		std::unordered_map<int, std::string> mymap;

		mymap.insert(std::make_pair(2, "two"));
		mymap.insert(std::make_pair(1, "one"));
		mymap.insert(std::make_pair(2, "two duplicate")); // Puede tener keys duplicadas

		std::cout << "Unordered map elements: ";
		for (auto elem : mymap) {
			std::cout << "{" << elem.first << ": " << elem.second << "}, ";
		}
		std::cout << std::endl;


	}

	void Exercise211_2()
	{
		std::cout << "__________________________________________Working with Arrays: " << "\n\n";
		arrays();
		std::cout << "\n\n__________________________________________Working with vectors: " << "\n\n";
		vectors();
		std::cout << "\n\n__________________________________________Working with Deques: " << "\n\n";
		Deque();
		std::cout << "\n\n__________________________________________Working with Lists " << "\n\n";
		lista_dobl();
		std::cout << "\n\n__________________________________________Working with forward list: " << "\n\n";
		Forward_list();
		std::cout << "\n\n__________________________________________Working with sets/multisets: " << "\n\n";
		Multiset_o_Set();
		std::cout << "\n\n__________________________________________Working with Map/multimap: " << "\n\n";
		Multimap_o_map();
		std::cout << "\n\n__________________________________________Working with unordered set/multiset: " << "\n\n";
		Unordered_Multiset_o_Set();
		std::cout << "\n\n__________________________________________Working with unordered map/multimap: " << "\n\n";
		Unordered_Multimap_o_map();
	}

}









int main()

{
	std::cout << "----------------------Exercise209_6----------------------------------\n\n";
	Exercise209_6::Exercise209_6();

	std::cout << "\n\n----------------------Exercise211_2----------------------------------\n\n";
	Exercise211_2::Exercise211_2();

}
