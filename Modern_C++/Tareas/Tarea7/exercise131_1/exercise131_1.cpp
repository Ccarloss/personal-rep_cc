/*
* Carlos Andres Cordero Retana

131.1. ***The same raw pointer �this� cannot be used to initialize two
unique_ptr; why? The same for shared_ptr<>

Tendr�amos 2 due�os del mismo puntero this. Cuando el programa se 
sale del ambito de alguno de los punteros due�o, el destructor de este se llamara, 
lo cual destruir� a this. El problema es que el otro puntero due�o tambi�n llamara al 
destructor a pesar de que this ya fue destruido. Esto provocar� una corrupcion del heap.

R/ 

how do you export a shared_ptr from an object of a class? Start with the following code:
(warning: it contains conceptual errors in the use of shared_ptr, find them!!) In the code
below, correct getSharedPtr().


*/

#include <iostream>


class Chunk : public std::enable_shared_from_this<Chunk>
{
public:
	[[nodiscard]] Chunk() = default;
	~Chunk()
	{
		std::cout << "Destructor called\n";
	}

	// El problema con esta funcion es, que cada vez que se llame, se creara un nuevo
	// administrador con el contador en 0. Esto es un grave problema, solo queremos un solo administrador!
	// Si tenemos varios administradores con el contador en 1, cuando estemos fuera del ambito de alguno de estos,
	// se llamara el destructor del mismo y provocara que el contador este en 0, eliminando el objeto por completo,
	// aun cuando existen otros administradores con el contador en 1.
	//std::shared_ptr<Chunk> getSharedPtr() { return std::shared_ptr<Chunk>(this);}


	// El fix ser�a retornando la funcion shared_from_this, ya que esta garantiza que solo tengamos un solo administrador!
	std::shared_ptr<Chunk> getSharedPtr() {
		return shared_from_this();
	}
	
	//Esta funcion crea un objeto tipo de chunk de forma mas segura (administra la memoria dinamica)
	static std::shared_ptr<Chunk> createChunk() {
		return
			std::make_shared<Chunk>();
	}
};
void usingSmartPtrsWithArrays() {
	using namespace std;
	shared_ptr<Chunk[]> p{ new Chunk[3] };

	shared_ptr<Chunk[]> pp{ new Chunk[3], [](Chunk* p) {
	 delete[] p; } };
	shared_ptr<Chunk> pChunk{ new Chunk{} };


	shared_ptr<Chunk> pc = Chunk::createChunk();
	// El contador se incrementa en 2, debido a que existen dos punteros que son constituyen un solo administrador
	shared_ptr<Chunk> sp = pc->getSharedPtr(); // ERROR
}


int main() {
	usingSmartPtrsWithArrays();

}