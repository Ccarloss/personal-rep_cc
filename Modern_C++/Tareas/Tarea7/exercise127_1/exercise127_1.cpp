/*
Carlos Cordero Retana

127.1. ***Repeat the previous exercise, but with a class Number < T >
where T can be any numeric type. Try adding % to Number and see
what happens when you try to use % for Number < double > and
Number < int >. Is there a solution using type_traits? [Hint: use
requires�- see next exercise]
*/

#include <iostream>
#include <type_traits>
using namespace std;

template<typename T> requires (std::is_arithmetic<T>::value)
class Number
{
public:
	T t;

	[[nodiscard]] explicit Number(const T& t)
		: t(t)
	{
	}

	// Esta funcion requiere:
	// 1) Acepte dos objetos de la clase Number con diferentes tipos subyacientes (int y double en este caso)
	// 2) Los objetos con tipos subyacientes que sean floats, los debe convertir a int, ya que el operador %
	// en C++ no esta definido para operar con floats
	
	template<typename U>
	Number<int> operator%(const Number<U>& other)
	{
		return Number<int>{ static_cast<int>(t) % static_cast<int>(other.t) };
	}

};




int main() {

	Number<int> i1(15);
	Number<double> i2(4);


	auto result = i1 % i2;  
	
	
	cout << "Result de " << i1.t <<"%" << i2.t << "= " << result.t << endl;

	return 0;
}