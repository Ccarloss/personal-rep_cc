/* 
Carlos Andres Cordero Retana


134.1. **Why is there a leak in function unique_ptrTesting() in slide
244? What is release() and what is reset()? Show usage of both with
care.

*/

/*

R/
Cuando se llama a release() en un std::unique_ptr, el puntero administrado 
se libera de la propiedad del std::unique_ptr, pero no se libera la memoria 
asociada con ese puntero. Esto significa que la responsabilidad de liberar 
la memoria recae en el c�digo que ahora posee el puntero. En este caso, como
no hay ning�n otro c�digo que libere la memoria apuntada por xx, se produce 
un memory leak.

Despu�s de llamar a p.release(), p ya no posee ning�n recurso. La llamada 
a reset() en este punto no libera ning�n recurso adicional, ya que p ya no 
tiene ning�n recurso asociado.


*/

#include <iostream>

using namespace std;


//void unique_ptrTesting() {
//	unique_ptr<Foo> p{ new Foo {5, 8.8f} };
//	auto x = *p;
//	cout << Foo(p->innerTup) << endl; // temporary object created, used in this line, and destroyed!
//		auto xx = p.release(); // p == nullptr, we have a leak!!
//	cout << xx << flush;
//	p.reset(); // too late!!, does no good at all!!
//}


int main() {


	// Uso del reset

	std::unique_ptr<int> up; 

	up.reset(new int);       // takes ownership of pointer
	*up = 5;
	std::cout << *up << '\n';

	up.reset(new int);       // deletes managed object, acquires new pointer
	*up = 10;
	std::cout << *up << '\n';

	up.reset();               // deletes managed object


	// Uso del release

	std::unique_ptr<int> auto_pointer(new int);
	int* manual_pointer;

	*auto_pointer = 10;

	manual_pointer = auto_pointer.release();  // auto_pointer is empty
	

	std::cout << "manual_pointer points to " << *manual_pointer << '\n';

	delete manual_pointer;

	return 0;
}