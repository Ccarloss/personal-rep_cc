// Carlos Andres Cordero Retana

// Tarea 8:  Tuplas y Pares

// Un par es una estruc

#include <cassert>
#include <iostream>
#include <tuple>
#include <memory>
#include <string>

/*147.1. **If you wanted to store references to objects or fundamental
types in a pair, you might be tempted to use & somewhere. Explain
how it is done correctly by avoiding copying. (Hint: slide 269)*/

namespace Excercise147

{
	void Excercise147_1() {

		int a = 1;
		int b = 2;
		auto par1 = std::make_pair(std::ref(a), std::ref(b));
		++par1.first;
		++par1.second;
		std::cout << "a = " << a << std::endl;
		std::cout << "b = " << b << std::endl;
		
	}

}

namespace Excercise149

{	//149.1. *Is std::make_pair a template function? How can we know?
	void exercise149_1()
	{
		// Es una variadic template porque acepta cualquier tipo
		auto par1 = std::make_pair(22, "hi");

		auto par2 = std::make_pair(4.76f, 27);
	}

	//149.2. *Explain how std::tie works and what is std::ignore (slide 271)

	//std::tie se utiliza para desempaquetar los valores de una tupla en variables individuales
	//std::ignore se utiliza como marcador cuando se desean ignorar valores al desempaquetar 
	// una tupla usando std::tie. En este caso, le indicamos que
	void exercise149_2() {

		auto par = std::make_pair('B', 'A');
		char ch1;
		//No nos importa el primer valor del par(p.first), y 
		//s�lo queremos asignar el segundo valor(p.second) a la variable c
		std::tie(std::ignore, ch1) = par;

		std::cout << "El valor de ch1 es " << ch1 << std::endl;
	}

	//149.3. *Create a tuple by using std::tie

	void exercise149_3() {

		int val1{ 11 };
		char val2{ 'H' };
		float val3{ 3.14 };

		auto tupla_tie = std::tie(val1, val2, val3);

		std::cout << "tupla_tie = (" << std::get<0>(tupla_tie) << "," << std::get<1>(tupla_tie) << "," << std::get<2>(tupla_tie) << ")"<< std::endl;
		
	}
	// 149.4. *Explain how we can extract values from a tuple using reference
	//wrappers(see Slide 271)

	void exercise149_4() {

		int x;

		float y;

		std::string z;

		std::tuple<int, float, std::string> values(22, 5.8, "Buenas");

		// Gracias a ref, logramos modificar los valores de  x, y y z 
		std::make_tuple(std::ref(x), std::ref(y), std::ref(z)) = values;


		std::cout << "El valor de (x,y,z) =" << x << "," << y << "," << z << std::endl;



		// ****Usando structured bindings****

		std::tuple<int, float, std::string> values2(16, 5.2, "Malas");

		// No ocupamos declarar las variables a, b y c!
		auto& [a, b, c] = values2;

		a++;

		// Gracias al & modificamos el primer elemento de la tupla values2 al hacer  a++ !
		std::cout << "El valor de (a,b,c) =" << std::get<0>(values2) << "," << b << "," << c << std::endl;
	}
}






//151. Exercise:
//151.1. * **Explain how tuples can be �printed� to an ostream.

namespace exercise151
{
	
		template<int IDX, int MAX, typename...Args>
		struct print_tuple {

			// Este metodo recibe un objeto de la clase ostream que me permite imprimir en consola
			// Ademas, recibe una tupla, cuyos element types son desempacados de Args
			static void print(std::ostream& strm, const std::tuple<Args...>& t) { 

			// Le pasamos data al objeto strm mediante el operador << para que la pueda imprimir en consola
			// Primero le pasamos el valor del elemento de la tupla apuntado por IDX
			// Luego, le pasamos para que imprima "," si no hemos llegado al ultimo elemento de la tupla
			// Si llegamos al ultimo elemento , imprima nada ""
				strm << std::get<IDX>(t) << (IDX + 1 == MAX ? "" : ",");

			// De forma recursiva, llamamos otra vez a print 
			// Solo que ahora la clase template se configura con:
			// -> indice actual +1  
			// -> los mismos tipos almacenados en Args
			// print recibe los objetos 2 originales
				print_tuple<IDX + 1, MAX, Args...>::print(strm, t);         
			}                                                               
		}; 

		// Caso base de la recursion
		template<int MAX, typename...Args>
		struct print_tuple<MAX, MAX, Args...> {
			static void print(std::ostream& strm, const std::tuple<Args...>& t) {
			}
		};





		// Sobrecarga del operador de inserci�n para imprimir la tupla
		// output operator for tuples:
		template<typename...Args>
		std::ostream& operator<<(std::ostream& strm, const std::tuple<Args...>& t) {
			strm << "[";
			print_tuple<0, sizeof...(Args), Args...>::print(strm, t);
			return strm << "]\n";
		}
	
}



int main() {

	std::cout << "Excercise147\n\n" << std::endl;
	Excercise147::Excercise147_1();

	std::cout << "\n\nExcercise149\n\n" << std::endl;
	Excercise149::exercise149_2();
	Excercise149::exercise149_3();
	Excercise149::exercise149_4();


	std::cout << "\n\nExcercise151\n\n" << std::endl;
	using namespace exercise151;
	// Ejemplo de uso
	auto t = std::make_tuple(1, 2.0, "hello"); // Crea una tupla
	std::cout << t << std::endl; // Imprime la tupla en std::cout
	
	
	return 0;
	


	
}
