/* 
120.1. **C++ 11 brings the template initializer_list<T>; it allows us to
construct an object by giving a list of values separated by commas and
held inside curly braces. This is of great value to initialize objects like
containers. Of course, every element must be of the same type for
the compiler to deduce T. Give an example of a vector initialized with
an initializer_list<T>.
*/
#include <iostream>

// Creamos la clase template para que el usuario pueda crear el arreglo de cualquier tipo de dato
template<typename T>
class vector {

public:

	int size;

	T* elements;

public:

	vector(std::initializer_list<T> listita) { // Inicializamos los atributos del constructor!!

		size = listita.size();

		elements = new T[size];  // elements apunta al primer elemento del array de tipo T creado en el heap

		std::copy(listita.begin(), listita.end(), elements);
	}



	~vector() 
	{
		if (elements != nullptr) {  // verifcamos que elements no sea nulo
			delete[] elements;
		}
	}


	void print() const {
		for (int i = 0; i < size; ++i) {
			std::cout << elements[i] << " ";
		}
		std::cout << std::endl;
	}

};



int main() 
{
	vector<int> intvector{ 2, 6, 8, 32, 55 };

	vector<std::string> stringvector{"Hola", "Hi", "hallo"};

	vector<float> floatvector{ 2.8, 6.2, 8.9, 32.88, 55.11 };


	std::cout << "Elementos del vector intvector: ";
	intvector.print();

	std::cout << "Elementos del vector stringvector: ";
	stringvector.print();

	std::cout << "Elementos del vector floatvector: ";
	floatvector.print();


}