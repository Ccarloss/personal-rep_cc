/*
Carlos Andres Cordero Retana

119.1. ***Explain the difference between lvalues and rvalues and how 
tthey interact with moving objects (see 225-227)*/



// Rvalues:

// - Son valores temporales que no tienen una direcci�n de memoria asociada

// - Se utilizan para realizar c�lculos o como argumentos en funciones

// - Tienden a ser objetos con vida �til corta, como valores devueltos por funciones temporales por ejemplo.

// Lvalues: 

//- Los lvalues son objetos que tienen una direcci�n de memoria asociada.

//- Son expresiones a las que se puede asignar un valor.

//- Los lvalues tienden a ser objetos con vida �til prolongada



/* */

#include <iostream>
#include <vector>

int getRvalue() {
    return 10;
}

int& getLvalue() {
    int lvalue = 5;
    return lvalue;   // retornamos una referencia al lvalue 
}


int main() {
    
    int lvalue = getRvalue();  // Estamos moviendo el valor temporal a un lvalue. El rvalue se destruye al final de la linea!

    std::cout << "el valor de lvalue que le paso el rvalue fue de: " << lvalue << std::endl;

    lvalue = getLvalue();  // Podemos igualar lvalue = lvalue (estamos copiando)
    
    std::cout << "el valor de lvalue copiado de otro lvalue fue de: " << lvalue << std::endl;

    getLvalue() = 8;   // Lo que devuelve getLvalue es un lvalue, por eso el compilador no se queja (pero no tiene sentido)

    return 0;
}