/*
Carlos Andr�s Cordero Retana

116.1. ***What is a consteval function and why is it called an 
immediate function? (See slides 205-210) What is a constinit variable 
and how does it solve the static initialization problem? (see slides
211-215) Explain another way to solve the static initialization problem
(hint: local statics are lazily initialized)

*/



// R: Una funcion consteval es aquella que se ejecuta SOLO en tiempo de compilaci�n.
// Es decir, si no se cumplen con los requisitos que ella pide, ocurrir� un error
// de compilaci�n.






#include <iostream>
#include <array>

// Esta funcion se ejecuta en tiempo de compilaci�n pero tambi�n en tiempo de ejecuci�n, 
// dependiendo de que le pasemos: una constante o una variable.
constexpr int fibonacci(int n)
{
	return n < 2 ? 1 : fibonacci(n - 2) + fibonacci((n - 1));
}

// Esta funcion SIEMPRE se ejecuta en compilaci�n. 
consteval int sqr(int n)
{
	// Solo se pueden invocar constexpr functions 
	int s = fibonacci(n);
	return n * n;
}


// Una variable constinit es aquella que se inicializa en tiempo de compilacion.
// Tiene que ser de tipo static (pertenzca a una translation unit) , global o que 
// pertenezca a un namespace.

// Gracias a C++20, al agregar constinit a una variable est�tica, se garantiza que la
//  variable se inicialice en tiempo de compilaci�n de manera constante, lo que evita 
// el problema de inicializaci�n est�tica parcialmente din�mica.
static constinit int c = 8;


// Las variables est�ticas locales se inicializan de forma "lazily", lo que significa
//  que se inicializan la primera vez que son llamadas. Por ejemplo, la primera vez
// que se llama, se inicializa con x =1. Esta es otra forma de c�mo resolver el static 
// initialization problem.

void func() {
	// Variable est�tica local inicializada de forma perezosa
	static int l = 1;

	std::cout << "l: " << l << std::endl;

	// Actualiza el valor de x para la pr�xima llamada a la funci�n
	l++;
}




int main() {


	constexpr int x = 5;

	constexpr int y = sqr(2);   //= 2*2

	// Podemos inicializar arrays en compilacion!!
	std::array<int, y>arry1;


	int y2 = sqr(2); // = 2*2

	// No podemos hacerlo con una variable normal , se queja el compilador
	//std::array<int, y2>arry2;

	func(); // l: 1
	func(); // l: 2
	func(); // l: 3


}
