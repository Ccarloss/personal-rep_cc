// Slide 103 contains code that displays integers but due to mixing signed/unsigned integers, the loop is endless. Try to cast static_cast<int>(begIdx).
// Run this code and test to confirm it works as it is; then remove the cast and rerun. This time the loop never ends.
#include "exercise79_1.h"

using namespace std;


void mixin_signed_unsigned()
{
    string::size_type begIdx = 0;
	// Con el casteo se logra comparar 2 enteros de forma correcta, para evitar el loop
    for (int i = 3; i >= (begIdx); --i) 
    {
        cout << i << "\n"<< flush;
    }
    cout << endl;
}

int main()
{
    mixin_signed_unsigned();
    return 0;
}


