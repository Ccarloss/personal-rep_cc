#pragma once
#include <ostream>


struct Foo {
	// NOTE: order of initialization is PARAMOUNT!!
	explicit Foo(std::tuple<int, float>& tup);

	Foo(int a, float b);
	~Foo();

private:
	int n1;
	float n2;
	// Al colocarlo al final , se inicializa primero n1 y n2, lo cual genera
	// la excepcion, pues n1 y n2 dependen de innerTup
	std::tuple<int, float> innerTup; 
	friend std::ostream& operator<<(std::ostream& o, const Foo& f);
};

void useFoo();