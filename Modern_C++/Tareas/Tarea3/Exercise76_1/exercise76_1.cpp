/*
	Carlos Andrés Cordero Retana

	***Exercise 76.1 Change struct Foo in slide 100 so that innerTup is placed last
	in the class. Run and test this class. What broke the code? Why?
*/


#include "exercise76_1.h"

#include <iostream>

using namespace std;


Foo::Foo(std::tuple<int, float>& tup) : innerTup{ tup }, n1{ get<0>(innerTup) }, n2{ get<1>(innerTup) }
{}

Foo::Foo(int a, float b) : innerTup{ tuple<int,float>(a,b) }, n1{ get<0>(innerTup) }, n2{ get<1>(innerTup) }
{}

Foo::~Foo()
{
	cout << "~Foo ending" << endl;
}

ostream& operator<<(ostream& o, const Foo& f)
{     // New Feature: function defined here!
	o << f.n1 << " " << f.n2 << endl;
	return  o;
}

void useFoo()
{
	tuple tpl{ 5, 7.8f }; // type deduction in C++ 17 or 20
	Foo foo{ tpl };
	cout << foo << endl;
}