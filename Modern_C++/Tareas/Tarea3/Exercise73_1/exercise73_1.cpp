/*
Carlos Andrés Cordero Retana

73.1. ***In slide 92, the copy constructor and assignment operator for
a class Vector are defined. When you try to use them, as in the
sample function called ‘useVector()’, a very bad exception will be
thrown at the end of its execution. Can you explain why and fix the
code, so it behaves correctly?
*/

#include <iostream>

#include "exercise73_1.h"


Vector::Vector(int size) : m_elem{ new double[size] }, m_size{ size }
{}

// El atributo  m_elem del constructor copy debe tener un nuevo arreglo!
Vector::Vector(const Vector& a) : m_elem{ new double[a.m_size] }, m_size{ a.m_size }
{   
// Debemos llenar el nuevo vector con los elementos del vector a
    for (int i = 0; i < m_size; ++i) { m_elem[i] = a.m_elem[i];
    }
}


Vector& Vector::operator=(const Vector& a)
	
{
    // De esta forma se evita la auto asignación
    if (this != &a) { 
        // Eliminamos lo que hay en el arreglo que apuntaba m_elem en el heap
        delete[] m_elem;

        this->m_size = a.m_size; // vector z y x con el mismo tamaño

        this->m_elem = new double[m_size]; // creamos el espacio del vector z

        // Realizamos una copia profunda, debido al problema de asignacion de punteros que habia
        for (int i = 0; i < m_size; ++i) {
            
            this->m_elem[i] = a.m_elem[i]; // Copiamos los elementos del vector x en z
        }
    }
    return *this;
}

// Si queremos acceder a algun valor del vector this
double Vector::operator[](int posicion) const 
{
    return this->m_elem[posicion];
}

Vector::~Vector() // Destructor
{
    delete[] m_elem;
}

int main()
{
    Vector x(1000);
    Vector z(1000);
    z = x;

    std:: cout<< "El valor de z[0]:" << z[0] << std::endl;
    return 0;
}




