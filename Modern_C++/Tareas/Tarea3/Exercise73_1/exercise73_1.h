#pragma once

class Vector {
    // Atributos
    double* m_elem;
    int m_size;

public:
    Vector(int size); // Constructor

    Vector(const Vector& a); // copia del constructor

    Vector& operator=(const Vector& a); // Sobrecarga del operador =
    double operator[](int pos) const; // Sobrecarga del operador[]

    ~Vector(); // Destructor
};