/*
Carlos Andrés Cordero Retana

109.1. ***Explain what type traits are and why they are useful in 
describing library and user defined types. They provide information in 
an unintrusive manner. Explain this. See <type_traits> at 
cppreference.com. Name at least 20 type traits and explain what 
information they allow access to
*/

#include <iostream>
#include <type_traits>
#include <typeinfo>
#include <vector>

class Typetrait { 
    public: 
    Typetrait(int x) { std::cout << "Constructor"<< std::endl; }
    ~Typetrait() { std::cout << "Destructor" << std::endl; }
};


/*
Los 'Type Traits' son metafunciones de plantillas en tiempo de compilación que devuelven información sobre los types.
Un type trait es un struct (templated) y la variable(s) miembro y/o los member types de la struct te dan información
acerca del type actual (T) que se está pasando en el template. Unintrusive manner" significa que la obtención de información 
sobre un tipo de dato no requiere modificar ni extender la definición de ese tipo de dato. 
*/


int main(){

// 1) is_floating_point
//true for fp types: float, double, long double.
//false for all other types



if(std::is_floating_point<int>::value){
    std::cout << "Value es un int"<< std::endl;
}

if(std::is_floating_point<long double>::value){
    std::cout << "True para long double"<< std::endl;
}

if(std::is_floating_point<float>::value){
    std::cout << "True para float\n"<< std::endl;
}

// 2) is_integral
//true for integral types: int, char, bool, short, long long, etc .
//false for all other types

 if(std::is_integral<int>::value) {
    std::cout << "int es un integral" << std::endl;
}

 if(std::is_integral<char>::value) {
    std::cout << "char es un integral\n\n" << std::endl;
}



//3) std::rank (working wit arrays)
// Brinda la dimension de un array

std::cout << "La dimension de un array int[4][2] es " << std::rank<int[4][2]>::value << std::endl;
std::cout << "La dimension de un array int[2] es " << std::rank<int[2]>::value << std::endl;
std::cout << "La dimension de un array int es " << std::rank<int>::value <<"\n\n"<< std::endl;

//4) std::remove_const
// It takes a type and gives you another type (removes consts)
//std:remove_const<const int>::type

if(std::is_integral< typename std::remove_const<const int>::type>::value) {
    std::cout << "El const fue removido, nos quedamos con int\n\n" << std::endl;
}

//5) std::is_nothrow_constructible

 //Se utiliza para determinar si es posible construir un objeto del tipo dado utilizando 
 //los argumentos especificados de manera que la construcción no arroje excepciones.

    if(!(std::is_nothrow_constructible<Typetrait, char>::value)){
        std::cout << "Se va a generar una excepcion si le metemos un char al constructor!\n\n" << std::endl;
    }


//6) std::is_destructible

//se utiliza para determinar si un tipo de dato dado es destructible. 
//Si utilizamos el member value, devuelve true si el tipo de dato tiene
// un destructor accesible y false en caso contrario.

if (std::is_destructible<Typetrait>::value) {
        std::cout << "El objeto de Typetrait5y6 es destructible\n\n" << std::endl;}



//7) std::is_nothrow_swappable
// Se utiliza para determinar si un tipo de dato dado puede ser intercambiado sin arrojar excepciones.

// En este caso, el type trait nos confirma que si podemos hacer swap de elementos entre dos vectores int, 
// sin que ocurra una excepcion

if (std::is_nothrow_swappable<std::vector<int>>::value) {
       std::cout << "std::vector<int> es nothrow-swappable \n\n" << std::endl;
    }



//8) std::alignment_of 
// Se utiliza para determinar el requisito de alineación de un tipo de dato dado. 
//Devuelve el valor de alineación requerido para una instancia del tipo dado

// Obtener el requisito de alineación para diferentes tipos de datos
    std::cout << "Requisito de alineacion para int: " << std::alignment_of<int>::value <<" bytes"<< std::endl;
    std::cout << "Requisito de alineacion para double: " << std::alignment_of<double>::value <<" bytes"<< std::endl;
    std::cout << "Requisito de alineacion para char: " << std::alignment_of<char>::value <<" bytes\n\n"<< std::endl;




//9) std::remove_extent
// se utiliza para eliminar el tamaño del primer nivel de un array de tipo dado. En otras palabras, elimina la dimensión 
// más externa de un array. En este ejemplo, note como el array pasa de int[5][10] a int[5]:

  std::cout << "La NUEVA dimension de int[5][10] es " << std::rank<std::remove_extent<int[5][10]>::type>::value <<"\n\n"<< std::endl;



//10) std::is_implicit_lifetime
// Determina si un objeto tiene un ciclo de vida implícito (por ejemplo, las variables locales tienen un ciclo de vida implícito),
//  o si necesita ser gestionado explícitamente por el programador (por ejemplo, los objetos creados con new en C++ necesitan ser
//  liberados con delete).


//11) std::common_type
// se utiliza para calcular el tipo común resultante de una lista de tipos dados. Este type trait determina un tipo que es 
// capaz de representar todos los tipos dados de manera segura y sin pérdida de precisión. En este caso, el tipo común resultante 
// será long long, ya que puede representar todos los valores de los tipos dados (int, long y long long) sin pérdida de precisión.

//std::common_type<int, long, long long>::type


//12) std::add_pointer
// Añade un puntero al tipo dado. Por ejemplo, std::add_pointer<int>::type es equivalente a int*.



//13) std::is_aggregate
// Determina si un tipo dado es un tipo de agregado en C++. Un tipo de agregado es una clase (o struct) que cumple con ciertas
// condiciones, como tener un constructor por defecto accesible, no tener constructores definidos por el usuario, no tener 
// inicializadores de miembro y más. Por ejemplo, un array o una clase sin miembros con atributos son tipos de agregado.



//14) std::has_unique_object_representations
// Determina si un tipo dado tiene representaciones de objetos únicas. Es decir, si dos objetos del mismo tipo con los mismos
// valores de miembros tienen representaciones de memoria idénticas. Esto se aplica principalmente a tipos de datos triviales.
// Por ejemplo, los tipos de datos enteros generalmente tienen representaciones de objetos únicas, mientras que los tipos de
// datos de punto flotante pueden no tenerlas debido a detalles de implementación como NaN (Not a Number).



//15) std::is_trivial
// Determina si un tipo dado es trivial. Un tipo trivial es un tipo que no tiene constructor, destructor, o asignación de 
// copia o de movimiento definidos por el usuario, y que no tiene miembros con tipos de datos no triviales. Por ejemplo, los
// tipos de datos fundamentales como int o double son triviales.



//16) std::is_standard_layout
// Determina si un tipo dado tiene un diseño estándar. Un tipo de diseño estándar es un tipo que se puede representar de
// manera predecible en memoria, siguiendo reglas específicas de alineación y organización de miembros. Por ejemplo, un 
// struct con solo miembros de tipo fundamental (int, double, etc.) y sin funciones virtuales o bases de clases virtuales
// es un tipo de diseño estándar.



//17) std::is_empty
// Determina si un tipo dado es un tipo vacío, es decir, si no tiene ningún miembro. Por ejemplo, si una clase no tiene
// ningún atributo definido, es un tipo vacío y std::is_empty devolverá true para ese tipo.

  if (std::is_empty<Typetrait>::value) {
      std::cout << "La clase es Typetrait es vacia\n\n" << std::endl;
  }



//18) std::is_bounded_array
// Determina si un tipo dado es un array de tamaño conocido en tiempo de compilación. Es decir, si el tamaño del array es
// determinado por una expresión constante. Por ejemplo, int array[5] es un array de tamaño conocido, mientras que int array[n]
// donde n es una variable, no lo es.



//19) std::is_abstract
// Determina si un tipo dado es una clase abstracta, es decir, si tiene al menos una función virtual pura. Una clase abstracta
// no puede ser instanciada directamente y debe ser subclaseada. Por ejemplo, una clase que contiene una función virtual pura
// como virtual void func() = 0; es una clase abstracta.


////20) std::is_same
// Determina si dos tipos dados son el mismo tipo. Devuelve true si los tipos son idénticos, es decir, si son el mismo tipo
// exactamente, incluyendo const, volatile y referencias. Devuelve false en caso contrario.

  if (!(std::is_same<int&, int>::value)) {
      std::cout << "int y int& no son iguales\n\n" << std::endl;
  }

  





}

