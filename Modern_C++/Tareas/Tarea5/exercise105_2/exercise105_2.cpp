// Carlos Andres Cordero Retana

/***Do types deduced using auto& always return reference types 
regardless of the references-ness of the initializer? If so, then it is not 
exactly type deduction! Right? We might be desiring to declare a 
reference to a type deduced by auto*/



// SÍ, siempre que utilicemos auto&, el tipo de datos deducido siempre sera:

// tipo de dato capturado por auto  +  &  

// Si solo utilizamos auto, no será posible deducir ningun tipo de dato referenciado. Esto, ya que el auto quita referencias y constantes.

// Por suerte, contamos con decltype(auto) que deduce el tipo de dato de dato, aun cuando este tenga &s o const

#include <iostream>
int main(){

    int x = 42;
    auto& ref1 = x; // ref1 es una referencia a un int

    ref1 = 100; // Modifica el valor de x a través de la referencia ref1
    std::cout << "x despues de modificar ref1: " << x << std::endl;


    //auto& ref2 = 10; // Esto causaría un error de compilación, ya que 10 no es una lvalue

    // std::cout << "ref2: " << ref2 << std::endl;

    return 0;
}