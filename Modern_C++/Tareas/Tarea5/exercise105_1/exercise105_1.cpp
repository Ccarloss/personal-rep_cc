/* 
Carlos Andres Cordero Retana

105.1. ***Distinguish auto type deduction from decltype(auto) type 
deduction. Can you see that auto never catches the “references-ness” 
of the initializer? What would happen if you add an ampersand after 
the auto? Is this always legal? In particular, can auto& be applied to a 
literal value? (i.e. auto& rr = 10;). */


#include <iostream>
	
double& h(double x) { return x; } 	// return lvalue reference


int main(){

    /*----------Answering : Distinguish auto type deduction from decltype(auto) type 
    deduction. Can you see that auto never catches the “references-ness” 
    of the initializer?-----------------------------------------------------------*/

    int i = 5;  
    const int& k = i;

    //Con decltype(auto) se conserva el tipo de dato original
    decltype(auto) di = i;
    decltype(auto) dk = k;

    //dk =66;  //Se queja porque es const: expression must be a modifiable lvalue 


    // Con auto eliminan las const y los &s
    auto ak = k;

    ak = 7;  // No se queja porque auto lo convirtió a int



    /*-----Answering : What would happen if you add an ampersand after the auto? 
    Is this always legal? In particular, can auto& be applied to a 
    literal value? (i.e. auto& rr = 10;).-------------------------------------*/


    // h(x) retorna double & a un lvalue x , no obstante con SOLO auto, se tendría un double.

    // Gracias al & que agregamos enfrente de auto, Ah sería double&

    // double& Ah = double& x 
    auto& Ah = h(1.1);  
    
    // OJO, esto es mala practica porque x es una variable local !!!

    //Cuando no salimos del ambito de h, x desaparece y por ende la referencia Ah se pierde

    //std::cout << "El valor de Ah es " <<Ah<< std::endl;  

    /*
    Las referencias deben vincularse siempre a lvalues. Una referencia 
    no puede vincularse a un rvalue directamente, ya que los lvalues
    representan ubicaciones de memoria con identidad, mientras que los
    rvalues son temporales sin ubicaciones de memoria identificables.

    Es decir, esto estaría incorrecto:
    
    auto& rr = 10;    ***initial value of reference to non-const must be an lvalue
    
    */

   

   // Siempre que se tenga un lvalue, sera legal utilizar auto& 
   int r = 5;

   auto& rr = r;

   std::cout<< "Comentando la linea 55 del codigo, se obtiene:\n"<< std::endl; 

   std::cout<< "El valor de la referencia auto& rr es: " << rr << std::endl;   

    return 0;
}









