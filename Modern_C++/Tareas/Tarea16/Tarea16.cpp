// Carlos Andres Cordero Retana

// Tarea 16: 

//  Tarea 16: STL Algorithms

#include "Algosutff.h"




/*
 275. ***Create a vector<int> with 3 elements in ascending order. Create
another container with 10 randomly generated elements and insert them
without using a sort algorithm into the first vector<int> in such a way that
the vector<int> remains sorted. Hint: use generate, lower_bound, and
insert.
 */

namespace Exercise_275
{
	using namespace std;
	using namespace std::literals;

	void exercise_275()
	{
		// Creamos un vector con 3 elementos en orden ascendente
		vector Vec1 = { 15, 23, 35 };
		print(Vec1, "Vector con 3 elementos incial: ");


		srand(time(NULL)); // semilla para el generador aleatorio
		// Creamos un vector con 10 elementos generados aleatoriamente
		vector<int>rand_vec(10);
		std::generate(rand_vec.begin(), rand_vec.end(), [n = 0]() mutable { return n= n+ rand() % 21; });
		print(rand_vec, "Vector con 10 elementos random: ");


		// Insertar elementos en el primer vector de forma que permanezca ordenado
		for (const auto& i : rand_vec) {
			auto iterador = std::lower_bound(Vec1.begin(), Vec1.end(), i);
			Vec1.insert(iterador, i);
		}

		print(Vec1, "El nuevo vector ordenado : ");
	}
}



/*
 281. **Combine two sorted ranges into a third one so that the resulting
container is sorted. Hint: use merge
 */

namespace Exercise_281
{
	using namespace std;
	using namespace std::literals;

	void Exercise_281()
	{
		vector c1 = { 1,2,4,7,9 };
		vector c2 = { 3,5,8,10 };

		print(c1, "C1: ");
		print(c2, "C2: ");

		vector<int> nuevo_vect;

		// Fusiona dos rangos ordenados (C1 y C2) en orden y los almacena en el vector
		// nuevo_vect, manteniendo el orden.
		std::merge(c1.begin(), c1.end(),
			c2.begin(), c2.end(),
			std::inserter(nuevo_vect,nuevo_vect.begin()));
		// Se imprime el vector resultante
		print(nuevo_vect, "C1 + C2 sorted: ");
	}

}



/*
283. ***Briefly describe ten of the following algorithms as to what they can
do. Please use a small illustrative example.
 */

namespace Exercise_283
{
	using namespace std;
	using namespace std::literals;


	void Is_sorted_1()
	{

		vector coll1 = { 1,1,2,3,4,5,6,7,8,9 };
		print(coll1, "coll1: ");

		//is_sorted() es una funci�n que se encuentra en la biblioteca est�ndar de C++, <algorithm>.
		//Esta funci�n se utiliza para determinar si un rango de elementos est� ordenado en orden
		//ascendente o no.Toma dos iteradores como argumentos que definen el rango que se va a verificar
		//y devuelve true si los elementos est�n ordenados en orden ascendente, y false en caso contrario.

		if (is_sorted(coll1.begin(), coll1.end()))
		{
			cout << "coll1 is sorted\n";
		}

		map<int, string> coll2;
		coll2 = { {1,"Bill"}, {2,"Jim"}, {3, "Nico"}, {4, "Liu"}, {5,"Ai"} };
		print_mapped_elements(coll2);

		auto compareName = [](const pair<int, string>& e1, const pair<int, string>& e2)
			{
				return e1.second < e2.second;
			};

		// check whether the names in map are sorted
		if (is_sorted(coll2.begin(), coll2.end(), compareName))
		{
			cout << "names in coll2 are sorted\n";
		}
		else
		{
			cout << "names in coll2 are not sorted\n";
		}

		//is_sorted_until() devuelve un iterador apuntando al primer elemento desordenado si el rango
		//no est� completamente ordenado.Si todos los elementos est�n ordenados, devuelve el iterador
		//final del rango.
		auto pos = is_sorted_until(coll2.begin(), coll2.end(), compareName);
		if (pos != coll2.end())
		{
			cout << "first unsorted name: " << pos->second << endl;
		}
	}

	void Is_partitioned_2()
	{
		vector coll = { 5,3,9,1,3,4,8,2,6 };
		print(coll, "coll: ");

		// Funcion que impone la condicion de los elementos de coll
		auto isOdd = [](int elem)
			{
				return elem % 2 == 1;
			};
		// Esta funcion verifica si un rango de elementos est� particionado seg�n un predicado dado.
		// Un predicado es una funci�n que establece una condici�n. En este caso, la funcion isOdd
		// es la que impone la condici�n.
		//
		// Entonces, dado que todos los elementos de coll que no cumplen con la condici�n se agrupan
		// al principio, y todos los que s� la cumplen se agrupa al final, is_partitioned devuelve true,
		// porque coll s� est� particionado seg�n la condici�n isOdd

		if (is_partitioned(coll.begin(), coll.end(), isOdd))
		{
			cout << "cout is partitioned" << endl;

			// partition point se utiliza para encontrar el primer elemento que no cumple la condici�n, es decir
			// busca el primer elemento que sea par (para este caso) y devuelve un iterador apuntando ah�
			// Si todos los elementos cumplen con la condicion, pos == coll.end().
			auto pos = partition_point(coll.begin(), coll.end(), isOdd);
			if (pos != coll.end())
			{
				cout << "First even element is: " << *pos << endl;
			}
		}
	}



	void Is_heap_3()
	{

		vector coll1 = { 9,8,7,7,7,5,4,2,1 };
		vector coll2 = { 5,3,2,1,4,7,9,8,6 };
		print(coll1, "coll1: ");
		print(coll2, "coll2: ");

		// is_heap es una funci�n de la biblioteca est�ndar de C++, <algorithm>, que se utiliza para verificar
		// si un rango de elementos est� organizado como un heap.
		cout << boolalpha << "coll1 is heap:" << is_heap(coll1.begin(), coll1.end()) << endl;
		cout << boolalpha << "coll2 is heap:" << is_heap(coll2.begin(), coll2.end()) << endl;

		// is heap_until devuelve un iterador apuuntando al primer elemento	que no cumpla con la condicion de ser heap
		auto pos = is_heap_until(coll2.begin(), coll2.end());
		if (pos != coll2.end())
		{
			cout << "First no heap element: " << *pos << endl;
		}
	}

	/// Testing equality
	bool bothEvenOrOdd(int elem1, int elem2)
	{
		return elem1 % 2 == elem2 % 2;
	}

	void Is_permutation_4()
	{
		vector<int> coll1;
		list<int> coll2;
		deque<int> coll3;

		coll1 = { 1,1,2,3,4,5,6,7,8,9 };
		coll2 = { 1,9,8,7,6,5,4,3,2,1 };
		coll3 = { 11,12,13,19,18,17,16,15,14,11 };


		print(coll1, "coll1: ");
		print(coll2, "coll2: ");
		print(coll3, "coll3: ");

		// Se utiliza para verificar si dos secuencias son permutaciones una de la otra
		if (is_permutation(coll1.begin(), coll1.end(), coll2.begin()))
		{
			cout << "coll1 and coll2 have equal elements" << endl;
		}
		else
		{
			cout << "coll1 and coll2 do not have equal elements" << endl;
		}
		//////////////////////////
		/// count # of even numbers
		///
		auto even_number_coll1 = count_if(coll1.begin(), coll1.end(), [](auto& item)
			{
				return item % 2 == 0;
			});
		auto even_number_coll3 = count_if(coll3.begin(), coll3.end(), [](auto& item)
			{
				return item % 2 == 0;
			});
		///////////////////////
		

		// La funci�n tambi�n puede aceptar un predicado opcional que define c�mo se comparan los elementos.

		//Se verifican si los elementos de coll1 y coll3 son permutaciones una de la otra, considerando como
		//iguales solo aquellos elementos que cumplen la condici�n definida por el predicado bothEvenOrOdd.

		//Si todos los elementos que cumplen esta condici�n est�n presentes en ambas secuencias, sin importar
		//el orden, entonces se consideran permutaciones una de la otra.
		if (is_permutation(coll1.begin(), coll1.end(), coll3.begin(), bothEvenOrOdd))
		{
			cout << "numbers of even and odd elements match" << endl;
		}
		else
		{
			cout << "numbers of even and odd elements don't match" << endl;
		}
	}


	void Replace_5()
	{
		list<int> coll;

		insert(coll, 2, 7);
		insert(coll, 4, 9);
		print(coll, "coll: ");

		// Reemplaza todas las ocurrencias de un valor espec�fico en una secuencia con otro valor dado:

		/*

		template< class ForwardIt, class T >
		void replace(ForwardIt first, ForwardIt last, const T & old_value, const T & new_value);

		---> first, last: Iteradores que definen el rango en el que se realizar� la b�squeda y el reemplazo.
		---> old_value: El valor que se buscar� en la secuencia para ser reemplazado.
		---> new_value : El valor con el que se reemplazar�n todas las ocurrencias de old_value.

		*/

		replace(coll.begin(), coll.end(), 6, 42);
		print(coll, "coll: ");
		// replace all values less than 5 with 0
		replace_if(coll.begin(), coll.end(), [](int elem)
			{
				return elem < 5;
			}, 0);
		print(coll, "coll: ");
	}

	void Fill_6()
	{
		//Esta funci�n se utiliza para llenar los primeros n elementos de una secuencia con un valor espec�fico.
		// Sintaxis:

		/*
		template< class OutputIt, class Size, class T >
		OutputIt fill_n( OutputIt first, Size count, const T& value );

		---> first: Iterador que apunta al primer elemento donde comenzar� el llenado.
		---> count: El n�mero de elementos que se llenar�n con el valor especificado.
		---> value: El valor con el que se llenar�n los elementos.
		 */

		fill_n(ostream_iterator<float>(cout, "\n"), 10, 7.7);
		cout << endl;
		list<string> coll;

		fill_n(back_inserter(coll), 9, "hello");
		print(coll, "coll\t");


		// Esta funci�n se utiliza para llenar toda la secuencia con un valor espec�fico ("again")
		fill(coll.begin(), coll.end(), "again");
		print(coll, "again\t");


		// replace all but 2 elements with "hi"
		fill_n(coll.begin(), coll.size() - 2, "hi");
		print(coll, "hi\t");


		//replace the second and up to the last element but one with "hmmmm"
		list<string>::iterator pos1, pos2;
		pos1 = coll.begin();
		pos2 = coll.end();
		++pos1;
		--pos2;
		fill(pos1, pos2, "hmmmm");
		print(coll, "hmmm\t");
	}


	void PartialSortCopy_7()
	{
		deque<int> coll1;
		vector<int> coll6(6);
		vector<int> coll30(30);

		insert(coll1, 3, 7);
		insert(coll1, 2, 6);
		insert(coll1, 1, 5);
		print(coll1, "coll1: ");

		// Esta funcion copia los elementos (y los ordena) del rango de coll1 en el rango coll6.
		// Por ultimo, devuelve un iterador que apunta (en coll6) al final del rango de los elementos copiados y ordenados parcialmente.
		vector<int>::const_iterator pos6;
		pos6 = partial_sort_copy(coll1.begin(), coll1.end(), coll6.begin(), coll6.end());


		//Copiamos los elementos del rango coll6.cbegin() ---> pos6,  hacia el flujo de salida est�ndar (cout)
		copy(coll6.cbegin(), pos6, ostream_iterator<int>(cout, " "));
		cout << endl;


		// Hacemos lo mismo que lo anterior, solo que una condici�n extra:
		// greater<int>(): queremos que los elementos se ordenen de mayor a menor
		vector<int>::const_iterator pos30;
		pos30 = partial_sort_copy(coll1.begin(), coll1.end(), coll30.begin(), coll30.end(), greater<int>());

		// print all copied elements
		copy(coll30.cbegin(), pos30, ostream_iterator<int>(cout, " "));

		cout << endl;
	}

	void Merge_8()
	{
		list<int> coll1;
		set<int> coll2;

		// Creamos las secuencias coll1 y coll3
		insert(coll1, 1, 6);
		insert(coll2, 3, 8);

		print(coll1, "coll1\n");
		print(coll2, "coll2\n");

		cout << "merged: ";

		//Se fusionan las dos secuencias ordenadas, coll1 y coll2, y la nueva secuencia fusionada se asigna al iterador: ostream_iterator
		merge(coll1.cbegin(), coll1.cend(), coll2.begin(), coll2.end(), ostream_iterator<int>(cout, " "));
		cout << endl;
	}



	//////////////
	///Vamos a utilizar varias operaciones para procesar dos conjuntos ordenados, c1 y c2. 
	///
	void SetOperations_9()
	{
		vector<int> c1 = { 1,2,2,4,6,7,7,9 };
		deque<int>  c2 = { 2,2,2,3,6,6,8,9 };

		//print source ranges
		cout << "c1:\n";
		copy(c1.cbegin(), c1.cend(), ostream_iterator<int>(cout, " "));
		cout << endl;

		cout << "c2:\n";
		copy(c2.cbegin(), c2.cend(), ostream_iterator<int>(cout, " "));
		cout << endl;

		// Se calcula la uni�n de dos conjuntos.Es decir, combina los elementos �nicos de ambos conjuntos en un tercer conjunto ordenado.
		cout << "set_union:";
		set_union(c1.cbegin(), c1.cend(), c2.cbegin(), c2.cend(), ostream_iterator<int>(cout, " "));
		cout << endl;


		// Se calcula la intersecci�n de dos conjuntos. Es decir, encuentra los elementos que est�n presentes en ambos conjuntos.
		cout << "set_intersection:";
		set_intersection(c1.cbegin(), c1.cend(), c2.cbegin(), c2.cend(), ostream_iterator<int>(cout, " "));
		cout << endl;


		// Se calcula la diferencia entre dos conjuntos. Es decir, encuentra los elementos que est�n presentes en el primer conjunto pero no en el segundo.
		cout << "set_difference:";
		set_difference(c1.cbegin(), c1.cend(), c2.cbegin(), c2.cend(), ostream_iterator<int>(cout, " "));
		cout << endl;

		// Se calcula la diferencia sim�trica de dos conjuntos. Es decir, encuentra los elementos que est�n presentes en uno de los conjuntos pero no en ambos.
		cout << "set_symmetric_difference:";
		set_symmetric_difference(c1.cbegin(), c1.cend(), c2.cbegin(), c2.cend(), ostream_iterator<int>(cout, " "));
		cout << endl;
	}

	/// Removing elements while copying
	///	Las funciones remove_copy() y remove_copy_if() copian elementos de una lista (coll1) a
	///	otros contenedores (cout y coll2) mientras se eliminan ciertos elementos.
	
	void RemoveCopy_10()
	{
		list<int> coll1;
		insert(coll1, 1, 6);
		insert(coll1, 1, 9);
		print(coll1, "coll1: ");
		// Copia los elementos de coll1 al flujo de salida est�ndar (cout), excluyendo los elementos con valor 3.
		remove_copy(coll1.begin(), coll1.end(), ostream_iterator<int>(cout, " "), 3);
		cout << endl;

		//Se copian los elementos de coll1 al flujo de salida est�ndar(cout), excluyendo los elementos mayores que 4,
		remove_copy_if(coll1.begin(), coll1.end(), ostream_iterator<int>(cout, " "), [](int elem)
			{
				return elem > 4;
			});

		cout << endl;
		multiset<int> coll2;


		//Se copian los elementos de coll1 al conjunto coll2, excluyendo los elementos que son menores que 4.
		remove_copy_if(coll1.begin(), coll1.end(), inserter(coll2, coll2.end()), bind(less<int>(), _1, 4));
		print(coll2, "coll2: ");
	}

	void Exercise_283()
	{
		cout << "\n___________Non_Modifying : Is_sorted______________\n" << endl;
		Is_sorted_1();

		cout << "\n___________Non_Modifying : Is_partitioned______________\n" << endl;
		Is_partitioned_2();
		cout << "\n___________Non_Modifying : Is_heap______________\n" << endl;
		Is_heap_3();
		cout << "\n___________Non_Modifying : Is_permutation______________\n" << endl;
		Is_permutation_4();
		cout << "\n___________Modifying : Replace______________\n" << endl;
		Replace_5();
		cout << "\n___________Modifying : Fill______________\n" << endl;
		Fill_6();
		cout << "\n___________Sorting: Partial_sort_copy______________\n" << endl;
		PartialSortCopy_7();
		cout << "\n___________Sorted-range: Merge______________\n" << endl;
		Merge_8();
		cout << "\n___________Set_union, Sorted-range: Set_intersection,Set_difference, Set_symmetric_difference______________\n" << endl;
		SetOperations_9();
		cout << "\n___________Removing: Remove_copy______________\n" << endl;
		RemoveCopy_10();
	}

}




int main()
{
	std::cout << "\n\n********************__Exercise_275__*******************\n\n" << std::endl;
	Exercise_275::exercise_275();
	std::cout << "\n\n********************__Exercise_281__*******************\n\n" << std::endl;
	Exercise_281::Exercise_281();
	std::cout << "\n\n********************__Exercise_283__*******************\n\n" << std::endl;
	Exercise_283::Exercise_283();


}