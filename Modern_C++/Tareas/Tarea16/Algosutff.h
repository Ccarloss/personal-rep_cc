#pragma once



#include <array>
#include <algorithm>
#include <functional>
#include <iostream>
#include <ostream>
#include <vector>
#include <list>
#include <forward_list>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <iterator>
#include <numeric>
#include <string>
#include <concepts>
#include <deque>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


using namespace std::literals;
using namespace std::placeholders;

template<typename C>
concept is_container = requires (C c)
{
	// c.begin();
	// c.end();
	std::begin(c);
	std::end(c);
};

template<typename Col>
void print(const Col& c, const std::string& s = "Contents"s)
{
	std::cout << s << ": ";
	for (auto elem : c)
	{
		std::cout << elem << " ";
	}
	std::cout << "\n";
}

template<typename Col> requires is_container<Col>
void insert(Col& c, int first, int last)
{
	for (int i = first; i <= last; ++i)
	{
		c.insert(c.end(), i);
	}
}

template<typename It, typename Col> requires is_container<Col>
void distance(It start, It end, const Col& c, const std::string& msg)
{
	std::cout << "the " << std::distance(start, end) + 1 << " element is " << msg << std::endl;
}

template<typename T1, typename T2>
void print_mapped_elements(std::map<T1, T2>& m)
{
	for (auto elem : m)
	{
		std::cout << elem.first << " -> " << elem.second << std::endl;
	}
}