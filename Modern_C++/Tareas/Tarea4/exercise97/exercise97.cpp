/*
Carlos Andrés Cordero Retana

97.1. ***We have to be very careful with inheritance. Although a
window with a border IS-A window, it can also be thought of as a
window that HAS-A border. Incorporating objects inside a class is
usually a better way to deal with extending an entity than going via
brute force using inheritance. Since a window may be “adorned” by
many additional items, creating a class for each possible combination
quickly becomes exponential and a combinatorial explosion that does
not scale well.


97.2. Looking at slide 157, how could you provide support for the
different entities without the combinatorial explosion caused by
multiple inheritance? [Note: multiple inheritance by itself is a VERY
useful feature of the C++ language, but not in this particular form]*/


#include "exercise97.h"

// Definición de la clase Point
Point::Point(int x, int y) : x{x}, y{y} {}

// Este metodo retorna el valor de x, y del objeto tipo punto
std::string Point::get_point() {

    return std::to_string(this->x) + "," + std::to_string(this->y) ;
}

// Definición de la clase Polygon
void Polygon::add(const Point& p) {
    points.push_back(p); 
}

void Polygon:: set_color(Color c) {
    color = c;
}



// Definición de la clase Simple_window
Simple_window::Simple_window(const Point& tl, int width, int height, const std::string& label)
    : tl{tl}, width{width}, height{height}, label{label} {}

// Este metodo se encargaría de agregar el objeto de la clase poligono al objeto tipo Simple_window    
void Simple_window::attach(const Polygon& poly) {
    polygons.push_back(poly); 
}

// Esta funcion se encarga de desplegar la ventana completa.
void Simple_window::wait_for_button() const {
    // Implementación de wait_for_button()
}
