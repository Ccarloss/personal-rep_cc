#ifndef exc97
#define exc97

#include <iostream>
#include <vector>
#include <tuple>
#include <string>

// Definición de la clase Point
class Point {
public:
    Point(int x, int y);

     std::string get_point();

    int x, y;
};

/* C++11 ha introducido las clases de enumeración 
 que hacen que las enumeraciones sean tanto fuertemente
tipadas como fuertemente acotadas*/
enum class Color {
    red,
    blue,
    green
};

// Definición de la clase Polygon
class Polygon {
public:
    // Este metodo almacena el objeto tipo Punto en el vector points (Composition approach)
    void add(const Point& p);

    // Se define el color del objeto tipo poligono A PARTIR del objeto tipo Color    
    void set_color(Color c);




private:
    std::vector<Point> points;
    Color color;
};

// Definición de la clase Simple_window
class Simple_window {
public:
    Simple_window(const Point& tl, int width, int height, const std::string& label);

    // Este metodo se encargaría de agregar el objeto de la clase poligono al objeto tipo Simple_window    
    void attach(const Polygon& poly);

    // Esta funcion se encarga de desplegar la ventana completa.
    void wait_for_button() const;

private:
    // Atributos típicos de toda ventana
    Point tl;
    int width, height;
    std::string label;
    std::vector<Polygon> polygons;
};

#endif 
