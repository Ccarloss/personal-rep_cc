/*
Carlos Andrés Cordero Retana

97.1. ***We have to be very careful with inheritance. Although a
window with a border IS-A window, it can also be thought of as a
window that HAS-A border. Incorporating objects inside a class is
usually a better way to deal with extending an entity than going via
brute force using inheritance. Since a window may be “adorned” by
many additional items, creating a class for each possible combination
quickly becomes exponential and a combinatorial explosion that does
not scale well.


97.2. Looking at slide 157, how could you provide support for the
different entities without the combinatorial explosion caused by
multiple inheritance? [Note: multiple inheritance by itself is a VERY
useful feature of the C++ language, but not in this particular form]*/


#include "exercise97.h"

int main() {
    Point tl(100, 200);
    Simple_window win(tl, 600, 400, "Canvas");
    
    Polygon poly;
    poly.add(Point(300, 200));
    poly.add(Point(350, 100));
    poly.add(Point(400, 200));
    poly.set_color(Color::red);

    win.attach(poly);
    win.wait_for_button();

    std:: cout << "La ubicacion de la ventana es: "<< tl.get_point() << std::endl;
    
    return 0;
}