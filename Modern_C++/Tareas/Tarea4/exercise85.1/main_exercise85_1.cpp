/*
	Carlos Andrés Cordero Retana

	Exercise 85.1 **The hierarchy given in slide 133 has a serious error in the
definition of the all_info() method. What is it? Create a program that
showcases the problem?
*/


#include "exercise85_1.h"

int main(){

    //De momento no aplicamos el concepto de polimorfismo
    student estudiante1("Mario", "mate");

    person persona1("Marito");

    estudiante1.all_info(); // Se llama al metodo de la clase student 

    persona1.all_info(); // Se llama al metodo de la clase person

    cout<<"\n-------------------------------------------------"<<endl;

    // Si queremos aplicar polimorfismo, no va a funcionar
    person* ptr = &persona1;

    ptr->all_info();  // Se llama al metodo de la clase person

    // Cambiamos el valor del ptr para que apunte al objeto tipo student

    ptr = &estudiante1;
    /*
    DESEARÍAMOS que se hubiera llamado al metodo de student, sin embargo dado que el metodo NO es virtual,
    no va a existir polimorfismo, y por ende, el compilador llamara al metodo de la clase base: person*/
    ptr->all_info(); 
    
    return 0;
}


