
/*
	Carlos Andrés Cordero Retana

	Exercise 85.1 **The hierarchy given in slide 133 has a serious error in the
definition of the all_info() method. What is it? Create a program that
showcases the problem?
*/

#ifndef PERSON_H
#define PERSON_H

#include <iostream>

using namespace std;

class person {
public:
    person();
    explicit person(const std::string& name);
    person(const person& other);
    void set_name(const std::string& n);
    std::string get_name() const;

    // El problema sería que el metodo no esta definido como VIRTUAL.
    // Esto evita sobreescribir el mismo metodo en las clases hijas
    void all_info() const;

private:
    std::string name;
};


class student : public person {
public:
    student(const std::string& name, const std::string& passed);
    void all_info() const ;  // Escribiriamos override si fuese Virtual

private:
    std::string passed;
};

#endif
