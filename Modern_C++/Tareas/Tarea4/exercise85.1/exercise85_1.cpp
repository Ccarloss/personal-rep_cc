/*
	Carlos Andrés Cordero Retana

	Exercise 85.1 **The hierarchy given in slide 133 has a serious error in the
definition of the all_info() method. What is it? Create a program that
showcases the problem?
*/

#include "exercise85_1.h"

// Clase Padre
person::person() {}

person::person(const std::string& name) : name(name) {}

person::person(const person& other) = default;

void person::set_name(const std::string& n) {
    name = n;
}

std::string person::get_name() const {
    return name;
}

void person::all_info() const {
    std::cout << "[person]   My name is " << name << std::endl;
}

// Clase hija
student::student(const std::string& name, const std::string& passed)
    : person(name), passed(passed) {}

void student::all_info() const {
    std::cout << "[student]  My name is " << this->get_name() << std::endl;
    std::cout << "    I passed the following grades: " << passed << std::endl;
}



