/*
Carlos Andrés Cordero Retana

88.1. **Looking at slide 137, what is an abstract class?

R/
- Es una clase que contiene uno o más métodos virtuales puros.
Un ejemplo de metodo virtual puro sería el metodo all_info().

- La implementación de esta función se lleva a cabo en las clase o clases
hijas. De lo contrario, estas también serían clases abstractas.

- No es permitido crear objetos de la clase padre (creature). Solo es permitido
crear objetos a partir de las clases hijas donde habita la definición del método
virtual.


** Why is placing override important in method all_info() of class person?
R/
- Es muy importante colocarlo, ya que mediante este se le indica al compilador
que verifique que el nombre del metódo all_info() es el mismo en la clase
padre que en la clase hija.

- De esta forma, si el nombre difiere, nos daríamos cuenta gracias a que el overrite
le dijo al compilador que chequeara.


Why does declaring an object of type creature an error? Then what is the use of
type creature?

- Las clases abstractas no pueden ser instanciadas directamente porque contienen 
una o más funciones virtuales puras, como virtual void all_info() const noexcept = 0;

- creature actúa como una interfaz común para todas las clases que representan 
diferentes tipos de criaturas, como person, animal, etc. Esto facilita el tratamiento
uniforme de diferentes tipos de criaturas en el código.


*/


#include <iostream>
#include <string>

using namespace std;

class creature {
public:

    virtual void all_info() const noexcept = 0;
};

class person : public creature {
public:
    person() = default;
    explicit person(const string& name) noexcept : name{name} {}

    void set_name(const string& n) { name = n; }
    string get_name() const { return name; }

    // La implementación le toca a la clase hija: person
    virtual void all_info() const noexcept override { cout << "My name is " << name << endl; }
private:
    string name;
};


class animal : public creature {
public:

    animal() = default;
    explicit animal(const string& type) noexcept : type{type} {}

    void set_type(const string& t) { type = t; }
    string get_type() const { return type; }

    // Implementación del método virtual 'all_info()'
    virtual void all_info() const noexcept override { cout << "Soy un animal " << type << "." << endl; }
private:
    string type;
};


int main() {
    person p("Luigi");
    p.all_info();

    animal a("mamifero");
    a.all_info();

    //creature c;  (provoca un error de compilacion)

    return 0;
}
