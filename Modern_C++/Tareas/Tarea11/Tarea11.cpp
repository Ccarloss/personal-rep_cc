// Carlos Andres Cordero Retana

// Tarea 11: Meta programming (programs that run at compile time)

#include <iostream>
#include <typeinfo>

/*
193. Exercise:
193.1. **What role does recursion play in meta programming? Explain
how the struct select in slides 337 allows us to program a �switch� to
use at compile time.
*/
namespace Exercise193_1 {

	template<unsigned N, typename...Cases>
	struct select; // general case, never instantiated!

	// Gracias a la recursion, se puede implementar el siguiente algoritmo en tiempo de
	// compilacion.
	/*
	select< 3, A1,A2,A3,A4,A5> 
		: select< 2,A2,A3,A4,A5>
			: select< 1,A3,A4,A5> 
				: select< 0,A4,A5>
	
	*/
	//La idea es heredar una nueva configuraci�n en cada iteraci�n, disminuyendo el valor N y la cantidad de A's
	//  hasta que N == 0, ya que en ese punto, el compilador le da preferencia a la especializaci�n 
	// definida con N==0. Actua como un switch en tiempo de compilaci�n, ya que el valor de A elegido 
	// depender� del valor de N ingresado.


	template<unsigned N, typename T, typename...Cases>
	struct select<N, T, Cases...>: select<N - 1, Cases...>
	{};

	// Cuando llegamos al caso en que N==0 (Ej : select< 0,A4,A5> )  
	// Se eligir�a el elemento T como type (en nuestro ej ser�a T = A4)
	template<typename T, typename...Cases> \
	struct select<0, T, Cases...> {
		using type = T;
	};

	
	template<unsigned N, typename...Cases> // alias para mejorar la legibilidad
	// Utilizamos typename para indicarle al compilador que el miembro de select es un tipo de dato
	using Select = typename select<N, Cases...>::type;


	// Aplicacion del algoritmo: el usuario desea obtener el type correspondiente a 
	// la cantidad de bytes ingresada por el mismo.
	template<unsigned N>
	struct integer {
		using Error = void;
		using type = Select<N, Error, signed char, short, Error, int, Error, Error, Error, long>;
	};


	template<unsigned N>// alias
	using Integer = typename integer<N>::type;


	void Exercise193_1(){
		typename integer<1>::type i1 = 15; // 1 byte -> signed char
		typename integer<2>::type i2 = 127; // 2 bytes -> short

		// Para poder comparar instrucciones en codigo ensamblador!
		int prueba = 44;

		// Usando el alias
		Integer<4> i3 = 44; // 4 bytes --> int
		Integer<8> i4 = 12; // 8 bytes--> long

		// Test
		std::cout << "El tipo de i1 es: " << typeid(i1).name() << std::endl;
		std::cout << "El tipo de i2 es: " << typeid(i2).name() << std::endl;
		std::cout << "El tipo de i3 es: " << typeid(i3).name() << std::endl;
		std::cout << "El tipo de i4 es: " << typeid(i4).name() << std::endl;
	}
}



/*
197. Exercise:
197.1. ***Why do we say in slide 347 that in meta programming we
have an implicit IS-A �hierarchy�? In slides 347 � 350, what concrete
types play the �role� of Tuple in the template class tuple_element
defined in slide 347.
*/

namespace Exercise197_1 {

	// CLASS tuple_element (find element by index)
	template<size_t _Index, class _Tuple>
	struct tuple_element;


	// ALIAS FOR EASE OF USE
	template<size_t _Index, class _Tuple>
	using tuple_element_t = typename tuple_element<_Index, _Tuple>::type;



	//-----------------------Especializacion: Array-----------------------//
	// Extract information from an index and a std::array ==> index, a type and a size (and verify if index < size)
	template<size_t _Idx, class _Ty,size_t _Size>
	struct tuple_element<_Idx, std::array<_Ty, _Size> >
	{ // struct to determine type of element _Idx in array
		static_assert(_Idx < _Size, "array index out of bounds");

		// Retornamos type como _Ty debido que un array 
		using type = _Ty;
	};




	//-----------------------Especializacion: Pares-----------------------//
	// El par tiene 2 tipos

	// Especializacion : tipo del primer elemento del par
	template<class _Ty1,
		class _Ty2>
	struct tuple_element<0, std::pair<_Ty1, _Ty2> >
	{ // struct to determine type of element 0 in pair
		using type = _Ty1;
	};

	// Especializacion : tipo del segundo elemento del par
	template<class _Ty1,
		class _Ty2>
	struct tuple_element<1, std::pair<_Ty1, _Ty2> >
	{ // struct to determine type of element 1 in pair
		using type = _Ty2;
	};




	//-----------------------Especializacion: Tuplas-----------------------//

	// special case: the empty tuple ==> always out of bounds and non substitutable!
	template<size_t _Index>
	struct tuple_element<_Index, std::tuple<> >
	{ // enforce bounds checking
		static_assert(Always_false<integral_constant<size_t, _Index> >::value,
			"tuple index out of bounds");
	};

	// Se emplea el mismo algoritmo del ejercicio 193_1! 
	template<size_t _Index,
		class _This,
		class... _Rest>
	struct tuple_element<_Index, std::tuple<_This, _Rest...> >
		: public tuple_element<_Index - 1, std::tuple<_Rest...> >
	{ // recursive tuple_element definition
	};


	template<class _This, class... _Rest>
	struct tuple_element<0, std::tuple<_This, _Rest...> >
	{ // select first element
		using type = _This;
		using _Ttype = std::tuple<_This, _Rest...>;
	};
	



	void Exercise197_1(){

		/*
		R/ En meta programming, tenemos una jerarqu�a impl�cita IS-A porque 
		las especializaciones de plantillas (en este caso fueron 3: arrays, pares y tuplas) 
		pudieron verse como extensiones o subtipos de la plantilla base (tuple_element). Es decir,
		cada especializaci�n de tuple_element fue un tipo de tuple_element y se hered� o se extendi�
		su funcionalidad para que trabajara con un tipo espec�fico de contenedor.

		Estamos hablando de polimorfismo est�tico, ya que no hay herencia entre los tipos.
		Tanto el array, el par y la tupla son independientes, pero los podemos usar como 
		si vinieran de una misma jerarqu�a de objetos.

		*/


		// Empleando la version de tuple_element para ARRAYS
		tuple_element<0, std::array<int, 2>>::type x = 0; // x es un int
		//Assembly check
		int x2 = 0;


		// Empleando la version de tuple_element para PARES
		tuple_element<0, std::pair<int, double>>::type y1 = 0; // y1 es int
		tuple_element<1, std::pair<int, double>>::type y2 = 0; // y2 es double
		//Assembly check
		double y3 = 0;


		// Empleando la version de tuple_element para TUPLAS
		tuple_element<1, std::tuple<int, float, unsigned>>::type z = 0;
		//Assembly check
		float z1= 0;

		
		std::cout << "Especializacion con Arrays, tipo de x es: " << typeid(x).name() << std::endl;
		
		std::cout << "Especializacion con Pares, tipo de y2 es: " << typeid(y2).name() << std::endl;
		
		std::cout << "Especializacion con tuplas, tipo de z es: " << typeid(z).name() << std::endl;

	}
		
}


int main() {


	std::cout << "----------------------Exercise193_1----------------------------------\n\n";
	Exercise193_1::Exercise193_1();


	std::cout << "\n\n----------------------Exercise197_1----------------------------------\n\n";
	Exercise197_1::Exercise197_1();

	return 0;

}