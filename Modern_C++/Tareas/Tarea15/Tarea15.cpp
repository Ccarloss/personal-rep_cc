// Carlos Andres Cordero Retana

// Tarea 15: More about iterators

//  tarea 15: 250-259

#include <iostream>
#include <unordered_set>
#include <set>


/*
250. Exercise:
250.1. **Explain how an iterator can become invalid (see slides 439-
440)
 */

// Si tenemos un puntero apuntando a un cierto elemento del contenedor,
// y este sufre ciertos cambios, el puntero se invalida.

namespace Exercise_250_1
{

	void exercise_250()
	{
		//---------------Caso 1:  Invalidando puntero de un contiguous container  ----------------//

		std::vector vec1{ 3, 5, 6, 10, 33 };

		std::vector<int>::iterator ptr_vec1 = std::next(vec1.begin());

		// Seg�n cppreference:

		// std::push_back:  This effectively increases the container size by one,
		// which causes an automatic reallocation of the allocated storage space
		// if -and only if- the new vector size surpasses the current vector capacity.

		vec1.push_back(44);

		//*** Se genera un reallocation, es decir el puntero SE INVALIDA

		//std::cout << *ptr_vec1 << std::endl;


		//---------------Caso 2:  Invalidando puntero de un node based container ----------------//

		std::unordered_multiset m_set1{ 10, 15, 6, 3, 33 };

		std::unordered_multiset<int>::iterator ptr_set1 = std::next(m_set1.begin());

		auto it = m_set1.find(15);

		// Insertamos el 22 antes del 15 en el multiset
		std::inserter(m_set1,it) = 22;

		// *** Con node based containers (linked lists, trees, hash tables) EL PUNTERO NO SE INVALIDA si el contenedor aumenta de tama�o
		std::cout << *ptr_set1 << std::endl;


		// Pero que pasa si removemos un elemento de un node based container?
		// Se anula el iterador que apuntaba a dicho elemento?
		auto it2 = m_set1.find(3);

		m_set1.erase(15);

		// El iterador it2 no se ve afectado, sigue apuntando a 3, apesar de que eliminamos el valor 15
		std::cout << *it2 << std::endl;

		// Este iterador s� se ve afectado, debido a que apuntaba al 15
		//std::cout << *ptr_set1 << std::endl;

	}
}




/*
259. Exercise:
259.1. **Can back_insert_iterator , front_insert_iterator be used on
associative containers? On unordered containers? Why? How can we
insert elements in these containers? Is there any type of insertion
iterator that works with these containers? Why or why not?
 */

namespace Exercise_259_1
{

	/*Can back_insert_iterator , front_insert_iterator be used on
associative containers? On unordered containers? Why?

	No, debido a que los associative containers y los unordered containers
	no cuentan con las funciones miembro push_back o push_front, las
	cuales son utilizadas por estos iteradores (back_insert_iterator,
	front_insert_iterator). Por lo tanto, Los back_insert_iterator y
	front_insert_iterator son utilizados para insertar elementos en
	la parte trasera o frontal de contenedores secuenciales  */


	void exercise_259_1()
	{
		/*How can we insert elements in these containers?
		 Is there any type of insertion iterator that works
		 with these containers? Why or why not?*/

		// Utilizamos el iterador std::insert_iterator para estos contenedores, debido a que
		// este iterador llama a la funcion miembro insert, la cual s� est� definida en
		// este tipo de contenedores.


		// Seg�n Cppreference, la funcion miembro del contenedor insert() recibe como parametros:

		//pos	-	iterator to the position before which the new element will be inserted
		//value - element value to insert

		std::unordered_multiset s1{ 33, 55, 5, 6,8 };

		// Antes de agregar elementos:
		
		std::cout << "Inicialmente: \n\n";
		for (auto elem : s1) {
			std::cout << elem << " ";
		}
		std::cout << std::endl;


		std::insert_iterator<std::unordered_multiset<int>> itr(s1, s1.begin());

		// As� es como ingresamos valores a los node based containers usando este iterador!
		*itr = 0;
		*itr = 1;
		*itr = 40;
		*itr = 3;


		std::cout << "\nDespues: \n\n";
		for (auto elem : s1) {
			std::cout << elem << " ";
		}
		std::cout << std::endl;

	}

}



int main()
{
	std::cout << "----------------------Exercise250_1----------------------------------\n\n";
	Exercise_250_1::exercise_250();
	std::cout << "\n\n----------------------Exercise259_1----------------------------------\n\n";
	Exercise_259_1::exercise_259_1();

}