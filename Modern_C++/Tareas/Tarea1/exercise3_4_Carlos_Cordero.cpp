#include <iostream>
using namespace std;

int main() {
    // Se declara 2 variables
    int val1, val2;

    // Se le solicita al usuario que ingrese dos numeros
    cout << "Ingrese el primer entero X1: ";
    cin >> val1;

    cout << "Ingrese el segundo entero X2: ";
    cin >> val2;

    // Se calculan todas las operaciones que se observan y se imprimen
    cout << "El menor fue : " << (val1 < val2 ? val1 : val2) << endl;
    cout << "El mayor fue : " << (val1 > val2 ? val1 : val2) << endl;
    cout << "Suma= " << val1 + val2 << endl;
    cout << "Diferencia X1-X2 " << val1 - val2 << endl;
    cout << "Producto: " << val1 * val2 << endl;

    // Debemos garantizar que X2 no sea 0 para que no se levante una excepcion
    if (val2 != 0) {
        // Casting de int --> float
        float val1f = val1;
        float val2f = val2;

        cout << "El ratio seria " << val1f / val2f << endl;
    } else {
        cout << "No se pudo calcular X1/X2 , ya que X2= 0" << endl;
    }

    return 0;
}
