#include <iostream>
using namespace std;

int main(){

    const char* cp = "Hello world";
    // Caso 1
    if(cp && *cp){
        cout << "Ningun operando es Nulo" << endl;
    }

    // Caso 2
    if(cp && nullptr){
        cout << "Ningun operando es Nulo" << endl;
    } else{
        cout << "El puntero es nulo" << endl;
    }

    //Caso 3
    if('\0' && *cp){
        cout << "Ningun operando es Nulo" << endl;
    } else{
        cout << "El caracter al que apunta el puntero es nulo" << endl;
    }

    // Caso 4
    if('A' && 10){
        cout << "Ningun operando es Nulo" << endl;
    }






}