#include <iostream>
using namespace std;

// Función que implementa el algoritmo bubble sort para ordenar el arreglo ingresado
void ordenar(int arreglo[3]) {
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2 - i; ++j) {
            // Comparación e intercambio de posiciones si se cumple la condición
            if (arreglo[j] > arreglo[j + 1]) {
                int temp = arreglo[j];
                arreglo[j] = arreglo[j + 1];
                arreglo[j + 1] = temp;
            }
        }
    }
}

int main() {
    int numerosingresados[3];

    // Ingreso de valores por el usuario
    for (int i = 0; i < 3; ++i) {
        cout << "Ingrese el numero V" << i + 1 << ": ";
        cin >> numerosingresados[i];
    }

    // Llamada a la función para ordenar el arreglo de menor a mayor
    ordenar(numerosingresados);

    // Mostrar el arreglo ordenado
    
    for (int i = 0; i < 3; ++i) {
        cout << numerosingresados[i] << ", ";
    }
    cout << endl;

    return 0;
}

