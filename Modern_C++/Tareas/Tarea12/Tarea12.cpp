// Carlos Andres Cordero Retana

// Tarea 12: Memory leaks con RAW pointers y strong exception guarantee

#include <iostream>
#include <vector>


/*
202.2. **If class ClassB_Naive had only one pointer, could we disregard
smart pointers? What if the constructor throws after the one pointer
has been initialized? So, what is your conclusion regarding the use of
smart pointers to deal with memory leaks?
*/
namespace Exercise202_2 {

	class ClassA {
		int val;
		static int classA_counter;
	public:
		ClassA(int _val) : val{ _val } {
			if (val < 0)
			{
				throw std::system_error(std::make_error_code(std::errc::invalid_argument), "Negative values notallowed in ClassA");
			}
			// si val >0 , creamos el objeto correctamente, y por ende, incrementamos counter
			++classA_counter;
			std::cout << "ClassA object constructed: " << this << std::endl;
		}
		~ClassA() {
			// Quitamos los objetos que fueron creados correctamente de classA_counter
			--classA_counter;  
			std::cout << "ClassA object destructed: " << this << std::endl;
		}
		static int counter() {
			return classA_counter;
		}
	};
	

	
		// Los RAW pointers (punteros crudos), como miembros de una clase, no tienen
		// destructor, a diferencia de los smart pointers.


		// Si hubiera m�s de un puntero crudo como miembro, s� existe peligro de
		// que ocurra un memory leak, ya que puede que ptr1 se cree de forma correcta,
		// pero ptr2 no se cree, debido a una excepci�n generada en la clase A producto
		// de que val2 <0 .


		//Esto provoca que el objeto ClassB_Naive no sea full created, lo cual implica
		//que el destructor de ClassB_Naive nunca se va a llamar, provocando as� el
		//memory leak, ya que nunca se le hizo delete al ptr1.


		// Conclusion:

		// *** Por lo tanto, si solo tenemos un puntero como miembro (como en este caso)
		// y si se le ingresa val1 <0 , no habr� memory leak, debido a que ptr1 no se llega a crear,
		// gracias a la excepci�n lanzada en la clase A, lo cual provoca, a nivel general, que no se
		// cree obj2. Esto ocurre, SIEMPRE y CUANDO la excepci�n sea lanzada antes de que se cree ptr1
		// por completo. 

		// En la imagen consola.png se verifica que no hubo leak al trabajar con un solo puntero crudo.
		// Por ende, para este caso, en el que solo tenemos un miembro ptr, no hace falta trabajar con
		// smart pointers para evitar leaks. No obstante, siempre es buena pr�ctica trabajar con smart
		// pointers.



	class ClassB_Naive {

		ClassA* ptr1;
		//ClassA* ptr2;
	public:
		ClassB_Naive(int val1 /*, int val2*/)
			: ptr1{ new ClassA{val1} } /*, ptr2{ new ClassA{val2} }*/
		{
		}
		// copy ctor
		ClassB_Naive(const ClassB_Naive& o)
			: ptr1{ new ClassA{*o.ptr1} }/*, ptr2{ new ClassA{ *o.ptr2 } }*/ {
		}
		// assignment oper
		ClassB_Naive& operator=(const ClassB_Naive& o) {
			*ptr1 = *o.ptr1;
			//*ptr2 = *o.ptr2;
			return *this;
		}
		~ClassB_Naive() {
			delete ptr1;
			//delete ptr2;
		}
	};

	void showLeakageInClassB_Naive() {

		std::cout << "classA_counter inicialmente: "<< ClassA::counter() << std::endl;
		try {
			ClassB_Naive obj1{ 10 };
			ClassB_Naive obj2{ -3 };
		}
		catch (std::exception err) {
			std::cout << err.what() << std::endl;
		}
		
		std::cout << "Si classA_counter es 0, entonces no hubo leak! :  "<< ClassA::counter() << std::endl;
	}
	// Inicializamos el contador
	int ClassA::classA_counter = 0;

}


/*
203.1. **Give an example of an assignment operator that provides the
strong exception guarantee (explain why slide 366 works)
*/
namespace Exercise203_1
{
	// En este ejemplo, queremos guardar objetos de tipo Value en un espacio de memoria determinado.
	// Es un buen ejemplo de strong exception guarantee, dado que si ocurre una excepcion en el proceso
	// de almacenamiento, la misma funci�n se encarga de eliminar los objetos que se almacenaron
	// correctamente, es decir, los objetos almacenados previos a la excepcion. Ojo, la funcion no elimina
	// el espacio en memoria (no hace delete) , solo se encarga de eliminar los objetos almacenados, gracias
	// a que llama al destructor de cada uno de ellos.


	template<class InputIt, class ForwardIt>
	ForwardIt uuninitialized_copy(InputIt first, InputIt last, ForwardIt d_first)
	{
		// value_type proporciona informaci�n sobre el tipo de valor al que apunta el iterador
		typedef typename std::iterator_traits<ForwardIt>::value_type Value;

		// Creamos otro iterador que sea igual a d_first, para que d_first lo utilicemos en catch,
		ForwardIt current = d_first;
		try {
			for (; first != last; ++first, ++current) {
				// almacenamos objetos de tipo Value en la direccion obtenida de:
				// static_cast<void*>(std::addressof(*current))
				::new (static_cast<void*>(std::addressof(*current))) Value(*first);
			}
			return current;
		}

		// Si ocurre una excepcion en el proceso de almacenamiento, entonces eliminamos
		// los objetos almacenados correctamente.

		// d_first : apunta al primer elemento del contenedor

		// current : apunta al objeto posterior al �ltimo objeto almacenado correctamente
		catch (...) {
			for (; d_first != current; ++d_first) {
				// Llamamos al destructor del objeto actual mediante el iterador que esta apuntando a dicho objeto
				d_first->~Value();
			}
			throw;
		}
	}


	void Exercise203_1()
	{
		// Vector de origen con algunos valores de tipo string
		std::vector<std::string> source = { "hola", "hello", "halo", "XDDDD" };

		// Vector de destino de tipo string con el mismo tama�o que el vector de origen
		std::vector<std::string> destination(source.size());

		// Llamar a uninitialized_copy para copiar los elementos del vector de origen al vector de destino
		uninitialized_copy(source.begin(), source.end(), destination.begin());

		// Imprimir el contenido del vector de destino para verificar si los elementos se copiaron correctamente
		std::cout << "Contenido del vector de destino despu�s de copiar:\n";
		for (const std::string& elem : destination) {
			std::cout << elem << "\n";
		}
	}

}


int main()
{
	std::cout << "----------------------Exercise202_2----------------------------------\n\n";
	using namespace Exercise202_2;
	showLeakageInClassB_Naive();

	std::cout << "\n\n----------------------Exercise203_1----------------------------------\n\n";
	Exercise203_1::Exercise203_1();

}