// Carlos Andres Cordero Retana

// Tarea 10: Advanced templates


/*
164. Exercise:
164.1. **Slide 295 presents two versions of a class called Thing. Explain
the differences, create an object of each and call the method
something().
*/

#include <iostream>

namespace Exercise164_1 {

	// Fin de la recursi�n
	void print() {}


	template <typename T, typename... Args>
	void print(T first, Args... args) {
		std::cout << first << std::endl; // Imprime el primer elemento
		print(args...); // Llama recursivamente para imprimir el resto de los elementos
	}

	class Thing1 {
	public:
		template <class... T_values> // template member function

		//Diferencia #1: Los argumentos son pasados por valor (copia)
		void something(T_values... values) {
			std::cout << "Primera version:\n";
			print(values...);
		}
	};


	template <class... T_values> // template class
	class Thing2 {
	public:
		// Diferencia #1: Los argumentos son constantes y son pasados por referencia
		void something(const T_values&... values) {
			std::cout << "Segunda version:\n";
			print(values...);  
		} 
		
	};

	void Exc164_1() {

		Thing1 obj1;

		
		obj1.something(22, 'A', 15.2);  // el compilador deduce T = int y la cola= char, float

		Thing2<int,char,float> obj2; // Diferencia#2: debemos especificar con cuantos tipos se va a trabajar


		obj2.something(22, 'A', 15.2);
	}

}




/*
169. Exercise:
169.1. ***EXTRA CREDIT: Explain how slides 304 and 305 work!! These
are examples of very advanced variadic templates and meta
programming. Why do we use decay? It�s omnipresent! What does it
do?

*/

namespace Exercise169_1_slide_304 {


	namespace JD {
		// Definimos la clase template tuple_size que recibe solo un tipo T
		template<typename T>
		class tuple_size;

		// Clase template tuple_size que recibe cualquier cantidad de tipos como argumentos
		template< class... Types >
		
		// Se crea un objeto de esta clase cuando se pasa un std::tuple con tipos de datos Types... 
		class tuple_size< std::tuple<Types...> > :
			
			// tuple_size hereda de la clase integral_constant (esta clase proporciona una constante entera en tiempo de comp)
			// Esta constante ser� de tipo size_t y el valor de ella es el calculado por sizeof...(Types)  --> cantidad de tipos de Types...
			public std::integral_constant<std::size_t, sizeof...(Types)> {};
	}

	template<typename Tuple>
	// && ---> referencia universal
	auto test(Tuple&& t)
	{	
		// Usamos estos alias para mayor legibilidad
		using with_decay = std::decay_t<Tuple>;
		using without_decay = Tuple;

		// El compilador no se queja, porque al usar decay, se eliminan los operadores de referencias &&
		// Por ende, el argumento que esperaba la clase tuple_size es el correcto
		auto size_with_decay_std = std::tuple_size<with_decay>::value;//version de tuple_size que esta definida en std
		auto size_with_decay_JD = JD::tuple_size<with_decay>::value; //version de tuple_size que esta definida en JD
		
		std::cout << "El tamano de la tupla usando tuple_size de std:"<<  size_with_decay_JD << std::endl;
		std::cout << "El tamano de la tupla usando tuple_size de JD:" << size_with_decay_std << std::endl;

		// El compilador se queja si no usamos decay, porque se espera que Tuple sea sin referencia
		//auto size_with_decay_std = std::tuple_size<without_decay>::value;
		//auto size_with_decay_JD = JD::tuple_size<without_decay>::value; 
	}
	void aboutDecay()
	{
		auto x = std::tuple<int, int, int>{ 21, 44, 88 };
		test(x);
	}

}



namespace Exercise169_1_slide_305 {


	
		namespace detail {

			//F --> tipo de la funcion
			// Tuple --> tipo de la tupla sobre la que se aplica la funcion
			// std::size_t... I --> secuencia de indices de tipo size_t
			template <class F, class Tuple, std::size_t... I>
			
			// La funcion recibe una funcion de la clase F, una tupla de la clase Tuple y una secuencia de indices de tipo I
			// Aplicamos la funcion f a los elementos de la tupla usando std::invoke
			constexpr decltype(auto) apply_impl(F&& f, Tuple&& t, std::index_sequence<I...>)
			{
				//std::get: accede al elemento en la tupla t en la posici�n especificada por el �ndice I. 
				//Usamos forward para asegurarnos que se mantenga la categor�a de valor original de t (no perder rvalues)
				// std::invoke aplica la funci�n f a cada elemento de la tupla t. 
				// Esto se logra mediante la expansi�n de par�metros (...) que pasa cada elemento individualmente como 
				// un argumento a f.
				return std::invoke(std::forward<F>(f), std::get<I>(std::forward<Tuple>(t))...);
			}
		} 

		template <class F, class Tuple>
		constexpr decltype(auto) apply(F&& f, Tuple&& t)
		{	
			// Llama a la funci�n interna apply_impl con la funci�n f, la tupla t,
           // y una secuencia de �ndices que representan las posiciones en la tupla.
			return detail::apply_impl(std::forward<F>(f), std::forward<Tuple>(t),
				std::make_index_sequence < std::tuple_size<std::decay_t<Tuple>>::value > {});
		}


}






/*
165. Exercise (Extra):
165.1. **Slide 296 presents a variadic method calling another variadic
function called �something_else�. Explain what the syntax �values��
means in that call.

*/
namespace Exercise165_1 {

	// Fin de la recursi�n
	void something_else() {}


	template <typename T, typename... Args>
	void something_else(T first, Args... args) {
		std::cout << first << std::endl;
		print(args...);
	}

	template <class... T_values>
	class Thing {
	public:
		// Si los 3 puntos estan al lado izquierdo de values, significa "empaque lo que el usuario ingres� en values"
		void something(T_values... values) {
			// Si los 3 puntos estan al lado derecho significa: desempaque lo que haya en la variable values
			something_else(values...);
		};
	};


}

int main() {

	std::cout << "--------------------------------Ejercicio 164_1-------------------------------------" << "\n\n";
	Exercise164_1::Exc164_1();

	std::cout << "\n\n--------------------------------Ejercicio 169_1-------------------------------------" << "\n\n";
	Exercise169_1_slide_304::aboutDecay();



}

