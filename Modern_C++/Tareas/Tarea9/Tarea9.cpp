// Carlos Andres Cordero Retana

// Tarea 9:  Libreria Chronos



#include <iostream>
#include <string>
#include <chrono>


using namespace std::literals;


/* 
157.1. *A duration can be cast to different units, like days, hours,
minutes, etc. Look at slide 281. Notice how we define the Days
duration which is given as a compile time ratio in seconds and based
on the int representation. Take two year_month_day values, convert
them to sys_days and subtract them to get a duration; then express
the duration in days, hours, minutes, etc (see slide 281). Also, take the
current time as given by now and subtract it from the time_point
corresponding to the epoch and express that in years, months, days,
hours, minutes, ...like this:

auto tp = system_clock::now();
// EPOCH:
auto tp1 = system_clock::time_point{};
auto dif = tp - tp1;
auto ms_epoch = duration_cast<milliseconds>(dif);
extractFromMs(ms_epoch);

You must define the function extractFromMs(
std::chrono::milliseconds ms) based on the solution of exercise 155
in tarea8.cpp.

*/

namespace Exercise157_1
{ 
	
	// Esta funcion devuelve un punto en el tiempo definido por el usuario
	inline std::chrono::system_clock::time_point makeTimePoint
	(int year, int mon, int day, int hour, int min, int sec = 0)
	{
		struct std::tm t;
		t.tm_sec = sec; // second of minute (0 .. 59 and 60 for leap seconds)
		t.tm_min = min; // minute of hour (0 .. 59)
		t.tm_hour = hour; // hour of day (0 .. 23)
		t.tm_mday = day; // day of month (1 .. 31)
		t.tm_mon = mon - 1; // month of year (0 .. 11)
		t.tm_year = year - 1900; // year since 1900
		t.tm_isdst = -1; // determine whether daylight saving time
		std::time_t tt = std::mktime(&t);
		if (tt == -1) {
			throw "no valid system time";
		}
		return std::chrono::system_clock::from_time_t(tt);
	}


	void Exercise157_1() {

		// Creamos 2 time points
		const std::chrono::time_point<std::chrono::system_clock> TP1 = makeTimePoint(2001, 1, 1, 11, 40);
		const std::chrono::time_point<std::chrono::system_clock> TP2 = makeTimePoint(2023, 1, 1, 11, 40);

		// Creamos la primer year_month_day
		const std::chrono::year_month_day ymd1{ std::chrono::floor<std::chrono::days>(TP1) };
		const auto start_day = std::chrono::sys_days{ ymd1 };

		// Creamos la segunda year_month_day
		const std::chrono::year_month_day ymd2{ std::chrono::floor<std::chrono::days>(TP2) };
		const auto end_day = std::chrono::sys_days{ ymd2 };

		// Calculamos la cantidad de dias
		auto dif = end_day- start_day;


		auto seconds = std::chrono::duration_cast<std::chrono::seconds>(dif);
		
		// Convertir la duraci�n de d�as a a�os 
		auto years = std::chrono::duration_cast<std::chrono::years>(seconds);


		// Convertir la duraci�n de d�as a meses 
		auto meses = std::chrono::duration_cast<std::chrono::months>(seconds);
		

		// Convertir la duraci�n de d�as a horas 
		auto horas = std::chrono::duration_cast<std::chrono::hours>(dif);


		// Convertir la duraci�n de d�as a minutos
		auto minutos = std::chrono::duration_cast<std::chrono::minutes>(dif);

		std::cout << "Fecha inicial : " << ymd1 << std::endl;
		std::cout << "Fecha final : " << ymd2 << std::endl;

		std::cout << "\n\nHan pasado : " << seconds << " (segundos)" << std::endl;
		std::cout << "Han pasado : " << horas << " (horas)" << std::endl;
		std::cout << "Han pasado : " << minutos << " (minutos)" << std::endl;
		std::cout << "Han pasado : " << dif << " (dias)" << std::endl;
		std::cout << "Han pasado : " << meses << " (meses)" << std::endl;
		std::cout << "Han pasado : " << years << " (anos)" << std::endl;		

	}
}


namespace Exercise157_3
{	
	

	// Esta funci�n devuelve el d�a actual como un objeto de tipo std::chrono::sys_days
	std::chrono::sys_days getToday()
	{
		// Se obtiene la hora actual del sistema en la zona horaria local
		auto local_now = std::chrono::current_zone()->to_local(std::chrono::system_clock::now());

		// Se convierte el tiempo actual de segundos a d�as y se almacena en un objeto year_month_day
		std::chrono::year_month_day ymd{ std::chrono::floor<std::chrono::days>(local_now) };

		// Se asigna el objeto year_month_day a un objeto de tipo std::chrono::sys_days (esto me permite sumarle dias!)
		std::chrono::sys_days today = ymd;

		// Se devuelve el d�a actual como un objeto std::chrono::sys_days
		return today;
	}


	using Days = std::chrono::duration<int, std::ratio<3600 * 24>>;
	inline constexpr Days operator "" _day(unsigned long long _Val)	// user defined literals
	{	// return integral days
	return (Days{ _Val });
	}

	void Exercise157_3() {

		auto today = getToday();
		// Se llama a la funci�n operador ""_day para que me devuelva 5 dias
		// Luego, le sumo a today esos 5 dias
		today += 5_day;
		std::cout << today << std::endl;
	}


}


/*
158. ***Explain what are calendrical types and how they may be used (see
299, 303, 304 and 305 and others in Tour of C++ v3.pptx) by answering
the following questions:

*/

namespace Exercise158
{
	//Show how to calculate today�s date (without hours, minutes and
	//seconds) in our local time zone.
	//Show how to calculate the current local time

	void today_date() {

		// Calculamos el current local time
		auto now = std::chrono::current_zone()->to_local(std::chrono::system_clock::now());

		std::chrono::year_month_day ymd{ floor<std::chrono::days>(now) };
		std::cout << "Fecha de hoy: " << ymd << '\n';
	}

	//Show how to calculate time since midnight in
	//hours::minutes::seconds.
	
	void time_since_midnight()

	{	// Time point actual en segundos, dado que usamos el clock --> system_clock
		auto current_time_point = std::chrono::current_zone()->to_local(std::chrono::system_clock::now());
		
		// Time point a medianoche, gracias a que floor redondea al inicio del dia actual (midnight)
		auto midnignt_time_point = floor<std::chrono::days>(current_time_point);
		
		
		auto diff = current_time_point - midnignt_time_point;

		auto segundos = std::chrono::duration_cast<std::chrono::seconds>(diff);

		// Convertir la duraci�n de d�as a horas 
		auto horitas = std::chrono::duration_cast<std::chrono::hours>(diff);


		// Convertir la duraci�n de d�as a minutos
		auto minutitos = std::chrono::duration_cast<std::chrono::minutes>(diff);

		std::cout << "\n\nDesde medianoche:\n";
		std::cout << "Han pasado:" << segundos << std::endl;
		std::cout << "Han pasado:" << minutitos << std::endl;
		std::cout << "Han pasado:" << horitas << std::endl;	
	}

	// Show how to print every second Tuesday of the current year
	void every_second_tuesday_this_year()
	{
		constexpr std::chrono::weekday_indexed wi = std::chrono::Tuesday[2];

		std::chrono::year_month_weekday ymwd = 2024y / std::chrono::January / wi;

		for (auto d = ymwd; d.year() == ymwd.year(); d += std::chrono::months{ 1 }) {

			//Para que no se nos imprima year/month/ Tuesday[2], hacemos la conversion!
		   std::chrono::year_month_day ymd{ d };
		   std::cout << ymd << "\n";
		}
	}



	// Show how to print every 30th of every month this current year

	void every_30_this_year() {

		// Establece la fecha de inicio como el 31 de enero de 2024.
		std::chrono::year_month_day first = 2024y / 1 / 31;

		// Itera sobre cada mes del a�o 2024.
		for (auto d = first; d.year() == first.year(); d += std::chrono::months{ 1 })
		{
			std::chrono::year_month_day dcorrected = d;

			// Verifica si la fecha actual es v�lida
			if (!d.ok())
			{
				// Si la fecha no es v�lida, se calcula el �ltimo d�a del mes correspondiente.
				auto ymd = d.year() / d.month() / std::chrono::last;
				dcorrected = ymd;
				std::cout << dcorrected << std::endl;
			}
			else
			{
				std::cout << d << ":\n"; // Imprime la fecha actual si es v�lida.
			}

		}
	}


	//Explain what system_clock::now() returns � test it! Why do we need
	//to_local() ? What happens if we call now() after 6pm Costa Rica time ?

	//  system_clock::now()  devuelve un time point que representa el tiempo actual.
	// Cuando utilizamos to_local, convierte la hora a la zona horaria correcta GMT-6.
	// De lo contrario, system_clock::now() devuelve un time_point relativo a GMT+0, 
	// que seria el del reino unido

	void testing_now_and_local() {

		// system_clock::now() devuelve un time_point relativo a GMT+0, Reino Unido
		auto hora_sistema = std::chrono::system_clock::now();

		// Cuando se ejecuta to_local, convierte la hora a la zona horaria correcta GMT-6 
		auto hora_local = std::chrono::current_zone()->to_local(hora_sistema);
		std::cout << std::format("La hora local correcta es {:%d/%m/%y %H:%M}  (EN CR)", hora_local);


		// Si la hora est� cerca del final del d�a (>6pm para CR), la fecha devolver� el d�a siguiente 
		// ya que es el siguiente d�a en Reino Unido. (Actualmente son mas de las 6, let's test it)
		std::cout << std::format("\nLa hora del sistema es {:%d/%m/%y %H:%M} (EN UK)\n", hora_sistema);

	}

	void africa_time_zones() {

		// Se obtiene la lista de bases de datos de zonas horarias disponibles.
		auto& lista_zonas_horarias = std::chrono::get_tzdb_list();

		// Se selecciona la primera base de datos de zonas horarias de la lista.
		auto& primera_zona_horaria = lista_zonas_horarias.front();

		// Se recorre cada zona horaria dentro de la base de datos.
		for (auto& zona : primera_zona_horaria.zones)
		{
			// Se verifica si el nombre de la zona horaria contiene "Africa/".
			if (zona.name().find("Africa/") != std::string::npos) {
				// Si contiene "Africa/", se imprime el nombre de la zona horaria.
				std::cout << zona.name() << std::endl;
			}
		}
	}

}




int main() {

	std::cout << "-------------------------Excercise157_1---------------------------\n\n" << std::endl;
	Exercise157_1::Exercise157_1();


	std::cout << "\n\n-------------------------Excercise157_3---------------------------\n\n" << std::endl;
	Exercise157_3::Exercise157_3();


	std::cout << "\n\n-------------------------Excercise158---------------------------\n\n" << std::endl;
	Exercise158::today_date();

	std::cout << "\n\n";
	Exercise158::time_since_midnight();

	std::cout << "\n\n El segundo martes de cada mes del 2024: \n";
	Exercise158::every_second_tuesday_this_year();
	
	std::cout << "\n\n Cada fin de mes del 2024 es: \n";
	Exercise158::every_30_this_year();

	std::cout << "\n\n Note que despues de las 6pm se coloca el dia siguiente!\n";
	Exercise158::testing_now_and_local();

	std::cout << "\n\n Time zones de africa!\n";
	Exercise158::africa_time_zones();

	return 0;

}
