#include <iostream>


int main(){


int x[10]; // Se inicializa el array de enteros

for (int* px = x; px < &x[10]; ++px) {
    *px *= 2;   // se duplica 
    std::cout << std::hex << *px << std::endl; // se imprime en formato e
}

// cx apunta al primer byte del array x interpretado como un array de caracteres (char). 
//Aquí se utiliza reinterpret_cast para realizar CASTING
char* cx = reinterpret_cast<char*>(x); 

//  Se declara un puntero one_past_end que apunta al byte justo después del final del array x. 
//Esto se utiliza para determinar el final del bucle en el siguiente paso.
const char* const one_past_end = reinterpret_cast<char*>(&x[10]);


/*recorre el array x byte por byte. En cada iteración, se interpreta el byte actual como
 un puntero a un entero (int* pInteger). Luego, se lee el valor apuntado por ese puntero y 
 se imprime en formato hexadecimal.*/
for (; cx < one_past_end; cx += sizeof(int)) {

int* pInteger = reinterpret_cast<int*>(cx);

auto val = *pInteger;

std::cout << std::hex << val << std::endl;
}

}
