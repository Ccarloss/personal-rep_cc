// Carlos Andrés Cordero Retana

//Exercise 70.1
/*
70.1. *What is the difference between a class and a struct? 

La diferencia principal entre una clase y una estructura en C++ radica en 
el nivel de acceso predeterminado. En una clase, los miembros son privados
por defecto, mientras que en una estructura son públicos por defecto.


70.1 Can we embed classes or structs in other classes or structs? If so, are these
nested types visible from the outside of the enclosing type and if so, is
this the best design?

Una clase o estructura también puede contener otra definición de clase/estructura 
dentro de sí misma, lo cual se denomina "nested class"; en esta situación, la 
clase que la contiene se conoce como "enclosing class". 


Para responder esta pregunta, se crea el ejemplo con la clase SistemaParticulas.

-> Los nested class sí son visibles desde el exterior, SIEMPRE Y CUANDO se le indique
el acceso como público. Tal y como se puede observar en esta linea de codigo:

SistemaParticulas::Particula particulaindividual;

-> Dado que sí es posible, dependiendo de la aplicación, se podría considerar como
un buen diseño o como un mal diseño.

*/


#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
using namespace std;

class SistemaParticulas{
    public:
    struct Particula{
        float x{0.0f};
        float y{0.0f};
        float z{0.0f};
        float speed{1.0f};

        // Mueve particulas aleatoriamente
        void Move() { 
            x += (rand() % 3 - 1) * speed;  
            y += (rand() % 3 - 1) * speed;
            z += (rand() % 3 - 1) * speed;}
    };

    
    void Simulacion(){
            for(size_t i = 0; i < m_particulas.size(); ++i){
                m_particulas[i].Move();

            }
    }

    std::vector<Particula> m_particulas;
};



class Class1 {
    
public:    //Por default son privados
    char a;
    int b;

public:

    // Metodo1
    void fun1() {
        cout << "Se ejecuta la funcion1 de el struct 1" << endl;
    }

    // Constructor clase 1
    Class1() : a('A'), b(17) {
        cout << "\n\nSe ejecuta el constructor de la Clase1" << endl;
    }

    // Destructor de clase 1
    ~Class1() {
        cout << "Se ejecuta el destructor de la Clase1" << endl;
    }
};



struct Struct1 {

    char a;
    int b;


    // Metodo1
    void fun1() {
        cout << "Se ejecuta la funcion1 de el struct 1" << std::endl;
    }

    // Constructor 
    Struct1() : a('A'), b(18) {
        cout << "\n\nSe ejecuta el Constructor de el struct 1" << std::endl;
    }

    // Destructor 
    ~Struct1() {
        cout << "Se ejecuta el destructor de el struct 1" << std::endl;
    }
};



int main() {


    // Crear un sistema de partículas
    SistemaParticulas sistema;

    // Inicializar el vector de partículas con 5 partículas
    for (int i = 0; i < 5; ++i) {
        sistema.m_particulas.push_back(SistemaParticulas::Particula());
    }

    // Se aplica el metodo Simulacion al objeto sistema
    sistema.Simulacion();

    // Imprimir las posiciones de las partículas
    for (size_t i = 0; i < sistema.m_particulas.size(); ++i) {
        cout << "Particula  " << i + 1 << ": ";
        cout << "X=" << sistema.m_particulas[i].x << ", ";
        cout << "Y=" << sistema.m_particulas[i].y << ", ";
        cout << "Z=" << sistema.m_particulas[i].z << std::endl;
    }

    // SI PODEMOS ACCEDER AL NESTED Struct 
    SistemaParticulas::Particula particulaindividual;
    particulaindividual.Move();
    cout << "\n\nParticula Individual: ";
    cout << "X=" << particulaindividual.x << ", ";
    cout << "Y=" << particulaindividual.y << ", ";
    cout << "Z=" << particulaindividual.z << std::endl;



    // Instancia de la clase Struct1
    Struct1 myObjStruct;
    myObjStruct.fun1();
    cout << "Los miembros del struct creada fueron a = " << myObjStruct.a << " y b = " << myObjStruct.b<<"\n\n";

    // Instancia de la clase Class1
    Class1 myObjClass;
    myObjClass.fun1();
    cout << "Los miembros de la clase creada fueron a = " << myObjClass.a << " y b = " << myObjClass.b<< "\n\n";

    return 0;
}


