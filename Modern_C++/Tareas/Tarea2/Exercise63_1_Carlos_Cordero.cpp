// Carlos Andrés Cordero Retana


//63.1 Exercise:
/*
a) **Dynamically allocated arrays use the new keyword; what
keyword is used to de-allocate? 

R/ 

En el ejemplo mostrado, se liberan 5 espacios del heap con el keyword: delete


b) Is there a difference between arrays and non-arrays as to how they are created or destroyed?

R/
Con el keyword new, se obtiene la dirección del heap del primer elemento (en el caso de un array) 
o del espacio para una variable determinada (non-arrays). La diferencia estaría en la sintaxis de
cómo se crean y se destruyen:

.                            Arrays:                                       vs             non-arrays         

Created:          pointerVariable = new dataType[# of elements];                  pointerVariable = new dataType;

Destroyed:        delete[] pointerVariable;                                       delete[] pointerVariable;      
*/


#include <iostream>
#include <array>

int main(){
    int num;
    std::cout << "Ingrese la cantidad de elementos del array tipo entero ";
    std::cin >> num;
    
    // Se reserva espacio para un arreglo de 5 elementos int en el heap
    int * arraypointer = new int[num];

    // Se declara y se inicializa el array cuyos valores se almacenaran en el heap
    std :: array < int, 5 > arry = {0, 1, 2, 8, 4};

    std :: cout<< "\n\nIngrese cada valor entero del array para almacenarlo en el heap:"<< std::endl;

    // Se colocan los valores del array en los 5 espacios del heap
    for (int i = 0; i < num; ++i){
        std:: cout << "Elemento "<< i + 1 << ": ";
        std:: cin >> *(arraypointer + i);
    }

    std:: cout << "\nEl array se almacena en el heap exitosamente :)\n\n" ;

    for(int i= 0; i < num; ++i){
        std:: cout << "Elemento [" << i << "] : " << *(arraypointer+i) << std::endl;
    }


    // Se liberan los 5 espacios de la memoria dinamica empleada
    delete[] arraypointer;


    
    // Se crea un espacio en el heap para almacenar el entero 22
    int * intpointer = new int;
    *intpointer = 22;

    std::cout<< "\nValor almacenado en el heap: " << *intpointer << std::endl;

    // Se libera el espacio de memoria utilizado
    delete intpointer;
   

    return 0;
}