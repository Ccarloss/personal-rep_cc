// Carlos Andrés Cordero Retana

// Exercises 68.1 (Extra)  and  *68.2

/*

68.1. (Extra)
A class is a unit of cohesive functionality which usually has an
associated state. Describe which kinds of elements can appear in a class 
definition. Why are certain parts defined in the private sector, the protected
 sector or the public sector?
 
 R/ 
 
 Como se puede observar en el código, una clase típicamente esta compuesta por:

 -> Especificadores de acceso (publicos, privados, protegidos)
 -> Atributos
 -> Metodos o funciones (hay dos metodos típicos: contructor y destructor)

Porque se busca limitar el acceso al programador cuando este desea utilizar los
diferentes métodos y atributos de alguna clase madre en su clase derivada.
 
Asimismo, se busca limitar el acceso a la información de todos los objetos creados.
Por ejemplo, en el ejemplo propuesto, el programador solo tiene acceso a saber el
nombre de los gatos, mas no su edad, ya que este atributo se puede accesar solo
dentro de la clase Mascota.




 *68.2. Classes can have data that is particular to each class instance
(or object), and also data that is shared by all instances of a class. This
latter is declared static. Define a class with one static data member.

R/ El elemento estático fue el atributo: nacionalidad. Se comprobó que los 3
objetos creados comparten el mismo valor en el atributo.

 */

 #include <iostream>
 using namespace std;



class Mascota {

    // Atributos
    public:
        static string nacionalidad;

    protected:
        string nombre;

    private:
        int edad;

    // Métodos
    public:
        void setEdad(int age){
            edad = age; // Dentro de la misma clase, se pueden acceder a elementos privados
        }   
 };


// Se inicializa el static data member  afuera de la clase
string Mascota::nacionalidad = "Costarricense";


// Clase derivada: Gato
class Gato : public Mascota {

    public:

        void setNombre(string name){
            nombre = name; // Las clase derivada tiene acceso a elementos protegidos de la clase Mascota
        }
        string getNombre(){
            return nombre;
        }
};



int main(){

    Gato gato1;
    gato1.setNombre("Jack"); // El metodo es público, el usuario tiene acceso
    gato1.setEdad(3);        // Este metodo es publico, el usuario tiene acceso

    // Afuera de la clase, el usuario no tiene acceso a los atributos nombre ni edad. 
    // Pero si tiene acceso al metodo getNombre  porque es publico

    cout << "El gato se llama " << gato1.getNombre() << endl;


    // Verificamos el funcionamiento del atributo estático
    Gato gato2;
    Gato gato3;
    cout << "\n\nEl atributo estatico del objeto 1 (gato 1) es " << gato1.nacionalidad << endl;
    cout << "El atributo estatico del objeto 2 (gato 2) es " << gato2.nacionalidad << endl;
    cout << "El atributo estatico del objeto 3 (gato 3) es " << gato3.nacionalidad << endl;
    

    // Es posible actualizar el valor estático de todos los objetos creados, con solo cambiar el de uno de ellos
    gato3.nacionalidad = "Peruano";
    cout << "\n\nEl atributo estatico actualizado del objeto 1 (gato 1) es " << gato1.nacionalidad << endl;
    cout << "El atributo estatico actualizado del objeto 2 (gato 2) es " << gato2.nacionalidad << endl;
    cout << "El atributo estatico actualizado del objeto 3 (gato 3) es " << gato3.nacionalidad << endl;

}



