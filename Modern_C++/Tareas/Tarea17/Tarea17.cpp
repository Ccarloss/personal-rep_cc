// Carlos Andres Cordero Retana

// Tarea 17: Formatted output and input parsing


#include <iostream>
#include <chrono>

/*
284. Exercise:
**Format integral values in several ways according to the integral
specifications in slide 509.
 */


namespace Exercise_284
{
	using namespace std;
	using namespace std::literals;

	void Exercise_284()
	{
		
		int valor = 25;
		// Imprimir el valor en hexadecimal, octal y binario
		cout << format("25 en hexadecimal: {:#X}\n" , valor);
		cout << format("25 en octal: {:#o}\n", valor);
		cout << format("25 en binario: {:#b}\n", valor);


		// Utilizar relleno y justificaci�n
		int value = 44;
		cout << format("{:^<8}\n", value );
		

		// Imprimir valores booleanos
		bool verdadero = false;
		bool falso = true;
		cout << format("verdadero: {:c}, falso: {:c}\n", verdadero ? 'V' : 'F', falso ? 'V' : 'F');
	}

}



/*
285. Exercise:
**Format floating point values in both fixed decimal and exponential
notation using the specifiers in slide 510-511.

 */


namespace Exercise_285
{
	using namespace std;
	using namespace std::literals;

	void Exercise_285()
	{
		double valor = 12345.6789;

		// Imprimir valor en notaci�n decimal fija
		cout << format("Valor en notaci�n fija: {:.2f}\n", valor);

		// Imprimir valor en notaci�n exponencial
		cout << format("Valor en notaci�n exponencial: {:.2E}\n", valor);
	}
}


/*
286. Exercise:
**Format dates using the conversion specifiers in slide 512 � use as
many as possible.
 */


namespace Exercise_286
{
	using namespace std;
	namespace chr = std::chrono;

	void Exercise_286()
	{
		auto local_now = chr::current_zone()->to_local(chr::system_clock::now());
		// Imprimir la fecha y hora local con formato completo
		cout << format("Fecha y hora local completa: {0:%c}\n", local_now);
		cout << format("D�a de la semana: {0:%A}\n", local_now);
		cout << format("Fecha (YYYY-MM-DD): {0:%F}\n", local_now);
		cout << format("Hora (HH:MM:SS): {0:%T}\n", local_now);

		// Imprimir la fecha y la semana del a�o actual
		cout << format("Estamos en la semana {0:%W} del ano {0:%Y}\n", local_now);
	}
}




int main()
{
	std::cout << "\n\n********************__Exercise_284__*******************\n\n" << std::endl;
	Exercise_284::Exercise_284();
	std::cout << "\n\n********************__Exercise_285__*******************\n\n" << std::endl;
	Exercise_285::Exercise_285();
	std::cout << "\n\n********************__Exercise_286__*******************\n\n" << std::endl;
	Exercise_286::Exercise_286();

}