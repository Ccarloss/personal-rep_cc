#-------------------- Universidad de Costa Rica ---------------------------#
#---------------------Escuela Ingeneria El�ctrica --------------------------#
#-------------- Estructuras de Computadoras Digitales I ---------------#

# Autor: Carlos Andr�s Cordero Retana - B92317

# Tarea 03 y 04 - I Ciclo 2022

# Proposito del codigo: Crear una calculadora polaca inversa, la cual realiza las operaciones de suma, resta, multiplicaci�n, divisi�n y raiz cuadrada. 
#Para ello, el c�digo utiliza tres funciones esencialmente: la primera revisa si el string ingresado est� correcto, la segunda le asigna el signo al string ingresado
# (en caso de que el string fuese un n�mero) y la tercera guarda los valores ingresados como flotantes en la pila. De esta forma, la primera funci�n s�lo tiene que llamar
# a los valores que se ingresaron en la pila por la tercera funci�n, para realizar la operaci�n que el usuario solicite.
.data 
	Mensajeinicial: .asciiz "\n \n  ------------------------Calculadora Polaca Inversa ----------------------------- \n \n Los signos de las operaciones son los siguientes: \n \n suma '+' \n\n resta '-' \n\n multiplicaci�n '*' \n \n division '/'  \n \n  Raiz Cuadrada 'S o s' \n \n Para borrar toda la pila y poner cero como �ltimo elemento apilado 'C o c' \n \n Mostrar el �ltimo resultado 'space'  \n \n Debe ingresar los operandos y operadores de manera secuencial.Por ejemplo: \n\n 241 enter \n 7 enter \n S enter \n 2,64575 \n + enter \n 243,645751\n"
	espacio: .asciiz "\n"
	ERROR: .asciiz "\n Error: Caracter incorrecto\n"
	input: .asciiz 
        
.text

addi $v0, $0, 4                                 # Se imprime el mensaje inicial
la $a0, Mensajeinicial                        
syscall


main:
	
	
	la $a0, input                          #  En a0 se guarda el string ingresado en input
	addi $a1, $0, 50000                  
	addi $v0, $0, 8                        
	syscall       
	
	                                                       
	addi $t9, $t9, 0                       # Contador de elementos que habr�n en la pila
	
	
	jal revisiondecadastring               # funci�n que revisa si el input ingresado es v�lido. De igual forma, realiza las operaciones suma, resta , multiplicaci�n, divisi�n y ra�z cuadrada,
	                                       # al tomar los  valores ingresados en la pila como flotantes, por la funci�n 'conversionstringanumero'
	                                       
	jal verificarsihaymenos                # Esta funci�n le asigna el signo al string ingresado por el usuario 
	
	jal conversionstringanumero            # En esta funci�n se convierte el string ingresado por el usuario en flotante. Dicho valor se guarda en la pila, para que pueda ser utilizado por la 
	                                       # funci�n 'revisiondecadastring'
	
	                                       
verificarsihaymenos:
        add $s6, $0, $a0                      # Guardamos la direcci�n del array en s6
        add $s7, $0, $a3                      # Guardamos la direcci�n del array en s7  
                 
	add $s4, $0, $0                       # Indicador si es positivo o negativo
	lb $t0, 0($a0)                        # Guardamos en t0 el valor de A[i]
	bne $t0, '-', positivo                # Si el signo es diferente de negativo, entonces s4 es positivo
	addi $s4,$0, -1                       # Si $s4=-1 significa que el n�mero es negativo
	addi $s6, $s6, 1                      # Avanzo al siguiente valor del String, ya que el primer digito llevar�a el menos
	addi $s7, $s7, -1                     # Solo cuento la cantidad de n�meros, ignoro el primer string que ser�a un -
	jr $ra

positivo:
        addi $s4, $0, 1                       # Si S4 es 1, significa que el numero ingresado no tiene un menos
        jr $ra
		


conversionstringanumero:

	add $t3, $s7,$0                       #Almacenamos el numero de elementos en t3
	add $t2, $s6, $0                      # Almacenamos en t2 la direccion a0
	addi $t4, $0, 1			      # i=1
	
	sub $t5, $t3, $t4                     # NUMELEM- i inicial
	
	add $s3, $0, $0                       # Sumador =0
	
        addi $t6, $0, 1			      # multiplicador=1  
         
        addi $t8, $0, -1                      # limite t8=-1								
loopconversion:
        
        beq $t5, $t8, finconversion
         
        add $t7, $t2, $t5                     #Direccion t7= a0 + NUMELEM-i
	
	lb $t0, 0($t7)                        # Cargamos en $t7 el valor en A[i]
	
	sub $t0, $t0, 48
	mul $t0, $t0, $t6                     # A[i] * 10 o 100 o 1000 dependiendo de las unidades
	
	add $s3, $s3, $t0                     # Vamos sumando, por ejemplo, para un numero de 4 cifras:  A[i]*1000 + A[i+1]*100 + A[i+2]*10 +  A[i+3]*1
	
	mul $t6, $t6, 10                      #  aumentamos el multiplicador en *10
	
	addi $t4, $t4, 1                      # i= i +1
	sub $t5, $t3, $t4                     # NUMELEM- i 
	j loopconversion
	
	
finconversion:
	mul $s3, $s3, $s4                     #Agregamos  el signo al resultado en s3        
	add $v0, $0, $s3                      #Guardo en $v0 el valor almacenado en $s3                                 
	mtc1 $v0, $f1                         # Convierto el numero entero ingresado a flotante
  	cvt.s.w $f1,$f1                     
	addi $t9, $t0, 1                      # N�mero de elementos en pila
	addi $sp, $sp, -4                     # Solicitamos un espacio en pila
	s.s $f1, 0($sp)                  
	j main	              



revisiondecadastring:     

               addi $t7, $0, 0                 # i= 0
	       addi $t8, $0, 0                 # t8= null

LoopNUMELEM:

	       add $t6, $t7, $a0               # Guardamos en t6 = A0 +i
	       lbu $t6, 0($t6)                 #Guardo en $t2 el valor de String[i}
	       beq $t8, $t6, finnumelem        # A[i] =0 , entonces ya se recorri� todo el string
	       addi $t7, $t7, 1                #i= i+1
	       j LoopNUMELEM
finnumelem :
	       addi $t7, $t7, -1               #Se resta 1, para tener el numero total de elementos correcto
	       addi $a3, $t7, 0                #Guardo en $a3 el numero de elementos que tengo en la pila, para emplearlo en la funcion vaciarpila
	
	
	             
	       beq $a3, $0, main                #Si no hay nada en el arreglo vuelvo a calculadora
	                    
	       addi $t0, $0, 0                  #Contador en i=0
	       addi $t3, $a3, 0                 #cargo en $t3 la cantidad de n�meros del arreglo 
Loopvalido:
	                     
	       add $t1, $t0, $a0                #$t1= a0 + i
	       lbu $t1, 0($t1)                                         
	        
	       addi $t2, $0, 83                 #Si el string es s o S entonces el caracter es valido
	       beq $t1, $t2, correcto
	       addi $t2, $0, 115
	       beq $t1, $t2, correcto
	                                                                 
		
	       addi $t2, $0, '+'                 #Si el string es + entonces el caracter es valido
	       beq $t1, $t2, correcto
	
	       addi $t2, $0, '-'                 #Si el string es - entonces el caracter es valido             
	       beq $t1, $t2, correcto                     
	
               addi $t2, $0, 42                  #Si el string es * entonces el caracter es valido  
	       beq $t1, $t2, correcto 
	
	       addi $t2, $0, 47                   #Si el string es / entonces el caracter es valido
	       beq $t1, $t2, correcto 
		
	       addi $t2, $0, ' '                  #Si el string es [space] entonces el caracter es valido
	       beq $t1, $t2, correcto 
		                                       
	       addi $t2, $0, 67                   #Si el string es c o C entonces el caracter es valido
	       beq $t1, $t2, correcto 
	       addi $t2, $0, 99
	       beq $t1, $t2, correcto 
		
	       addi $t2, $0, '0'
	       sltu $t2, $t1, $t2                 # Pone 1 en $t2 si el elemento A[i] es menor que "0" 
	       bne $t2, $0,  incorrecto           #Si es t2=1, entonces A[i] <0 y el elemento es invalido
	       addi $t2, $0, '9'
	       sleu $t2, $t1, $t2                 # Si A[i] <= 9 entonces A[i] es valido
	       bne $t2, $0, correcto  
		
		j incorrecto
		
correcto :
	       addi $t0, $t0, 1                  #i= i+1   para leer el siguiente elemento del array
	       beq $t0, $t3, identificaroperador #Si el contador t0 es igual al n�mero de elementos t3 ,  el string se ha recorrido por completo
	       j Loopvalido                      # y se verifica si el string ingresado es una operaci�n en 'identificaroperador' 
                                                	
incorrecto:
		addi $v0,$0,4                    
		la $a0,ERROR                     #Si el caracter no es correcto, se imprime un mensaje de error	
		syscall                           
		addi $v0, $0, 10
		syscall
                                               
                                                  #Esta funci�n verifica si el string que se ingresa es una operaci�n  
identificaroperador:

	addi $t0, $0, 1
	bne  $a3, $t0, finidentificar             #Si el numero de elementos no es igual a 1, entonces no puede ser una operacion
	lbu $t1, 0($a0)                           #Guardo en $t1 el valor de Operador[i]	
                                                                                
	addi $t2, $0, 83                         
	beq $t1, $t2, raiz                        #Verificamos si el string ingresado corresponde al operador 'S' o 's'
	addi $t2, $0, 115                        
	beq $t1, $t2, raiz
	
	addi $t2, $0, '+'
	beq $t1, $t2, suma                        #Verificamos si el string ingresado corresponde al operador '+', si es as� brinque a suma
	
	addi $t2, $0, 45                          #Verificamos si el string ingresado corresponde al operador '-', si es as� brinque a resta
	beq $t1, $t2, resta
	
	addi $t2, $0, 42                          #Verificamos si el string ingresado corresponde al operador '*', si es as� brinque a multiplicacion
	beq $t1, $t2, multiplicacion
	
	addi $t2, $0, 47                          #Verificamos si el string ingresado corresponde al operador '/', si es as� brinque a divisi�n
	beq $t1, $t2, division	
	
	addi $t2, $0, 32                          #Verificamos si el string ingresado corresponde al operador '[space]', si es as� brinque a space
	beq $t1, $t2, space	       
	
	addi $t2, $0, 67                          #Verificamos si el string ingresado corresponde al operador 'C o c', si es as� brinque a Coc
	beq $t1, $t2, Coc
	addi $t2, $0, 99
	beq $t1, $t2, Coc
	jr $ra

raiz:                                             #La funci�n Ra�z recibe un n�mero flotante de la pila, calcula su ra�z, y la imprime
                                                  #De igual forma, almacena el resultado en la pila en 0(sp) 
        l.s $f0, 0($sp)                           #Cargamos en f0 el ultimo numero ingresado por el usuario    
	
	
	addi $t0, $0, 2
	mtc1 $t0, $f1                             #se transfiere un valor de un registro entero a uno flotante
	cvt.s.w $f1, $f1                          #f1=2 Convierte un valor entero en un registro punto flotante en un valor en punto flotante	

	div.s $f2, $f0, $f1                       #$f2=x=N/2 valor inicial de la SEMILLA

	addi $t0, $0, 20                          #numero de iteraciones

iterRaiz:
	div.s $f3, $f0, $f2                        #$f3=N/x
	add.s $f3, $f2, $f3                        #$f3=x+N/x
	div.s $f2, $f3, $f1                        # $f2=(x+N/x)/2

	beq $t0, $0, retRaiz
	addi $t0, $t0, -1
	j iterRaiz

retRaiz:
	                                           #Ojo, no se resta la cantidad de elementos en la pila, dado que el resultado lo guardamos donde estaba el numero a evaluar
	                          
	s.s $f2, 0($sp)                            #Carga en la �ltima posici�n de la pila el resultado de la raiz cuadrada
	
	l.s $f12, 0($sp)                           #Extraemos de la pila el resultado de la multiplicacion y lo guardamos en f12 para imprimirlo
	addi $v0, $0, 2                                     
	syscall	                 
	
	addi $v0,$0, 4
	la $a0, espacio
	syscall
	
	j main  	
		
				
	
suma:                                              #La funci�n suma recibe dos n�meros flotantes de la pila, calcula la suma de estos e imprime el resultado 
                                                   #De igual forma, elimina una posici�n de la pila y almacena el resultado en la pila en 0(sp). 
	l.s $f2, 0($sp)                         
	l.s $f3, 4($sp)
	
	add.s $f4, $f3, $f2                        # Suma   4(sp) - 0(sp)
	addi $sp, $sp, 4                           # Se resta un elemento de la pila
	addi $t9, $t9, -1                          # Elementos en Pila -1 
	s.s $f4, 0($sp)                            # Carga en la �ltima posici�n de la pila el resultado
	
	
	l.s $f12, 0($sp)                           #Extraemos de la pila el resultado de la suma y lo guardamos en f12 para imprimirlo
	addi $v0, $0, 2                                     
	syscall	
	
	
	addi $v0,$0, 4                             #Imprimimos un espacio para que el usuario ingrese un nuevo string en la siguiente l�nea
	la $a0, espacio
	syscall
	
	j main
resta:                                             #La funci�n resta recibe dos n�meros flotantes de la pila, calcula la resta de estos e imprime el resultado 
                                                   #De igual forma, elimina una posici�n de la pila y almacena el resultado en la pila en 0(sp).


	l.s $f2, 0($sp)                            #Carga en $f2 el �ltimo valor de la pila
	l.s $f3, 4($sp)                            #Carga en $f3 el pen�ltimo valor de la pila
	sub.s $f4, $f3, $f2                        # Guarda la resta en f4
	
	
	addi $sp, $sp, 4                           #Se resta un elemento de la pila
	
	addi $t9, $t9, -1                          #Elementos en Pila -1 	
	
	
	s.s $f4, 0($sp)                            #Carga en la �ltima posici�n de la pila el resultado
	
	l.s $f12, 0($sp)                           #Extraemos de la pila el resultado de la resta y lo guardamos en f12 para imprimirlo
	addi $v0, $0, 2                                     
	syscall	
	
	addi $v0,$0, 4                             #Imprimimos un espacio para que el usuario ingrese un nuevo string en la siguiente l�nea
	la $a0, espacio
	syscall
	j main                                     #Volvemos a solicitarle al usuario que ingrese otro string
	
	
space:                                             #La funci�n space extrae el �ltimo valor ingresado o calculado
	                                           #Es decir, imprime el valor que est� en 0(sp)
	                                           
	l.s $f12, 0($sp)                           #Extraemos el ultimo valor de la pila en f12 para imprimirlo
	addi $v0, $0, 2                                     
	syscall			
	

	addi $v0,$0, 4
	la $a0, espacio                            #Imprimimos un espacio para que el usuario ingrese un nuevo string en la siguiente l�nea
	syscall

	j main                                     #Volvemos a solicitarle al usuario que ingrese otro string
	
	
Coc:                                               #La funci�n Coc calcula la cantidad de elementos que se tienen en la pila y regresa sp a la posici�n original
	                                           

	mul $t0, $t9, 4                            #  Se multiplica por 4 la cantidad de elementos en la pila para determinar la cantidad de espacios que se han solicitado en la pila
	add $sp, $sp, $t0                          #  Se restaura la pila
	addi $t9, $0, 0                            #  Vuelve el contador de elementos de la pila a cero
	j main                                     #  Volvemos a solicitarle al usuario que ingrese otro string	



division:                                          #La funci�n division recibe dos n�meros flotantes de la pila, calcula la divisi�n de estos e imprime el resultado 
                                                   #De igual forma, elimina una posici�n de la pila y almacena el resultado en la pila en 0(sp).


	l.s $f2, 0($sp)                            #  Carga en $f2 el �ltimo valor de la pila
	l.s $f3, 4($sp)                            #  Carga en $f3 el pen�ltimo valor de la pila
	div.s $f4, $f3, $f2                        #  Almacena en f4 el resultado de 4(sp)/0(sp)
	addi $sp, $sp, 4                           #  Se resta un elemento de la pila
	addi $t9, $t9, -1                          #  Se le resta al contador de elementos -1	
	
	
	s.s $f4, 0($sp)                            # Carga en la �ltima posici�n de la pila el resultado 
	
	l.s $f12, 0($sp)                           # Extraemos de la pila el resultado de la multiplicacion y lo guardamos en f12 para imprimirlo
	addi $v0, $0, 2                                     
	syscall	
	
	addi $v0,$0, 4                             # Imprimimos un espacio para que el usuario ingrese un nuevo string en la siguiente l�nea
	la $a0, espacio
	syscall
	
	j main
	
	
multiplicacion:                                   # La funci�n multiplicaci�n recibe dos n�meros flotantes de la pila, calcula la multiplicaci�n de estos e imprime el resultado 
                                                  # De igual forma, elimina una posici�n de la pila y almacena el resultado en la pila en 0(sp).


	l.s $f2, 0($sp)                           # Carga en $f2 el �ltimo valor de la pila
	l.s $f3, 4($sp)                           # Carga en $f3 el pen�ltimo valor de la pila
	mul.s $f4, $f3, $f2                       # Almacena en f4 el resultado de 4(sp)*0(sp)
	
	addi $sp, $sp, 4                          # Se resta un elemento de la pila
	addi $t9, $t9, -1                         # Se le resta al contador de elementos -1	 
	
	s.s $f4, 0($sp)                           # Carga en la �ltima posici�n de la pila el resultado
	
	l.s $f12, 0($sp)                          # Extraemos de la pila el resultado de la multiplicacion y lo guardamos en f12 para imprimirlo
	addi $v0, $0, 2                                     
	syscall			
	

	addi $v0,$0, 4
	la $a0,  espacio                          # Imprimimos un espacio para que el usuario ingrese un nuevo string en la siguiente l�nea
	syscall
	j main
	

	
		
finidentificar:	
	jr $ra	                                  # Brincamos al menu para ir a la funci�n 'verificarsihaymenos'
