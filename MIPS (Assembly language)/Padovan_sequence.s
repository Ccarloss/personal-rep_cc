

#-------------------- Universidad de Costa Rica ---------------------------#
#---------------------Escuela Ingeneria El�ctrica --------------------------#
#-------------- Estructuras de Computadoras Digitales I ---------------#

# Autor: Carlos Andr�s Cordero Retana - B92317

# Tarea 01- I Ciclo 2022

# Proposito del codigo: El c�digo calcula la sucesi�n de Padovan para un n�mero n
# ingresado por el usuario. En caso de que se ingrese un n�mero negativo, 
# el c�digo devuelve un mensaje de error.




.data
        #Definimos los string a imprimir
        
	coma: .asciiz ","
	leern: .asciiz "\n Ingrese un numero entero n tal que n>=0 :"
	parente: .asciiz "}"
	sucesionresultado: .asciiz "\n La sucesi�n de Padovan para el n ingresado ser�a: {"
	nLTHAN3: .asciiz "\n P(n)="
	AdressError: .asciiz "Error, el valor de n debe ser mayor o igual a 0\n"
	
	#Definimos el array inicial para n>3
	
	direccionArray: .word 1,1,1    
	
	
.text

main:
	
	addi $v0,$0,4
	la $a0, leern               #imprimir "ingrese el valor de n"
	syscall
	
	
	addi $v0,$0,5               #leer valor de n guardado en $v0               
	syscall                     #codigo 5, para leer enteros
	
	                           
	slt $t5, $v0,$0             #Revisamos que se haya digitado un numero n entero positivo
	bne $t5, $0, error
	
	
	addi $a1, $v0,0             #Guardar el valor de n en $a1
	
	
	la $a2, direccionArray      #Guardar la direccion del array=[1,1,1] en $a2
	
	
	jal sucesion                #llamamos a la funcion sucesion
	
	
	li $v0, 10                  #Finalizamos el programa
        syscall
        
        
        
        
              #Funcion que recibe por $a1 el valor de n y por $a2 la direccion del array= [1,1,1]
 sucesion:
 
 
 	     addi $sp,$sp,-16                #creamos la pila. Ocupamos 16 espacios para almacenar el valor de $a1, $a2 y $ra
 	     
 	     
 	     sw $ra, 12($sp)                 #guardamos la direccion para regresar a main
 	     sw $a1, 8($sp)                  #guardamos el valor de n en la pila
 	     sw $a2, 4($sp)                  #guardamos la direccion del array=[1,1,1] en la pila
 	     
 	                                     #si n<3, brinque a printArraynLT3 donde se imprime p(n)=1
 	     sltiu $t2, $a1, 3     
 	     bne $t2,$0,  printArraynLT3
 	     
 	     
 	     addi $t0, $0, 3                 #puntero Pn=3
 	     addi $t1,$a1,1                  #n + 1
 	     
suceLoop:
	     slt $t2, $t0, $t1               #Pn < n+1  
	     beq $t2, $0, endsuce            #Si no se cumple la condici�n, significa que en la iteracion anterior Pn=n, y as�,
	                                     #el array esta listo para imprimir en endsuce.
	                             
	                             
	     sll $t3, $t0, 2                 #4i
	     addu $t3, $a2, $t3              # A + 4i
	     
	     lw $t4, -8($t3)                # Array [Pn-2]
	     lw $t5, -12($t3)               # Array [Pn-3]
	     
	     addu $t6, $t4, $t5             # Array[ Pn-2 ] + Array [ Pn-3]
	     sw $t6, 0($t3)                 # Array [Pn] = Array[ Pn-2 ] + Array [ Pn-3]
	     
	     addi $t0, $t0,1                #Pn= Pn+1
	     j suceLoop
	     
	     
endsuce:
	    jal printArraynHT3                # Llamamos a la funcion printArray
	    lw $ra, 12($sp)                   # Almacenamos otra vez en ra la direcci�n para regresar al main
	    addi $sp, $sp, 16                 # Devolvemos el puntero de pila a su posicion original
	    jr $ra                            # Regresamos al main
	    
	    
	           #Funcion para imprimir el array, dado que n>3
printArraynHT3:
	
	   add $t7, $0, $0                    #i = 0
	   
	   
	   addi $v0, $0, 4                    #print("La sucesion ser�a={")
           la $a0, sucesionresultado 
           syscall
           
looprint:
           
           addi $t1,$a1,1                     #Guardamos en $t1 el valor de n+1
           slt $t8, $t7, $t1                  #i< n+1
           beq $t8, $0, endprint              # Si no se cumple la condicion, significa que en la interacion anterior i=n.
                                              # As�, dado que el array se reccorri� por completo, brinque a endprint
                                     
           
           sll $t9, $t7, 2                    # 4i
           addu $t9, $t9, $a2                 # A + 4i
           
           addi $v0, $0,1
           lw $a0 , 0($t9)                    #print(A[i])
           syscall
          
          addi $v0, $0, 4                     #print(,)
          la $a0, coma
          syscall
          
          addi $t7, $t7, 1
          j looprint
endprint:
          addi $v0, $0, 4
          la $a0, parente                      #print("}")
          syscall
         
          jr $ra
         
         
                     #Funcion para imprmir 1 cuando n<3
 printArraynLT3:
 
         addi $v0, $0, 4                     #print("P(n)=")
         la $a0, nLTHAN3
         syscall
 
         addi $v0, $0, 1   
	 add $a0, $v0, $0                     #print (1)
         syscall
         
         addi $sp, $sp, 16                   #Devolvemos el puntero de pila a su posicion original
	 jr $ra                              #Regresamos al main
         
         
 
                    # Funcion para el Error
error:
	ori $v0, $0, 4 		   # Imprimir mensaje de error
	la $a0, AdressError
	syscall
	j main      
 
          
          
           
           
           
	   
	   
 	          
	
