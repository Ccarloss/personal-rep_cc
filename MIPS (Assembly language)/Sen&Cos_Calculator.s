



#-------------------- Universidad de Costa Rica ---------------------------#
#---------------------Escuela Ingeneria El�ctrica --------------------------#
#-------------- Estructuras de Computadoras Digitales I ---------------#

# Autor: Carlos Andr�s Cordero Retana - B92317

# Tarea 02- I Ciclo 2022

# Proposito del codigo: El c�digo calcula el seno , coseno del angulo opuesto al cateto menor de un triangulo
#rectangulo. Para ello, se calcula el arcseno de d/c , donde d es el cateto menor y c es la hipotenusa, para la cual
#se emplea la funcion hipo. Asimismo,se emplean otras funciones anidadas, como pot, fact y masomenosseno, que ser�n 
#esenciales.
.data
        
	readA: .asciiz "\nDigite un cateto a: "                                           
	readB: .asciiz "\nDigite otro cateto b: "
	hipoo: .asciiz "\nEl valor de la hipotenusa es: "
	senoo: .asciiz "\nEl valor del seno del angulo opuesto al cateto  m�s peque�o es: "
	cosenoo: .asciiz "\nEl valor del coseno del angulo opuesto al cateto m�s peque�o es: "
	arcsenoo: .asciiz "\nEl �ngulo del tri�ngulo rect�ngulo opuesto al cateto menor (en radianes): "
	espacio: .asciiz "\n"
	
	
.text
main:


                                                        
	addi $v0,$0, 4                                       #leemos el valor de a
	la $a0, readA
	syscall

	li $v0, 6                                       #Lee un float que queda en $f0
	syscall	

	mov.s $f1, $f0                                  #mueve el numero flotante de $f0 a $f1, f1 es el cateto a

		
                                                        #leemos el valor de b
	addi $v0,$0, 4	
	la $a0, readB
	syscall

	addi $v0,$0, 6                                  #Lee un float que queda en $f0
	syscall	

	mov.s $f2, $f0                                  #mueve el numero flotante de $f0 a $f2    
	
	sub.s $f0, $f0, $f0                             #cargamos un cero en $f0	
	sub.s $f3, $f3, $f3                             #cargamos un cero en $f3	
	
	c.eq.s $f1, $f0                                 #Pone cond en 1 si $f1==$f0
	bc1t fin                                        #si cond es 1 salta a la etiqueta

	
	c.eq.s $f2, $f0                                 #Pone cond en 1 si $f1==$f0
	bc1t fin                                        #si cond es 1 salta a la etiqueta
	
	                                                #Guardamos los valores de a y b en la pila para pasarselos a las funciones hipo, seno, coseno y arcoseno
	addi $sp, $sp, -8
	s.s $f1, 4($sp) 
	s.s $f2, 0($sp)
	
	
	jal hipo                                        #Se llama a la funci�n hipo que calcula la hipotenusa
	
	                                                #Se imprime el valor de la hipotenusa, dado que esta funcion se llamara en otras funciones, por ende
	                                                # para que se imprima el resultado una sola vez, se coloca aqui el print
	
	l.s $f12, 0($sp) 

	                                                #Imprimimos "El valor de la hipotenusa es"
	addi $v0,$0, 4
	la $a0, hipoo
	syscall
	                                                #Imprimimos el valor de la hipotenusa
	addi $v0,$0, 2     
	syscall
	                                    
        jal cosenooo                                    #Llamamos a la funci�n cosenooo
	
	jal seno                                        #LLamamos a la funci�n seno    
	
	
	
	jal arcseno                                     #llamamos a la funcion arcoseno
	
	
	addi $v0,$0, 4                                  #Se iimprime el angulo opuesto al cateto menor
	la $a0, arcsenoo
	syscall
	
       
	addi $v0,$0, 2 
	syscall	
	
	
	
	j main                                          #Seguimos solicitando al usuario que ingrese catetos           
        
 cosenooo:                                              #La funci�n coseno recibe como entradas los dos catetos y retorna por f12 el valor del coseno del �ngulo opuesto al cateto menor
                                                        #La idea ser� separar los casos cuando n sea par e impar, para luego restar ambos resultados despu�s de la �ltima iteraci�n
         addi $sp, $sp, -12
  	 sw  $ra, 0($sp)                                #Super importante, guardamos el valor de ra en la pila para poder regresar al main
  	 s.s $f1, 4($sp)                                #De igual forma, guardamos los catetos almacenados en f1 y f2, para luego
  	 s.s $f2, 8($sp)                                # poder utilizarlos en las otras funciones 
  	 
  	 
  	                                                #Llamamos a la funcion arcseno que me calcula el angulo opuesto al cateto menor  
  	 addi $sp, $sp, -8
   	 s.s $f1, 4($sp)                                #Guardamos en la pila las entradas que le entran a la funci�n arcseno
   	 s.s $f2, 0($sp)
   	
   	 jal  arcseno
   	
   	 l.s $f4, 0($sp)                                #La funci�n arcseno me regresa el a;ngulo en la direccion 0(sp), por ende
   	 addi $sp, $sp, 8                               # lo almacenamos en f4, y devuelvo los campos solicitados
  	 
  	 
  	 sub.s $f1, $f1, $f1                            #almacenamos un 0 en f1 para los casos pares 
  	 sub.s $f3, $f3, $f3                            #almacenamos un 0 en f3 para los casos impares
  	 addi $t1, $0, 15                               # numero de iteraciones n 
  	 addi $s0, $0, -1

loopsumacoseno:
 
         beq $t1, $s0, finalcoseno                      #cuando n=15 vaya a finalcoseno
         
                                                                                       
         addi $t2, $0, 1                                #0000 0001
         and $t2, $t1, $t2                              #MASCARA para detectar si n es par o impar 
         
         beq $t2, $0, pars                              # si es par , brinque a par
         
                                                        
         add $t3, $t1, $t1                              # t3= 2n
         
         
         addi $sp, $sp, -8                              #Calculamos el termino x^(2n) al llamar a la funcion pot. Para ello le ingresamos las entradas por la pila
   	 s.s $f4, 4($sp)                                # En f4 est� X
   	 sw $t3, 0($sp)                                 # En t3 est� el exponente
   	 jal pot
   	 l.s $f8, 0($sp)                                # f8= X^2n+1
   	 addi $sp, $sp, 8
   	 
   	                                                #Calculamos el termino (2n)! al llamar a la funci�n fact
   	 add $a0, $t3, $0                               # La funci�n recibe por a0 el valor 2n
   	 jal fact
   	 mtc1 $v0, $f9                                  #La funci�n me devuelve el resultado en v0, raz�n por la cual lo convertimos en un valor en punto flotante
  	 cvt.s.w $f9,$f9
   	 
   	 div.s $f7, $f8, $f9                            # x^2n/2n!
   	 
   	 add.s $f3, $f3, $f7                            # Sumamos cada x^2n/2n! en cada iteraci�n y lo guardamos en el registro f3
   	 
   	 addi $t1, $t1, -1
   	 j loopsumacoseno
   	 
 pars:
                                                      #Realizamos el mismo procedimiento anterior, solo que aqu� se suman los casos pares
                                                      #Calculamos el termino x^(2n) al llamar a la funcion pot, como en el caso anterior
         add $t3, $t1, $t1  #2n
         
         
         addi $sp, $sp, -8
   	 s.s $f4, 4($sp)                              # En f4 est� X
   	 sw $t3, 0($sp)                               # En t3 est� el exponente
   	 jal pot
   	 l.s $f6, 0($sp)                              # f6= X^2n par
   	 addi $sp, $sp, 8
   	 
   	                                              #Calculamos el termino (2n)!
   	 add $a0, $t3, $0  
   	 jal fact
   	 mtc1 $v0, $f10
  	 cvt.s.w $f10,$f10
   	 
   	 div.s $f11, $f6, $f10                        #x^2n/ (2n)!
   	 
   	 add.s $f1, $f1, $f11                         # Guardamos cada suma en $f1
   	 
   	 
   	 addi $t1, $t1, -1
         j loopsumacoseno
      
finalcoseno:
        sub.s $f12, $f1, $f3                          # Restamos los casos pares-impares, y el resultado lo almacenamos en f12
         
   	
	addi $v0,$0, 4     
	la $a0, cosenoo                               #Imprimimos, "el valor del coseno...."
	syscall
	
       
	addi $v0,$0, 2                                #Imprimimos el valor del coseno
	syscall	
	
	lw $ra, 0($sp)                                #agarramos de la pila el valor original de ra para regresar al main
	l.s $f1, 4($sp)                               #de igual forma, le regresamos a f1 y f2 los valores de a y b
	l.s $f2, 8($sp)
	
	addi $sp, $sp, 12
	
	jr $ra                                        #regresamos al main
                  
        
seno:                                                 #La funci�n seno calcula el valor del seno del �ngulo opuesto al cateto menor
                                                      # De hecho funciona exactamente igual que la funcion coseno
         addi $sp, $sp, -12                           #la �nica diferencia es que en t3 en vez de 2n habr� un 2n+1
  	 sw  $ra, 0($sp)                               
  	 s.s $f1, 4($sp) 
  	 s.s $f2, 8($sp)
  	 
  	 
  	                                              #entradas para llamar a la funcion arcseno  
  	 addi $sp, $sp, -8
   	 s.s $f1, 4($sp)
   	 s.s $f2, 0($sp)
   	
   	 jal  arcseno
   	
   	 l.s $f4, 0($sp)                              #guardamos el valor del �ngulo en f4
   	 addi $sp, $sp, 8
  	 
  	 
  	 sub.s $f1, $f1, $f1                          #almacenamos un 0 en f1 para los casos pares
  	 sub.s $f3, $f3, $f3                          #almacenamos un 0 en f3 para los casos impares
  	 addi $t1, $0, 15                             # numero de iteraciones n
  	 addi $s0, $0, -1

loopsumaseno:
 
         beq $t1, $s0, finalseno  
         
                                                      #MASCARA para detectar si n es par o impar
                                         
         addi $t2, $0, 1                              #0000 0001
         
         and $t2, $t1, $t2
         
         beq $t2, $0, par                             #si n es par, brinque a los casos pares
         
                                                      #Calculamos el termino x^(2n+1) al llamar a la funcion pot
         add $t3, $t1, $t1                            #2n
         addi $t3, $t3, 1                             #2n+1
         
         addi $sp, $sp, -8
   	 s.s $f4, 4($sp)                              # En f4 est� X
   	 sw $t3, 0($sp)                               # En t3 est� el exponente
   	 jal pot
   	 l.s $f8, 0($sp)                              # f8= X^2n+1
   	 addi $sp, $sp, 8
   	 
   	                                              #Calculamos el termino (2n+1)!
   	 add $a0, $t3, $0                             # a0= 2n+1
   	 jal fact
   	 mtc1 $v0, $f9
  	 cvt.s.w $f9,$f9                              # f9 = (2n+1)!
   	 
   	 div.s $f7, $f8, $f9                          # f7= X^2n+1 / (2n+1)!
   	 
   	 add.s $f3, $f3, $f7                          #sumamos todas las iteraciones impares
   	 addi $t1, $t1, -1
   	 j loopsumaseno
   	 
 par:
 
                                                      #Calculamos el termino x^(2n+1) al llamar a la funcion pot
         add $t3, $t1, $t1                            #2n
         addi $t3, $t3, 1                             #2n+1
         
         addi $sp, $sp, -8
   	 s.s $f4, 4($sp)                              # En f4 est� X
   	 sw $t3, 0($sp)                               # En t3 est� el exponente
   	 jal pot
   	 l.s $f6, 0($sp)                              # f6= X^2n+1 par
   	 addi $sp, $sp, 8
   	 
   	                                              #Calculamos el termino (2n+1)!
   	 add $a0, $t3, $0                             # a0= 2n+1
   	 jal fact
   	 mtc1 $v0, $f10
  	 cvt.s.w $f10,$f10
   	 
   	 div.s $f11, $f6, $f10                        #f11= x^2n+1/ (2n+1)!
   	 add.s $f1, $f1, $f11                         # en f1 guardamos todas las iteraciones pares
   	 
   	 
   	 addi $t1, $t1, -1
         j loopsumaseno
      
finalseno:                                            #guaradamos en f12 la resta pares-impares
        sub.s $f12, $f1, $f3
      
	addi $v0,$0, 4                                #Imprimimos "el valor del seno..."
	la $a0, senoo
	syscall
	
       
	li $v0, 2                                     #imprimimos el valor del seno obtenido
	syscall	
	
	lw $ra, 0($sp)                               #devolvemos a los registros ra, f1 y f2 sus valores iniciales
	l.s $f1, 4($sp)
	l.s $f2, 8($sp)
	
	addi $sp, $sp, 12                     
	
	jr $ra                                       #brincamos a main
           
               
arcseno:                                             #La funci�n arcseno calcula el �ngulo opuesto al cateto menor
                                                     #Para ello necesitaremos llamar a tres funciones:
                                                     # mayormenorseno : me calcula el valor de entrada X= d/c donde d es el cateto menor y c la hipotenusa
  	 addi $sp, $sp, -12                          #factorial: me calcula el factorial de un n�mero 
  	 sw $ra, 0($sp)                              # pot: me calcula la potencia de n�mero
  	 s.s $f1, 4($sp)                             #Para ello , guardamos en la pila los valores de ra , a y b.
  	 s.s $f2, 8($sp)
  	 
  	 
  	                                             #eGuardamos en la pila la entradas que requiere la funcion mayoromenorseno   
  	 addi $sp, $sp, -8                           # la cual me retorna en f4 el valor de x= d/c
   	 s.s $f1, 4($sp)
   	 s.s $f2, 0($sp)
   	
   	 jal  mayormenorseno
   	
   	 l.s $f4, 0($sp)   
   	 addi $sp, $sp, 8                           #En f4 ya tengo la entarda para calcular arcseno
  	 
  	 
  	 sub.s $f1, $f1, $f1                        # forzamos un 0 en f1
  	 addi $t1, $0, 17                           #numero de iteraciones n
  	 addi $s0, $0, -1                           #valor final
  	 
 loopsuma:
 
 	 beq $t1, $s0, finarcseno                   #cuando n sea menor 0, brinque a finarcseno. La idea de este loop es calcular cada t�rmino para montar la serie propuesta 
 	                                            #Pero, como se ver�, para calcular algunos t�rminos ser� necesario llamar a la funci�n fact o pot.
 	 add $a0,$t1,$0
         jal fact                                   #factorial de n, se devuelve en v0
         mul $t2,$v0,$v0                            #Se guarda (n!)^2 en t2
         mtc1 $t2,$f0
         cvt.s.w $f0,$f0                            #f0=(n!)^2 
         
       	
	add $t3, $t1, $t1                           #t3=2n
  	add $a0,$t3,$0                              #a0=t3=2n
  	jal fact                                    #factorial de 2n, se devuelve en v0
  	add $t4,$v0,$0
  	mtc1 $t4,$f6
  	cvt.s.w $f6,$f6                             #f6=(2n)! 
	 
   	addi $t3, $t3, 1                            #2n+1
   	mtc1 $t3,$f7
  	cvt.s.w $f7,$f7
   	
   	mul.s $f9, $f7, $f0                        # (2n+1) * n^2 
   	div.s $f9, $f6, $f9                        # 2n!/ (2n+1) * n^2
   	
   	
   	                                           #Entradas para pot X^2n+1
   	addi $sp, $sp, -8
   	s.s $f4, 4($sp)
   	sw $t3, 0($sp)
   	
   	jal pot
   	
   	l.s $f8, 0($sp)                           # f8= X^2n+1
   	addi $sp, $sp, 8
   	
   	
   	mul.s $f9, $f9, $f8                       # 2n! * X^2n+1/ (2n+1) * n!^2
   	
   	
   	                                          
   	addi $t7, $0, 4
   	mtc1 $t7,$f14                             #f14=4
  	cvt.s.w $f14,$f14                         
   	
   	addi $sp, $sp, -8                         #Entradas para pot 4^n
   	s.s $f14, 4($sp)
   	sw $t1, 0($sp)
   	
   	jal pot
   	
   	l.s $f15, 0($sp)                          # f15= 4^n
   	addi $sp, $sp, 8
   	
	addi $t9, $0, 1
   	mtc1 $t9,$f16
  	cvt.s.w $f16,$f16                         #f16=1
	
	div.s $f17, $f16 , $f15                   #f7=  1/ 4^n
	mul.s $f19, $f9, $f17                     #f19= 2n! * X^2n+1/ (2n+1) * n!^2* 4^n
	
   	add.s $f1, $f1, $f19                      # Almacenamos en f1 la suma total de cada 2n! * X^2n+1/ (2n+1) * n!^2* 4^n desde n=0 hasta n=17
 	 
 	addi $t1, $t1, -1                         #n=n-1
 	 
 	 j loopsuma
 	 
 finarcseno:                         
        s.s   $f1, 12($sp)                        #Regresamos el reusltado final en la direcci�n 0(sp)
        sub.s $f5, $f5, $f5
   	add.s $f12, $f1, $f5                      #asimismo, lo almacenamos en f12 para imprimirlo en el main
	
	lw $ra, 0($sp)                            #Importante, guardamos en ra la direcci�n de retorno al main
	l.s $f1, 4($sp)
	l.s $f2, 8($sp)
	
	addi $sp, $sp, 12
	
	jr $ra


pot:                                  #La funcion pot recibe un valor flotante (que es la base) almacenado en la pila en 24(sp)
                                      #Asimismo, recibe el exponente en un valor entero almacenado en 20($sp)
	 addi $sp, $sp, -20           #Retorna un valor flotante en la direcci�n 20(sp)
  	 sw   $t0, 0($sp)             #Guardamos en la pila los diferentes registros de inter�s
  	 sw   $t3, 4($sp) 
  	 s.s $f0, 8($sp)
  	 s.s $f4, 12($sp)
  	 s.s $f12, 16($sp)
  	 
  	 
  	 lw $t3, 20($sp)              #Cargamos las entradas en t3 y en f4
  	 l.s $f4, 24($sp)
  	 
  	 
  	 addi $t0, $0, 1             
  	 
  	 mtc1 $t0, $f0 
	 cvt.s.w $f0, $f0             #f0=1, para realizar la primera iteraci�n
	 
loopot:

	 beq $t3, $0, finpot          #Cuando el exponente sea 0, vaya a finpot
	
	 mul.s $f0, $f0, $f4          #Multiplicamos X*X la cantidad t3(exponente) de veces
	
		
	 addi $t3, $t3, -1            #vamos restando el exponente
	
	j loopot
	
finpot: 
	
	s.s $f0, 20($sp)              #Guardamos en la pila el resultado de la funci�n en la direcci�n 20(sp)
	 
	lw $t0, 0($sp)
	lw $t3, 4($sp)
	l.s $f0, 8($sp)
	l.s $f4, 12($sp)
	l.s $f12, 16($sp)
	
	addi $sp, $sp, 20
	
	jr $ra
  	 

  		 
 fact:                              #La funci�n fact recibe la entrada por a0 y retorna por v0 el factorial de a0
       
	addi $sp, $sp, -8           # solicitamos 2 espacios en la pila
	
	sw $ra, 4($sp)              # guardamos la direccion de retorno

	sw $a0, 0($sp)              # guardarmos n

	slti $t0,$a0,1              #  n < 1
	
	beq $t0,$zero,L1            # if n >= 1, vaya a L1

	addi $v0,$zero,1            # return 1
	
	addi $sp,$sp,8              # pop 2 items off stack
	
	jr $ra                      # return to caller

	L1:

	addi $a0,$a0, -1            # n >= 1: argument gets (n � 1)

	jal fact                    # call fact with (n �1)
	
	lw $a0, 0($sp)              # return from jal: restore argument n
	
	lw $ra, 4($sp)              # restore the return address
	
	addi $sp, $sp, 8            # adjust stack pointer to pop 2 items
	
	mul $v0,$a0,$v0             # return n * fact (n � 1)
	
	jr $ra
  		 	 	 
  		 	 	  		 	 	 
		 	 	  		 	 	  		 	 	  		 	  		 	 	  		 	 	  		 	 	  		 	 	  		 	 	  		 	 	 
mayormenorseno:                        #Esta funcion calcula el cociente d/c donde d es el cateto menor ingresado

                                       # Guardamos los inputs a, b, c y el registro f0 que tambi�n lo utilizaremos
        addi $sp,$sp, -12
	sw $ra, 0($sp)                 #  VALOR de ra
	s.s $f1, 4($sp)                # VALOR de a
	s.s $f2, 8($sp)                #  VALOR de b
	
	l.s $f1, 16($sp)               #almaceno en $f1 el valor de a
	l.s $f2, 12($sp)               #almaceno  en $f2 el valor de b
	
	
	addi $sp,$sp, -8               #Llamamos a la funci�n hipo que me calcula el valor de la hipotenusa
	s.s $f1, 4($sp)                # VALOR de a
	s.s $f2, 0($sp)                #  VALOR de b
	jal hipo 
	l.s $f3, 0($sp)                #almacenamos en f3 el valor de c
   	addi $sp, $sp, 8         
	
                                       # Si a< b             
	
	c.lt.s $f1, $f2                #Pone cond en 1 si a<b = $f1< $f2
	bc1t acatetomenor              #si cond es 1 salta a la etiqueta	
	
	div.s $f4, $f2, $f3            # x = b/c
	
	s.s $f4, 12($sp)               #Guardamos la salida en la posicion 12, esto, dado que se guardara en f12 el valor que haya en 0(sp)
	
	j finseno
	
	
acatetomenor:	
        div.s $f4, $f1, $f3            # x= a/c
	
	s.s $f4, 12($sp)               #Guardamos la salida en la posicion 12, esto, dado que se guardara en f12 el valor que haya en 0(sp)
	
	j finseno
	
    	
finseno:
        lw $ra, 0($sp)                 #Devolvemos a ra su valor original cuando se llam� la funci�n
	l.s $f1, 4($sp)                #De igual forma, devolvemos a los registros f1 y f2 los valores de a y b
	l.s $f2, 8($sp) 
	addi $sp,$sp, 12   
	
	jr $ra


       	
hipo:                                  #La funci�n hipo toma los catetos a y b ingresados, y calcula la ra�z del cuadrado de la suma de estos
                                       #Es decir, la hipotenusa jaj
        addi $sp,$sp, -16              #Creamos espacio para trabajar en la pila
	s.s $f0, 12($sp)               #posicion 16 
	s.s $f1, 8($sp)                #posicion 20
	s.s $f2, 4($sp)                #posicion 24
	s.s $f3, 0($sp)                #posicion 28


	l.s $f1, 20($sp)               #Cargamos en f1 el valor a
	l.s $f2, 16($sp)               #Cargamos en f2 el valor de b
	
	
	mul.s $f1, $f1, $f1            # a^2
	mul.s $f2, $f2, $f2            # b^2
	
	add.s $f0, $f1 , $f2           #a^2 + b^2
	
	
	addi $t0, $0, 2
	mtc1 $t0, $f1                  #se transfiere un valor de un registro entero a uno flotante
	cvt.s.w $f1, $f1               #f1=2 Convierte un valor entero en un registro punto flotante en un valor en punto flotante	

	div.s $f2, $f0, $f1            #$f2=x=N/2 valor inicial de la SEMILLA

	addi $t0, $0, 20               #numero de iteraciones

iterRaiz:
	div.s $f3, $f0, $f2            #$f3=N/x
	add.s $f3, $f2, $f3            #$f3=x+N/x
	div.s $f2, $f3, $f1            # $f2=(x+N/x)/2

	beq $t0, $0, retRaiz
	addi $t0, $t0, -1
	j iterRaiz

retRaiz:
	s.s $f2, 16($sp)               #Regresamos el valor de la hipotenusa por la pila, en la direcci�n 16(sp)

	l.s $f3, 0($sp)                #Devolvemos a los a los registros empleados sus valores originales
	l.s $f2, 4($sp)
	l.s $f1, 8($sp)
	l.s $f0, 12($sp) 
	addi $sp,$sp, 16
	
	jr $ra	                       
	
	

fin:	
	
	addi $v0,$0, 10                     #Finalizamos el programa en caso de que a y b sean 0
        syscall
