module DUT
(
    input TARJETA_RECIBIDA,TIPO_TRANS,DIGITO_STB,MONTO_STB, Clk, reset,
    input [3:0] DIGITO,
    input [15:0] PIN,
    input [31:0] MONTO,
    input [63:0] BALANCE_INICIAL,
    output BALANCE_ACTUALIZADO,ENTREGAR_DINERO,PIN_INCORRECTO,ADVERTENCIA,BLOQUEO,FONDOS_INSUFICIENTES
);


reg [3:0] estado_pres;
reg [2:0] INTENTOS;
reg [63:0] BALANCE;

reg [3:0] prox_estado;
reg [2:0] nxt_INTENTOS;
reg [63:0] nxt_BALANCE;

reg [3:0] DIGITOS [4];  // Almacena los 4 digitos de 4 bits cada uno
reg [3:0] nxt_DIGITOS [4];

// Asignacion de estados

parameter a = 4'b0000;
parameter b = 4'b0001;
parameter c = 4'b0010;
parameter d = 4'b0011;
parameter e = 4'b0100;
parameter f = 4'b0101;
parameter g = 4'b0110;
parameter h = 4'b0111;
parameter i = 4'b1000;
parameter j = 4'b1001;
parameter k = 4'b1010;


// Memoria de proximo estado

always@ (posedge Clk) 
begin
    if (!reset) begin
        estado_pres <= a;
        INTENTOS <= 3'b001;
        BALANCE <= 64'd0;
        DIGITOS[0] <= 4'd0;
        DIGITOS[1] <= 4'd0;
        DIGITOS[2] <= 4'd0;
        DIGITOS[3] <= 4'd0;
    end else begin 
        estado_pres <= prox_estado;
        INTENTOS <= nxt_INTENTOS;
        BALANCE <= nxt_BALANCE;
        DIGITOS[0] <= nxt_DIGITOS[0];
        DIGITOS[1] <= nxt_DIGITOS[1];
        DIGITOS[2] <= nxt_DIGITOS[2];
        DIGITOS[3] <= nxt_DIGITOS[3];
    end
end


// Logica de calculo de proximo estado

always@(*)
begin

  prox_estado = estado_pres;       //Para garantizar el comportamiento del FF se necesita
  nxt_INTENTOS = INTENTOS;         //que por defecto el prox_estado y nxt_count0 sostenga el valor del estado anterior.
  nxt_BALANCE = BALANCE;
  nxt_DIGITOS[0] = DIGITOS[0];
  nxt_DIGITOS[1] = DIGITOS[1];
  nxt_DIGITOS[2] = DIGITOS[2];
  nxt_DIGITOS[3] = DIGITOS[3];

    case (estado_pres)
        a:  if(TARJETA_RECIBIDA) begin     // Estado inicial: esperando ingreso de tarjeta
            prox_estado = b;
            nxt_BALANCE = BALANCE_INICIAL;
        end
        else prox_estado = a;
                                          
        b:  begin                        // Esperamos el ingreso del primer digito del PIN
            if(DIGITO_STB) begin
                nxt_DIGITOS[3] <= DIGITO;
                prox_estado = c;
            end else prox_estado = b;
            end 
            
        c:  begin  
            if(DIGITO_STB) begin
                nxt_DIGITOS[2] <= DIGITO;   // Esperamos el ingreso del segundo digito del PIN
                prox_estado = d;
            end else prox_estado = c;                    
            end

        d:  begin 
            if(DIGITO_STB) begin
                nxt_DIGITOS[1] <= DIGITO;  // Esperamos el ingreso del tercer digito del PIN
                prox_estado = e;
            end else prox_estado = d;
            end
    
        e: begin                           
            if(DIGITO_STB) begin
                nxt_DIGITOS[0] <= DIGITO;      // Esperamos el ingreso del cuarto digito del PIN
                prox_estado = k;
            end else prox_estado = e;
            end
        
        k: begin
            if (PIN == {DIGITOS[3],DIGITOS[2],DIGITOS[1],DIGITOS[0]})begin   // Evaluamos si el PIN ingresado es correcto
                    if (TIPO_TRANS) prox_estado = g;
                    else prox_estado = h;

            end else begin
                        case (INTENTOS)
                            3'b001 : begin 
                                prox_estado = i;
                                nxt_INTENTOS = INTENTOS + 1;
                            end
 
                            3'b010 : begin 
                                prox_estado = j;
                                nxt_INTENTOS = INTENTOS + 1;
                            end
                           
                            3'b011 : prox_estado = f;       
                        endcase
                end
        end

        f: if (!reset) prox_estado = a;      // Estado de bloqueo (si el usuario ingreso tres veces el PIN de forma erronea)
        else prox_estado = f;
        
        g:  begin                            // Estado en caso de que el usuario desee hacer un deposito
            if(MONTO_STB) begin
                nxt_BALANCE = BALANCE + MONTO;
                prox_estado = a;
            end  else prox_estado = g;                         
            end
        
        h:  begin 
            if(MONTO_STB) begin                       // Estado en caso de que el usuario desee hacer un retiro
                if(MONTO < BALANCE_INICIAL) begin      // Se verifica que el monto ingresado sea menor que el saldo disponible
                    nxt_BALANCE = BALANCE - MONTO;
                    prox_estado = a;
                end else prox_estado = h;
                
            end  else prox_estado = h;                                       

            end
        i: prox_estado = b;                    // Estado de PIN INCORRECTO (si el usuario se equivoco 1 vez)

        j: prox_estado = b;                    // Estado de ADVERTENCIA (si el usuario se equivoco 2 veces)
  
        default: prox_estado = a;            // Si la maquina cae en un estado indeterminado, empiece de nuevo

    endcase
end


//Logica de calculo de salidas 

assign BALANCE_ACTUALIZADO = ((estado_pres == g & MONTO_STB)|(estado_pres == h & MONTO_STB & (MONTO < BALANCE_INICIAL)));
assign ENTREGAR_DINERO = ((estado_pres == h) & MONTO_STB & (MONTO < BALANCE_INICIAL));
assign PIN_INCORRECTO = (estado_pres == i);
assign ADVERTENCIA = (estado_pres == j);
assign BLOQUEO = (estado_pres == f);
assign FONDOS_INSUFICIENTES = ((estado_pres == h) & MONTO_STB & (MONTO>BALANCE_INICIAL));
    
endmodule
