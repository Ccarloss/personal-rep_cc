module tester ( TARJETA_RECIBIDA,TIPO_TRANS,DIGITO_STB,MONTO_STB, Clk, reset,
    DIGITO,
    PIN,
    MONTO,
    BALANCE_INICIAL,
    BALANCE_ACTUALIZADO,ENTREGAR_DINERO,PIN_INCORRECTO,ADVERTENCIA,BLOQUEO,FONDOS_INSUFICIENTES
);


// 1) Definicion de variables
output reg TARJETA_RECIBIDA,TIPO_TRANS,DIGITO_STB,MONTO_STB, Clk, reset;

output reg [3:0]DIGITO;
output reg [15:0]PIN;
output reg [31:0]MONTO;
output reg [63:0]BALANCE_INICIAL;

input BALANCE_ACTUALIZADO,ENTREGAR_DINERO,PIN_INCORRECTO,ADVERTENCIA,BLOQUEO,FONDOS_INSUFICIENTES;

reg [119:0] testvectors [40:0];
integer puntero;


initial 
    begin
    
// 2) Llenamos la tabla de vectores

//({TARJETA_RECIBIDA, TIPO_TRANS, DIGITO_STB, MONTO_STB}, DIGITO, PIN, MONTO, BALANCE_INICIAL)

// Prueba 1:  funcionamiento normal + deposito

// a --> b --> c --> d --> e --> k --> g --> a

testvectors[0] = {4'b1000,4'd0,4'd2,4'd3,4'd1,4'd7,32'd0,64'd100000};  // a--> b
testvectors[1] = {4'b1010,4'd2,4'd2,4'd3,4'd1,4'd7,32'd0,64'd100000};  // b --> c
testvectors[2] = {4'b1010,4'd3,4'd2,4'd3,4'd1,4'd7,32'd0,64'd100000};  // c --> d
testvectors[3] = {4'b1010,4'd1,4'd2,4'd3,4'd1,4'd7,32'd0,64'd100000};  // d --> e
testvectors[4] = {4'b1010,4'd7,4'd2,4'd3,4'd1,4'd7,32'd0,64'd100000};  // e --> k
testvectors[5] = {4'b1100,4'd0,4'd2,4'd3,4'd1,4'd7,32'd12000,64'd100000}; // k --> g
testvectors[6] = {4'b1001,4'd0,4'd2,4'd3,4'd1,4'd7,32'd12000,64'd100000}; // g --> a

// Prueba 2:  funcionamiento normal + PIN_INCORRECTO + ADVERTENCIA + BLOQUEO

//// a --> b --> c --> d --> e --> i --> b --> c --> d --> e --> j--> b --> c --> d --> e --> f 

testvectors[7] =  {4'b1000,4'd0,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // a--> b
testvectors[8] =  {4'b1010,4'd5,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // b --> c
testvectors[9] =  {4'b1010,4'd5,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // c --> d
testvectors[10] = {4'b1010,4'd6,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // d --> e
testvectors[11] = {4'b1010,4'd0,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // e --> k
testvectors[12] = {4'b1000,4'd0,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // k --> i
testvectors[13] = {4'b1000,4'd0,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // i --> b
testvectors[14] = {4'b1010,4'd2,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // b --> c
testvectors[15] = {4'b1010,4'd5,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // c --> d
testvectors[16] = {4'b1010,4'd2,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // d --> e
testvectors[17] = {4'b1010,4'd1,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // e --> k
testvectors[18] = {4'b1000,4'd1,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // k --> j
testvectors[19] = {4'b1000,4'd0,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // j --> b
testvectors[20] = {4'b1010,4'd5,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // b --> c
testvectors[21] = {4'b1010,4'd4,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // c --> d
testvectors[22] = {4'b1010,4'd3,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // d --> e
testvectors[23] = {4'b1010,4'd2,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // e --> k
testvectors[24] = {4'b1000,4'd2,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // k --> f
testvectors[25] = {4'b0000,4'd5,4'd5,4'd5,4'd6,4'd5,32'd0,64'd300000};  // f --> a (se aplica en dos flancos decrecientes veces por culpa del reset)
//
//
// Prueba 3: funcionamiento normal + retiro + FONDOS_INSUFICIENTES + ENTREGAR_DINERO

// a --> b --> c --> d --> e --> k --> h --> h --> a --> a

testvectors[26] = {4'b1000,4'd0,4'd2,4'd7,4'd3,4'd7,32'd0,64'd20000};     // a--> b
testvectors[27] = {4'b1010,4'd2,4'd2,4'd7,4'd3,4'd7,32'd0,64'd20000};     // b --> c
testvectors[28] = {4'b1010,4'd7,4'd2,4'd7,4'd3,4'd7,32'd0,64'd20000};     // c --> d
testvectors[29] = {4'b1010,4'd3,4'd2,4'd7,4'd3,4'd7,32'd0,64'd20000};     // d --> e
testvectors[30] = {4'b1010,4'd7,4'd2,4'd7,4'd3,4'd7,32'd0,64'd20000};     // e --> k
testvectors[31] = {4'b1000,4'd7,4'd2,4'd7,4'd3,4'd7,32'd0,64'd20000};     // k --> h
testvectors[32] = {4'b1001,4'd7,4'd2,4'd7,4'd3,4'd7,32'd24000,64'd20000}; // h --> h
testvectors[33] = {4'b1001,4'd0,4'd2,4'd7,4'd3,4'd7,32'd5000,64'd20000};  // h --> a
testvectors[34] = {4'b0000,4'd0,4'd0,4'd0,4'd0,4'd0,32'd0,64'd000000};    // a--> a  


// 3) Power up reset

    reset = 0; #3;

// Para no arrancar en X hasta el primer flanco decreciente
    TARJETA_RECIBIDA = 0; TIPO_TRANS=0; DIGITO_STB = 0; MONTO_STB = 0;
    DIGITO = 4'd0;
    PIN = 16'd0;
    MONTO = 32'd0;
    BALANCE_INICIAL = 64'd0;

#4; reset = 1;

// 4) Inicializamos el puntero
    puntero = 0;


end

// 5) Ponemos a correr el reloj
always
    begin
        Clk = 0; #5; Clk = 1; #5;
    end


// 6) Memoria de entradas  (Inyectamos un nuevo vector de entrada en cada flanco decreciente)

always @(negedge Clk) 
    begin
        $display ("Entradas-----> = TARJETA_RECIBIDA = %b , TIPO_TRANS = %b ,DIGITO_STB = %b, MONTO_STB = %b\n",TARJETA_RECIBIDA,TIPO_TRANS,DIGITO_STB,MONTO_STB );
        {TARJETA_RECIBIDA, TIPO_TRANS, DIGITO_STB, MONTO_STB, DIGITO, PIN, MONTO, BALANCE_INICIAL} = testvectors[puntero];
        #10;
        MONTO_STB = 0;
        DIGITO_STB = 0;
        if(puntero == 25) begin
            reset = 0; #6; reset = 1;
        end

    end


// 7) Alistamos el nuevo vector durante el flanco positivo para que este listo en el flanco negativo
// Ademas, vemos cual es el valor de la salida obtenido.

always @(posedge Clk)
begin
  if (reset)
    begin
        //debug
        $display ("Salidas-----> BALANCE_ACTUALIZADO = %b ,ENTREGAR_DINERO = %b ,PIN_INCORRECTO = %b ,ADVERTENCIA = %b ,BLOQUEO,FONDOS_INSUFICIENTES = %b\n", BALANCE_ACTUALIZADO,ENTREGAR_DINERO,PIN_INCORRECTO,ADVERTENCIA,BLOQUEO,FONDOS_INSUFICIENTES);
        puntero = puntero + 1;
        if (puntero == 35) begin
            #4;
            $finish;
        end
    end
end

    
endmodule
