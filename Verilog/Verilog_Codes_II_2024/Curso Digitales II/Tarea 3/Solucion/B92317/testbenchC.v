// Universidad de Costa Rica
// Descripción Conductual de un controlador de cajero automatico
// Carlos Andres Cordero Retana B92317
// Fecha: 06/10/2024


`timescale 1ns/1ns
`include "tester.v"
`include "DUT.v"
                              

module testbench_t1;

// Definimos todas las variables como wires para conectar el testbench con el DUT
  wire TARJETA_RECIBIDA,TIPO_TRANS,DIGITO_STB,MONTO_STB, Clk, reset;
  wire [3:0] DIGITO;
  wire [15:0] PIN;
  wire [31:0] MONTO;
  wire [63:0] BALANCE_INICIAL;
  wire BALANCE_ACTUALIZADO,ENTREGAR_DINERO,PIN_INCORRECTO,ADVERTENCIA,BLOQUEO,FONDOS_INSUFICIENTES;


  initial begin
    $dumpfile("OndasConductual.vcd");               // Archivo para simular en gtkwave
    $dumpvars(-1,UUT);                   // Almacene todas las variables del modulo actual y primitivas que cambian
  end

// Conectamos el DUT con el tester  (Se agrega la clave correcta 2317 para hacer overwrite)
 DUT UUT (
    .TARJETA_RECIBIDA(TARJETA_RECIBIDA),.TIPO_TRANS(TIPO_TRANS), .DIGITO_STB(DIGITO_STB), .MONTO_STB(MONTO_STB), .Clk(Clk), .reset(reset),
    .DIGITO(DIGITO),
    .PIN(PIN),
    .MONTO(MONTO),
    .BALANCE_INICIAL(BALANCE_INICIAL),
    .BALANCE_ACTUALIZADO(BALANCE_ACTUALIZADO), .ENTREGAR_DINERO(ENTREGAR_DINERO), .PIN_INCORRECTO(PIN_INCORRECTO), .ADVERTENCIA(ADVERTENCIA), .BLOQUEO(BLOQUEO), .FONDOS_INSUFICIENTES(FONDOS_INSUFICIENTES)
  );

 tester probador (
    .TARJETA_RECIBIDA(TARJETA_RECIBIDA),.TIPO_TRANS(TIPO_TRANS), .DIGITO_STB(DIGITO_STB), .MONTO_STB(MONTO_STB), .Clk(Clk), .reset(reset),
    .DIGITO(DIGITO),
    .PIN(PIN),
    .MONTO(MONTO),
    .BALANCE_INICIAL(BALANCE_INICIAL),
    .BALANCE_ACTUALIZADO(BALANCE_ACTUALIZADO), .ENTREGAR_DINERO(ENTREGAR_DINERO), .PIN_INCORRECTO(PIN_INCORRECTO), .ADVERTENCIA(ADVERTENCIA), .BLOQUEO(BLOQUEO), .FONDOS_INSUFICIENTES(FONDOS_INSUFICIENTES)
  );

endmodule
