module receptor_I2C(
 
   SDA_OUT, SDA_OE, SCL, RD_DATAr,
   
   CLK, RESET, SDA_INr, WR_DATAr
   );

  //Entradas
  input CLK, RESET, SDA_OE, SDA_OUT, SCL;
  input [15:0] RD_DATAr;

  //Salidas
  output reg SDA_INr;
  output reg [15:0] WR_DATAr;
  

  // states
  localparam START    = 5'b00001;
  localparam ADDRESS  = 5'b00010;
  localparam AWRITE    = 5'b00100;
  localparam AREAD     = 5'b01000;
  localparam STOP     = 5'b10000;

  //Variables internas
  reg [4:0] state, nxt_state;
  reg [3:0] count_bits, nxt_count_bits;
  reg [3:0] count_trama, nxt_count_trama;
  reg [6:0] address_leida, nxt_address_leida;
  reg RNW_leido, nxt_RNW_leido;

  reg [15:0] nxt_WR_DATAr;

  wire negedge_SDA_OUT;
  wire posedge_SDA_OUT;
  reg SDA_OUT_anterior, nxt_SDA_INr, dato_leido;

  // Para identificar mas adelante las condiciones de Start y Stop
  assign negedge_SDA_OUT = (SDA_OUT_anterior && !SDA_OUT);
  assign posedge_SDA_OUT = (!SDA_OUT_anterior && SDA_OUT);

  //Fip flops (mismos 3 grupos del generador)
  always @(posedge CLK) begin
    if (RESET) begin
      state       <= START;
      SDA_OUT_anterior <= 1;
    end else begin
      state        <= nxt_state;
      SDA_OUT_anterior <= SDA_OUT;
    end
  end


  always @(posedge SCL) begin
    if (RESET) begin
      count_bits  <= 0;
      count_trama  <= 0;
      address_leida <= 0;
      RNW_leido <= 0;

    end else begin
      count_bits   <= nxt_count_bits;
      count_trama   <= nxt_count_trama;
      WR_DATAr  <= nxt_WR_DATAr;
      address_leida <= nxt_address_leida;
      RNW_leido <= nxt_RNW_leido;
    end
  end
// Este grupo de FFs garantizan que se respeten los tiempos de set up
  always @(negedge SCL) begin
    if (RESET) begin
      SDA_INr     <= 0;
    end else begin
      SDA_INr      <= nxt_SDA_INr;
    end
  end

  //Logica combinacional

  always @(*) begin
    nxt_state = state;
    nxt_count_bits = count_bits;
    nxt_count_trama = count_trama;
    nxt_SDA_INr = SDA_INr;
    nxt_RNW_leido = RNW_leido;
    nxt_address_leida = address_leida;
    nxt_WR_DATAr = WR_DATAr;

    case (state)
	    START:    // EStado pre transaccion
        begin
          WR_DATAr <= 16'd0;
		      nxt_count_bits = 0;
          nxt_count_trama = 0;
          SDA_INr = 1;
          address_leida = 0;
          RNW_leido = 0;
          if (SCL & negedge_SDA_OUT) nxt_state = ADDRESS;
	      end

      ADDRESS:    // En este estado se recibe la direccion del generador y se responde con un ACK si obtuvo la trama completa
        begin
          nxt_count_trama = 0;
          nxt_count_bits = count_bits + 1;
          if (count_bits < 8) 
            begin
              nxt_SDA_INr = 1;
              if (count_bits == 7) nxt_RNW_leido = SDA_OUT;
              else nxt_address_leida[6-count_bits] =  SDA_OUT;
            end
          else 
            begin
              nxt_SDA_INr = 0; 
              nxt_count_trama = count_trama + 1;
              nxt_count_bits = 0;
              if (RNW_leido) 
                begin
                  nxt_state = AREAD;
                end
              else nxt_state = AWRITE;
            end                  
        end

	    AWRITE:         //  Se reciben dos transacciones  de 8 bits para escribir. Se responde con 2 acks si las transacciones fueron correctas
        begin
            nxt_count_bits = count_bits + 1;
            if(count_bits < 8)
              begin
                nxt_SDA_INr = 1;
                if (count_trama == 1) nxt_WR_DATAr [ 15 - count_bits] = SDA_OUT;
                else nxt_WR_DATAr [ 7 - count_bits] = SDA_OUT;
              end
            else
              begin
                nxt_SDA_INr = 0;
                nxt_count_trama = count_trama + 1;
                nxt_count_bits = 0;
                if (count_trama == 2) nxt_state = STOP;
              end
        end

	    AREAD:        //  Se mandan dos transacciones  de 8 bits solicitas por el generador. Se reciben los acks por parte del generador
        begin
          nxt_count_bits = count_bits + 1;
          if(count_bits < 8)
            begin
              if (count_trama == 1) nxt_SDA_INr = RD_DATAr [15 - count_bits];
              else nxt_SDA_INr = RD_DATAr[ 7 - count_bits];
            end
          else
            begin
              nxt_SDA_INr = 0;
              nxt_count_trama = count_trama + 1;
              nxt_count_bits = 0;
              if (count_trama == 2) nxt_state = STOP;
            end
        end

      STOP:     // Estado que permite detectar la condicion de STOP enviada por el generador
        begin
          nxt_SDA_INr = 0;
          nxt_count_bits = 0;
          if (SCL & posedge_SDA_OUT)
          begin
            nxt_state = START;
            nxt_count_trama = 0;  
          end
	      end
    
    default: 
      begin
        nxt_state = START;
	    end
    endcase;
  end
endmodule