module generador_I2C(
   // Salidas
   SDA_OUT, SDA_OE, SCL, RD_DATA,
   // Entradas
   CLK, RESET, START_STB, SDA_IN, WR_DATA, I2C_ADDR, RNW
   );

  //Entradas
  input SDA_IN;
  input CLK, RESET, START_STB, RNW;
  input [15:0] WR_DATA;
  input [6:0] I2C_ADDR;

  //Salidas
  output reg SDA_OUT, SDA_OE;
  output     SCL;
  output reg [15:0] RD_DATA;

  // Estados
  localparam START    = 5'b00001;
  localparam ADDRESS  = 5'b00010;
  localparam WRITE    = 5'b00100;
  localparam READ     = 5'b01000;
  localparam STOP     = 5'b10000;

  //Variables internas
  reg [4:0] estado, nxt_estado;
  reg [3:0] cuenta_bits, nxt_cuenta_bits;
  reg [3:0] cuenta_trama, nxt_cuenta_trama;
  reg [8:0] cuenta_CLK;
  reg [15:0] nxt_RD_DATA;

  wire posedge_SCL;
  reg nxt_SDA_OUT, nxt_SDA_OE;

  //SCL es el 25% de la frecuencia de CLK
  assign SCL = (estado == START) ? (1'b1) : (cuenta_CLK[2]);

  //Fip flops 

  //Grupo 1
  always @(posedge CLK) begin
    if (RESET) begin
      estado       <= START;
      cuenta_CLK     <= 0;
    end else begin
      estado        <= nxt_estado;
      cuenta_CLK    <= cuenta_CLK+1;
    end
  end

 // Grupo 2
    always @(posedge SCL) begin
    if (RESET|| START_STB) begin
      cuenta_bits  <= 0;
      cuenta_trama  <= 0;
      SDA_OUT     <= 0;
      SDA_OE      <= 0;

    end else begin
      cuenta_bits   <= nxt_cuenta_bits;
      cuenta_trama   <= nxt_cuenta_trama;
      cuenta_CLK    <= cuenta_CLK+1;
      SDA_OUT      <= nxt_SDA_OUT;
      SDA_OE       <= nxt_SDA_OE;
      RD_DATA      <=  nxt_RD_DATA;
    end
  end
  
// Grupo 3:  Se crean estos FF para poder respetar los tiempos de set up del protocolo
  always @(negedge SCL) begin
    if (RESET|| START_STB) begin
      SDA_OUT     <= 0;
      SDA_OE      <= 0;

    end else begin
      SDA_OUT      <= nxt_SDA_OUT;
      SDA_OE       <= nxt_SDA_OE;
    end
  end

  //Logica combinacional

  always @(*) begin
    nxt_estado = estado;
    nxt_cuenta_bits = cuenta_bits;
    nxt_cuenta_trama = cuenta_trama;
    nxt_SDA_OUT = SDA_OUT;
    nxt_SDA_OE = SDA_OE;
    nxt_RD_DATA = RD_DATA;

    case (estado)
	    START:                           // Estado antes de iniciar una transaccion
        begin
		      SDA_OE = 1'b1;
          SDA_OUT = START_STB;
          RD_DATA <= 16'd0;
		      nxt_cuenta_bits = 0;
          nxt_cuenta_trama = 0;
          if (!START_STB) nxt_estado = ADDRESS;   
	      end

      ADDRESS:                      // En este estado se manda la direccion destino y si se trata de un read/write
        begin
          nxt_cuenta_trama = 0;
          nxt_cuenta_bits = cuenta_bits + 1;
          if (cuenta_bits < 8) 
            begin
              if (cuenta_bits == 7) nxt_SDA_OUT = RNW;
              else nxt_SDA_OUT = I2C_ADDR[6- cuenta_bits];
            end
          else 
            begin 
              nxt_cuenta_trama = cuenta_trama + 1;
              nxt_cuenta_bits = 0;
              nxt_SDA_OE = 1'b0;
              if (RNW) 
                begin
                  nxt_SDA_OUT = 1'b1;
                  nxt_estado = READ;
                end
              else nxt_estado = WRITE;
            end
        end

	    WRITE:                     //  Se procesan dos transacciones  de 8 bits cada una para escritura
        begin
            nxt_cuenta_bits = cuenta_bits + 1;
            if(cuenta_bits < 8)
              begin
                nxt_SDA_OE = 1'b1;
                if (cuenta_trama == 1) nxt_SDA_OUT = WR_DATA [ 15 - cuenta_bits];
                else nxt_SDA_OUT = WR_DATA [ 7 - cuenta_bits];
              end
            else
              begin
                if (cuenta_trama == 2)
                  begin
                    nxt_SDA_OE = 0;
                    nxt_cuenta_trama = cuenta_trama + 1;
                    nxt_cuenta_bits = 0;
                    nxt_SDA_OUT = 1;
                    nxt_estado = STOP;
                  end
                else
                  begin
                    nxt_SDA_OE = 0;
                    nxt_cuenta_trama = cuenta_trama + 1;
                    nxt_cuenta_bits = 0;
                  end
              end
        end

	    READ:              // Se procesan dos transacciones  de 8 bits cada una para lectura
        begin
          nxt_SDA_OE = 0;
          nxt_cuenta_bits = cuenta_bits + 1;
          if(cuenta_bits < 8)
            begin
              if (cuenta_trama == 1) nxt_RD_DATA [ 15 - cuenta_bits] = SDA_IN;
              else nxt_RD_DATA [ 7 - cuenta_bits] = SDA_IN;
              if (cuenta_bits == 7) nxt_SDA_OUT = 0; 
              else nxt_SDA_OUT = 1;
            end
          else
            begin
              if (cuenta_trama == 2)
                begin
                  nxt_SDA_OE = 1;
                  nxt_cuenta_trama = 0;
                  nxt_cuenta_bits = 0;
                  nxt_SDA_OUT = 1;
                  nxt_estado = STOP;
                end
              else
                begin
                  if (cuenta_trama == 1) nxt_SDA_OE = 1;
                  nxt_cuenta_trama = cuenta_trama + 1;
                  nxt_cuenta_bits = 0;
                end
            end
        end

      STOP:                      // Estado que permite recibir el ACK (en caso de escritura) o mandar el ACK (en caso lectura)
        begin
          nxt_cuenta_bits = 0;
          nxt_cuenta_trama = cuenta_trama + 1;
          nxt_SDA_OE = 0;
          if (cuenta_trama == 3)
          begin
            nxt_estado = START;
            nxt_cuenta_trama = 0;  
          end
	      end
    
    default: 
      begin
        nxt_estado = START;
	    end
    endcase;
  end
endmodule