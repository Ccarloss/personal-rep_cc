`include "generador_I2C.v"
`include "receptor_I2C.v"
`include "tester.v"

// Universidad de Costa Rica
// Descripción Condutual del protocolo de comunicacion I2C
// Carlos Andres Cordero Retana B92317
// Fecha: 27/10/2024

module I2C_tb;


  wire CLK, RESET_generador, RESET_receptor, START_STB, RNW;
  wire [15:0] WR_DATA;
  wire [15:0] WR_DATAr;
  wire [6:0] I2C_ADDR;

  wire SDA_INcable;
  wire SDA_OUT, SDA_OE;
  wire SCL;
  wire [15:0] RD_DATA;
  wire [15:0] RD_DATAr;



initial begin
	$dumpfile("I2C_ondas.vcd");
  $dumpvars(-1, U0);
	$dumpvars(-1, U1);
end

generador_I2C U0 (

//Entradas
  .CLK        (CLK), 
  .RESET      (RESET_generador), 
  .START_STB  (START_STB), 
  .RNW        (RNW),
  .SDA_IN     (SDA_INcable),
  .WR_DATA    (WR_DATA),
  .I2C_ADDR   (I2C_ADDR),
//Salidas
  .SDA_OUT    (SDA_OUT),
  .SDA_OE     (SDA_OE),
  .SCL        (SCL),
  .RD_DATA    (RD_DATA)
);


receptor_I2C U1 (

//Salidas
  .SDA_INr     (SDA_INcable),
  .WR_DATAr    (WR_DATAr),
//Entrada
  .CLK        (CLK), 
  .RESET      (RESET_receptor), 
  .SDA_OUT    (SDA_OUT),
  .SDA_OE     (SDA_OE),
  .SCL        (SCL),
  .RD_DATAr    (RD_DATAr)
);


tester U2 (  
  .CLK (CLK), 
  .RESET_generador(RESET_generador), 
  .RESET_receptor(RESET_receptor), 
  .START_STB(START_STB), 
  .RNW(RNW), 
  .WR_DATA(WR_DATA),
  .I2C_ADDR(I2C_ADDR), 
  .RD_DATAr(RD_DATAr), 
  .WR_DATAr(WR_DATAr), 
  .SDA_INcable(SDA_INcable), 
  .SDA_OUT(SDA_OUT), 
  .SDA_OE(SDA_OE), 
  .SCL(SCL), 
  .RD_DATA(RD_DATA)
);

endmodule