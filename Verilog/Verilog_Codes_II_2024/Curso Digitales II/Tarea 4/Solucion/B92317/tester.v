
// Testbench Code Goes here
module tester (  CLK, RESET_generador, RESET_receptor, START_STB, RNW, WR_DATA,
  I2C_ADDR, RD_DATAr, WR_DATAr, SDA_INcable, SDA_OUT, SDA_OE, SCL, RD_DATA);


  output reg CLK, RESET_generador, RESET_receptor, START_STB, RNW;
  output reg [15:0] WR_DATA;
  output reg [6:0] I2C_ADDR;
  output reg [15:0] RD_DATAr;

  input wire [15:0] WR_DATAr;
  input wire SDA_INcable;
  input wire SDA_OUT, SDA_OE;
  input wire SCL;
  input wire [15:0] RD_DATA;



initial begin
  // Entradas al generador
	CLK = 0;
	RESET_generador = 1; 
	RNW = 1'b0;       // ------------------------> Escritura
	I2C_ADDR = 7'b0010001;   // B92317  ----> 17 en binario
	WR_DATA = 16'b1100110010101010;
	START_STB = 1;

	// Entradas al receptor
  RESET_receptor = 1; 
  RD_DATAr = 16'b1011011011011010;

	#18;
	RESET_generador = 0;
  RESET_receptor = 0;
	#2
	START_STB = 0;
  #1870 START_STB = 1;

  #50
  RNW = 1'b1;      // ------------------------> Lectura
	I2C_ADDR = 7'b1100101;    // Se cambia  la direccion para probar funcionalidad

  #200
  START_STB = 0;

  #1870
  START_STB = 1;
  #500
  $finish;
end

always begin
 #5 CLK = !CLK;
end


endmodule