module DUT (
    input SNE,FLAG, reinicio, SNS, Clk, reset,
    input [15:0] PSWD,
    output ON, OFF, ALM1, ALM2
);

reg [2:0] estado_pres;
reg [2:0] count0;

reg [2:0] prox_estado;
reg [2:0] nxt_count0;

// Asignacion de estados

parameter a = 3'b000;
parameter b = 3'b001;
parameter c = 3'b010;
parameter d = 3'b011;
parameter e = 3'b100;

// Memoria de proximo estado

always@ (posedge Clk, posedge reset) 
begin
    if (reset) begin
        estado_pres <= a;
        count0 <= 3'b000;
    end else begin 
        estado_pres <= prox_estado;
        count0 <= nxt_count0;
    end
end


// Logica de calculo de proximo estado

always@(*)
begin

  prox_estado = estado_pres;   //Para garantizar el comportamiento del FF se necesita
  nxt_count0 = count0;         //que por defecto el prox_estado y nxt_count0 sostenga el valor del estado anterior.
 

    case (estado_pres)
        a:  begin                                   // Esperando sensor de que el carro llego               
            nxt_count0 <= 3'b000;
            if (SNE) prox_estado = b;               
            else prox_estado = a;
            end

        b:  if (!FLAG & PSWD == 16'b0010010001101000) prox_estado = b; // Si el usuario no ha ingresado la clave, siga en b
            else
            begin
                nxt_count0 = count0 + 1;                                // Si el usuario recibio la clave,
                if (PSWD == 16'b0010001100010111) prox_estado = d;      // se hacen las siguientes preguntas
                else if(count0+1 == 3'b011) prox_estado = c;
                    else prox_estado = b;
            end 
            
        c:  begin                          // Estado de alarma porque el numero de intentos llego a 3
            nxt_count0 <= 3'b000;
            if (reinicio) prox_estado = b;
            else prox_estado = c;
            end

        d:  begin                           // Esperando a que llegue el sensor de salida
            nxt_count0 <= 3'b000;
            case ({SNS,SNE})
                2'b00 : prox_estado = d;
                2'b01 : prox_estado = e;
                2'b11 : prox_estado = e;
                2'b10 : prox_estado = a;
            endcase
            end
    
        e:  if (reinicio) prox_estado = b;    // Estado de alarma porque se detectaron los dos sensores SNS y SNE
            else prox_estado = e;
        
        default: prox_estado = a;            // Si la maquina cae en un estado indeterminado, empiece de nuevo

    endcase
end


//Logica de calculo de salidas  (Todas las salidas de moore)

assign ON = (estado_pres == d);
assign OFF = (estado_pres == a|estado_pres == b);
assign ALM1 = (estado_pres == c);
assign ALM2 = (estado_pres == e);
    
endmodule