// Universidad de Costa Rica
// Descripción estrutural de un controlador automatizado para la entrada de un estacionamiento
// Autor: Carlos Andres Cordero Retana B92317
// Fecha: 08/09/2024


`timescale 1ns/1ns
`include "tester.v"
`include "cmos_cells.v"
`include "DUT_syn.v"
                              

module testbench_t1;

// Definimos todas las variables como wires para conectar el testbench con el DUT
  wire SNE,FLAG, reinicio, SNS, Clk, reset;
  wire [15:0] PSWD;
  wire ON, OFF, ALM1, ALM2;


  initial begin
    $dumpfile("Ondas.vcd");               // Archivo para simular en gtkwave
    $dumpvars(-1,UUT);                   // Almacene todas las variables del modulo actual y primitivas que cambian
  end

// Conectamos el DUT con el tester  (Se agrega la clave correcta 2317 para hacer overwrite)
 DUT #(.Clave_correcta(16'b0010001100010111)) UUT (
    .SNE(SNE), .FLAG(FLAG) , .reinicio(reinicio), .SNS(SNS) , .Clk(Clk), .reset(reset),
    .PSWD(PSWD),
    .ON(ON), .OFF(OFF), .ALM1(ALM1), .ALM2(ALM2)
  );

 tester probador (
    .SNE(SNE), .FLAG(FLAG) , .reinicio(reinicio), .SNS(SNS) , .Clk(Clk), .reset(reset),
    .PSWD(PSWD),
    .ON(ON), .OFF(OFF), .ALM1(ALM1), .ALM2(ALM2)
  );

endmodule
