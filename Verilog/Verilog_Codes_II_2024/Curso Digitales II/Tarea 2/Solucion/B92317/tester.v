module tester (SNE,FLAG, reinicio, SNS, Clk, reset,
                PSWD,
                ON, OFF, ALM1, ALM2
);


// 1) Definicion de variables
output reg SNE,FLAG, reinicio, SNS, Clk, reset;

output reg [15:0] PSWD;

input ON, OFF, ALM1, ALM2;


reg [19:0] testvectors [25:0];   // Creamos un array de 26 vectores de 20 bits cada uno.

integer puntero;


initial 
    begin
    
// 2) Llenamos la tabla de vectores   (SNE,FLAG, reinicio, SNS, PSWD)

// Prueba 1:  a --> b --> d --> a
testvectors[0] = 20'b1000_0010_0100_0110_1000;               //  Llega carro (cambiamos al estado b), esperando a que ingrese el pin
testvectors[1] = 20'b0100_0010_0011_0001_0111;               //  Se ingresa el pin correcto, intento 1, se cambia al estado d (porton abierto)
testvectors[2] = 20'b0001_0010_0100_0110_1000;               //  Sensor de fin de entrada, se cierra la puerta, volvemos al estado a

// Prueba 2:  a --> b --> b --> d --> a
testvectors[3] = 20'b1000_0010_0100_0110_1000;               //  Llega carro, esperando a que ingrese el pin
testvectors[4] = 20'b0100_0010_0011_0000_0101;               //  Se ingresa el pin incorrecto, volvemos al estado b (Intento 1)
testvectors[5] = 20'b0100_1000_0011_0000_0001;               //  Se ingresa el pin incorrecto, volvemos al estado b (Intento 2)
testvectors[6] = 20'b0100_0010_0011_0001_0111;               //  Se ingresa el pin correcto, pasamos al estado d: porton abierto
testvectors[7] = 20'b0001_0010_0100_0110_1000;               //  Sensor de fin de entrada, se cierra la puerta, volvemos al estado a
//
//// Prueba 3:  a --> b --> b --> b --> c --> b --> d --> a
testvectors[8] = 20'b1000_0010_0100_0110_1000;              // Llega carro, esperando a que ingrese el pin
testvectors[9] = 20'b0100_0010_0011_0000_0100;              //  Se ingresa el pin incorrecto, volvemos al estado b (Intento 1)
testvectors[10] = 20'b0100_1000_0011_0000_0000;              //  Se ingresa el pin incorrecto, volvemos al estado b (Intento 2)
testvectors[11] = 20'b0100_1000_0001_0000_1000;              //  Se ingresa el pin incorrecto, pasamos al estado c (Se activa alarma)
testvectors[12] = 20'b0010_0010_0100_0110_1000;              //  Se recibe la senal de reinicio, cambiamos al estado b               //  
testvectors[13] = 20'b0100_0010_0011_0001_0111;              //  Se ingresa el pin correcto, intento 1, se cambia al estado d (porton abierto)
testvectors[14] = 20'b0001_0010_0100_0110_1000;              //  Sensor de fin de entrada, se cierra la puerta, volvemos al estado a

// Prueba 4:  a --> b --> d --> e --> b --> d --> a
testvectors[15] = 20'b1000_0010_0100_0110_1000;               //  Llega carro, esperando a que ingrese el pin
testvectors[16] = 20'b0100_0010_0011_0001_0111;               //  Se ingresa el pin correcto, pasamos al estado d: porton abierto
testvectors[17] = 20'b1001_0010_0100_0110_1000;               //  Se detectan ambos sensores, cambiamos al estado e: alarma bloqueo
testvectors[18] = 20'b0010_0010_0100_0110_1000;               //  Se recibe la senal de reinicio, cambiamos al estado b
testvectors[19] = 20'b0100_0010_0000_0000_0000;               //  Se recibe clave incorrecta, seguimos en b
testvectors[20] = 20'b0100_0010_0011_0001_0111;               //  Se ingresa el pin correcto, intento 1, se cambia al estado d (porton abierto)
testvectors[21] = 20'b0001_0010_0100_0110_1000;               //  Sensor de fin de entrada, se cierra la puerta, volvemos al esta


// 3) Power up reset

    reset = 1; #17; reset = 0;
    

// 4) Inicializamos el puntero
    puntero = 0;

    end

// 5) Ponemos a correr el reloj
always
    begin
        Clk = 0; #15; Clk = 1; #15;
    end


// 6) Memoria de entradas  (Inyectamos un nuevo vector de entrada en cada flanco decreciente)

always @(negedge Clk) 
    begin
        {SNE,FLAG, reinicio, SNS, PSWD} = testvectors[puntero];
        #3;
        FLAG = 0;
        //debug
        $display("\nVector: %d----> SNE: %b , FLAG: %b , reinicio: %b , SNS: %b, PSWD: %b",puntero,SNE,FLAG, reinicio, SNS, PSWD); 
    end



// 7) Alistamos el nuevo vector durante el flanco positivo para que este listo en el flanco negativo
// Ademas, vemos cual es el valor de la salida obtenido.

always @(posedge Clk)
begin
#1 if (!reset)
    begin
        //debug
        $display ("Salidas-----> Porton abierto: %b , Porton cerrado: %b , Alarma por intentos : %b , alarma por sensores : %b\n", ON, OFF, ALM1, ALM2);
        puntero = puntero + 1;
        if (puntero == 22) begin
            #13;
            $finish;
        end
    end
end

    
endmodule
