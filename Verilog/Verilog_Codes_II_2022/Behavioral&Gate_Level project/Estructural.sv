/* ***********************************************************
                    Universidad de Costa Rica
                 Escuela de Ingenieria Electrica/
                            IE0323 
                    Circuitos Digitales 1

                        Estructural.v

Autor1: Luis Alberto Arias Brenes <luisalberto.arias@ucr.ac.cr>
Autor2: Carlos Andrés Cordero Retana <carlos.corderoretana@ucr.ac.cr>
Fecha: 12/12/21

Descripcion:
  Desarrollo estructural de las conexiones correspondientes a la 
  maquina de estados a partir de los modulos creados en el archivo 
  de subcomponentes
*********************************************************** */

// Archivo con los componentes Flipflop y compuertas logicas
`include "Subcomponentes.sv"

module Estructural(
    input wire W, Y, clk, rst,
    output wire M, D, R);


// Variables Internas 
wire conexion [0:45];

/* Se realizan las conexiones llamando a los modulos, es importante mencionar 
que se va realizando la conexion segun las funciones logicas
obtenidas. 
conexion[4] = A
conexion[5] = A negado
conexion[6] = B
conexion[7] = B negado
conexion[8] = C
conexion[9] = C negado
conexion[15] = D_A
conexion[19] = D_B
conexion[26] = Dc

Ademas al lado de cada una se comenta que forma cada una de las compuertas */

//Logica de proximo estado para FF D_A
AND AND_1 (.A(W), .B(conexion[4]), .C(conexion[10])); //W*A
AND AND_2 (.A(conexion[10]), .B(conexion[9]), .C(conexion[11])); //W*A*C'
AND AND_3 (.A(~W), .B(conexion[6]), .C(conexion[12])); //W'*B
AND AND_4 (.A(conexion[6]), .B(conexion[8]), .C(conexion[13])); //B*C
OR OR_1 (.D(conexion[12]), .E(conexion[13]), .F(conexion[14])); //W'*B + B*C
OR OR_2 (.D(conexion[11]), .E(conexion[14]), .F(conexion[15])); //W'*B + B*C + W*A*C'

//Logica de proximo estado para FF D_B
AND AND_5 (.A(conexion[5]), .B(~Y), .C(conexion[16])); //Y'*A'
AND AND_6 (.A(Y), .B(conexion[9]), .C(conexion[17])); //Y*C'
OR OR_3 (.D(conexion[16]), .E(conexion[17]), .F(conexion[18])); //Y'*A' + Y*C'
OR OR_4 (.D(conexion[18]), .E(conexion[7]), .F(conexion[19])); //Y'*A' + Y*C' + B'


//Logica de proximo estado para FF D_C
AND AND_7 (.A(W), .B(conexion[7]), .C(conexion[20])); //W*B'
AND AND_8 (.A(conexion[5]), .B(W), .C(conexion[21])); //W*A'
AND AND_9 (.A(Y), .B(conexion[6]), .C(conexion[22])); //Y*B
AND AND_10 (.A(conexion[22]), .B(conexion[4]), .C(conexion[23])); //Y*A*B
OR OR_5 (.D(conexion[20]), .E(conexion[21]), .F(conexion[24])); //WB' + WA'
OR OR_6 (.D(conexion[23]), .E(conexion[24]), .F(conexion[25]));//Y*A*B + WB' + WA'
OR OR_7 (.D(conexion[8]), .E(conexion[25]), .F(conexion[26]));//Y*A*B + WB' + WA' + C

// Memoria de estados
FLipFLopA_D bit_A(.D(conexion[15]), .clk(clk), .reset(rst), .Q(conexion[4]), .Q_neg(conexion[5]));
FLipFLopA_D bit_B(.D(conexion[19]), .clk(clk), .reset(rst), .Q(conexion[6]), .Q_neg(conexion[7]));
FLipFLopA_D bit_C(.D(conexion[26]), .clk(clk), .reset(rst), .Q(conexion[8]), .Q_neg(conexion[9]));

//Logica de salidas:
//Salida M 
AND AND_11 (.A(conexion[4]), .B(conexion[8]), .C(conexion[27])); //A*C
AND AND_12 (.A(conexion[6]), .B(conexion[9]), .C(conexion[28])); //B*C'
OR OR_8 (.D(conexion[27]), .E(conexion[28]), .F(M));//A*C + B*C'


//Salida RN
AND AND_13 (.A(conexion[4]), .B(conexion[6]), .C(R)); //A*B

//Salida D
AND AND_14 (.A(~W), .B(conexion[7]), .C(conexion[31])); //W'*B'
AND AND_15 (.A(conexion[31]), .B(conexion[9]), .C(conexion[32])); //W'*B'*C'
AND AND_16 (.A(~W), .B(conexion[5]), .C(conexion[33])); //W'*A'
AND AND_17 (.A(conexion[33]), .B(conexion[9]), .C(conexion[34])); //W'*A'*C'
AND AND_18 (.A(Y), .B(conexion[5]), .C(conexion[35])); //Y*A'
AND AND_19 (.A(conexion[35]), .B(conexion[8]), .C(conexion[36])); //Y*A'*C
OR OR_9 (.D(conexion[32]), .E(conexion[34]), .F(conexion[37]));// W'*B'*C' + W'*A'*C'
OR OR_10 (.D(conexion[37]), .E(conexion[36]), .F(D));// W'*B'*C' + W'*A'*C' + Y*A'*C


endmodule
