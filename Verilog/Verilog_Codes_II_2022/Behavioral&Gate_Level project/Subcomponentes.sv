/* ***********************************************************
                    Universidad de Costa Rica
                 Escuela de Ingenieria Electrica/
                            IE0323 
                    Circuitos Digitales 1

                        Subcomponentes.v

Autor1: Luis Alberto Arias Brenes <luisalberto.arias@ucr.ac.cr>
Autor2: Carlos Andrés Cordero Retana <carlos.corderoretana@ucr.ac.cr>
Fecha: 12/12/21

Descripcion:
  En el archivo se crean los subcomponentes necesarios para ejecutar 
   el modulo estructural, estos componentes son compuertas lógicas AND 
   y OR además de un Flipflop tipo D
*********************************************************** */

/* Modulo de una compuerta AND
Entradas: - A
          - B
Salida: -C 
La salida es una variable tipo wire que 
tiene la operacion logica AND. */

module AND (
            input wire A, B, 
            output wire C);

//Asignacion de la salida
assign C = A & B; 

endmodule
 


/* Modulo OR
Entradas: -D
          -E
Salida: -F 
La salida es el resultado de la operación lógica 
correspondiente a la compuerta or, variables tipo wire */
module OR(
        input wire D, E,
        output wire F); 

assign F = D | E;  

endmodule 


/* Modulo FlipFlop_D
Es un FlipFlop tipo D transparente, lo que e
entra en D sale por Q, con reset de activación 
en estado bajo y activo en el flanco creciente 
de reloj 
Entradas tipo wire y salidas tipo reg que se 
pueden inicializar */

module FLipFLopA_D(
              input wire D, 
              input wire clk, reset, 
              output reg Q, Q_neg);

	always @(posedge clk, negedge reset) 
	begin

	//Asignaciones no bloqueantes, pasa al mismo tiempo todos los datos
        if (!reset) Q <= 1'b0; 
        else Q <= D;
	     Q_neg <= ~D; 
	end

        // Inicializacion del FlipFlop 
        initial 
        begin
               Q = 0;
               Q_neg = 1; 
        end

endmodule
//**************************
module FLipFLopB_D(
              input wire D, 
              input wire clk, reset, 
              output reg Q, Q_neg);

	always @(posedge clk, negedge reset) 
	begin

	//Asignaciones no bloqueantes, pasa al mismo tiempo todos los datos
        if (!reset) Q <= 1'b1; 
        else Q <= D;
	     Q_neg <= ~D; 
	end

        // Inicializacion del FlipFlop 
        initial 
        begin
                Q = 1;
               Q_neg = 0; 
        end

endmodule


module FLipFLopi_D(
              input wire D, 
              input wire clk, reset, 
              output reg Q, Q_neg);

	always @(negedge clk, negedge reset) //Memoria de entradas, se activan en estado bajo pasan
	begin

	//Asignaciones no bloqueantes, pasa al mismo tiempo todos los datos
        if (!reset) Q <= 1'b0; 
        else Q <= D;
	     Q_neg <= ~D; 
	end

        // Inicializacion del FlipFlop 
        initial 
        begin
               Q = 0;
               Q_neg = 1; 
        end

endmodule

