/* ***********************************************************
                    Universidad de Costa Rica
                 Escuela de Ingenieria Electrica/
                            IE0323 
                    Circuitos Digitales 1

                        Testbench.v

Autor1: Luis Alberto Arias Brenes <luisalberto.arias@ucr.ac.cr>
Carlos Andrés Cordero Retana <carlos.corderoretana@ucr.ac.cr>
Fecha: 12/12/21

Descripcion:
  Probador al que se conectan los datos del testbench para 
  visualizar los resultados obtenidos
*********************************************************** */

// Escala de tiempo 
`timescale 1ns/1ns

// Se incluyen los archivos necesarios para hacer las pruebas
`include "Conductual.sv"
`include "Estructural.sv"
`include "Testbench.sv"


module Probador(); //Es una interfaz, no tiene entradas ni salidas
    wire clk, rst, W, Y;
    wire D, R, M; 

//Instanciamos el modulo de interes
/*
conductual uut( .clk(clk), .rst(rst), .W(W), .Y(Y), 
                .D(D), .R(R), .M(M)); // Unidad a evaluar
Testbench maqtest(.clk(clk), .rst(rst), .W(W), .Y(Y), 
                .D(D), .R(R), .M(M)); // Conexion con el banco de pruebas */


Estructural unt( .clk(clk), .rst(rst), .W(W), .Y(Y), 
                .D(D), .R(R), .M(M)); // Unidad a evaluar
Testbench estest(.clk(clk), .rst(rst), .W(W), .Y(Y), 
                .D(D), .R(R), .M(M)); // Conexion con el banco de pruebas 

endmodule 