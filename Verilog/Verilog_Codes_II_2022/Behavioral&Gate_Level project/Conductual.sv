/* ***********************************************************
                    Universidad de Costa Rica
                 Escuela de Ingenieria Electrica/
                            IE0323 
                    Circuitos Digitales 1

                        Conductual.v

Autor1: Luis Alberto Arias Brenes <luisalberto.arias@ucr.ac.cr>
Autor2: Carlos Andrés Cordero Retana <carlos.corderoretana@ucr.ac.cr>
Fecha: 12/12/21

Descripcion:
  Se desarrolla el código para una descripción RTL conductual 
  de una máquina de estados a partir de su funcionamiento en el
  diagrama ASM.
  
*********************************************************** */

module conductual ( input clk, rst,
                     input W, Y,
                     output D,R,M );

// Variables internas para la máquina
reg [2:0] EstPres, ProxEstado;


//Asignación de estados
parameter a =  3'b010;
parameter b =  3'b110;
parameter c =  3'b011;
parameter d =  3'b111;
parameter e =  3'b100;
parameter f =  3'b101;

/*Memoria de estados: 
Representa el funcionamiento de FlipFlops
que se activan en el flanco creciente de reloj 
y con un reset asinconico en estado bajo. Si 
se activa el reset vuelve al estado a, sino se 
activa el reset, se pasa al siguiente estado.
Bloque always: Bloque de sensibilidad, cada vez 
haya un cambio en clk o rst se actualizan los valores */
always @(posedge clk, negedge rst) 
    begin
    if (!rst) EstPres  <=   a;
    else     EstPres  <=   ProxEstado;
    end

//Lógica de cáculo de próximo estado
always @(*) 
    begin
        case (EstPres)
        a:  if(W) ProxEstado = c;
            else  ProxEstado = b;
        
        b:  if(Y) ProxEstado = d;
            else  ProxEstado = e;

        c:  if(Y) ProxEstado = f;
            else  ProxEstado = d;

        d:   ProxEstado = f;
             
        
        e:  if(W) ProxEstado = d;
            else  ProxEstado = a;

        f:  ProxEstado = c;
        endcase
        
      end


//Lógica de cálculo de salidas
assign M = (  (EstPres == a) |  (EstPres == b) | (EstPres==d) | (EstPres==f)  );


assign R = ~((EstPres == a) | (EstPres == c)  |  (EstPres == e) | (EstPres == f) );

assign D = ( ((EstPres==a)&~W) |  ((EstPres==c)&Y)  | ((EstPres==e)&~W)     );    
   

endmodule