/* ***********************************************************
                    Universidad de Costa Rica
                 Escuela de Ingenieria Electrica/
                            IE0323 
                    Circuitos Digitales 1

                        Testbench.v

Autor1: Luis Alberto Arias Brenes <luisalberto.arias@ucr.ac.cr>
Carlos Andrés Cordero Retana <carlos.corderoretana@ucr.ac.cr>
Fecha: 12/12/21

Descripcion:
  Se presenta el codigo del banco de pruebas Testbench para la máquina de estados, 
  en este se implementa el código necesario para poder ejecutar las secuencias 
  que demuestren un correcto funcionamiento
*********************************************************** */

// Escala de tiempo para la simulacion 
`timescale 1ns/1ns

/* Modulo Testbench
Las entradas de los modulos conducutal y estructural 
se definen como salidas y las salidas como entradas 
para este modulo, variables de entrada tipo reg para 
poder asignarles valores */

module Testbench(
            output reg clk, rst,
            output reg W, Y,
            input wire D, R, M);

//Variables internas
reg [1:0] testvectors [6:0]; /* Arreglo de 7 vectores con 2bits cada uno, dado que le asignaremos valores
                              a las entradas W y Y   */

integer vectornum;  /* es la variable que recorrer
                    la secuencia de vectores de entrada que se crea seguidamente */

// Inicializacion de variables 
initial                                          
    begin
    vectornum=0;

    rst= 0;
    #2 rst =1;

     
  /*                                    //SECUENCIA 1

    // La secuencia de estados es: a --> c --> f --> c --> d --> rst

    $dumpfile ("Secuencia1.vcd");
    $dumpvars;  // Almacena las variables del programa.

    testvectors[0]= 2'b10;    //Pasamos de a al estado c            
    
    testvectors[1]= 2'b01;    //Pasamos al estado f
    
    testvectors[2]= 2'b00;    // No importa los  valores entrada, pasamos directamente al estado c 
    
    testvectors[3]= 2'b00;    //pasamos al estado d

    end

// Actualizacion de vector de entrada en flanco positivo de reloj, 
always @(posedge clk) 
 

   if (vectornum==3)
      begin
     #2 rst=0; //Activacion del reset en medio ciclo de reloj en estado d
     #4 $finish;
      end
else
    if(rst)
    begin
        $display ("Las salidas serian D=%b R=%b M=%b",D, R, M);
        vectornum= vectornum+1;
    end       


*/



/*
                                      //SECUENCIA 2

    $dumpfile ("Secuencia2.vcd");
    $dumpvars;

    testvectors[0]= 2'b00;    //Pasamos al estado b            
    
    testvectors[1]= 2'b01;    //Pasamos al estado d
    
    testvectors[2]= 2'b10;    // No importa los  valores entrada, pasamos directamente al estado f
    
    testvectors[3]= 2'b00;    

    end

// Actualizacion del vector de entrada en flanco positivo de reloj
always @(posedge clk) 

if(rst)
    begin
        $display ("Las salidas serian D=%b R=%b M=%b",D, R, M);
        vectornum= vectornum+1;
            if (vectornum==4)
                    #4 $finish;  
    end


*/




                                      //SECUENCIA 3
  
    $dumpfile ("Secuencia3.vcd");
    $dumpvars;

    testvectors[0]= 2'b00;    //Pasamos al estado b            
    
    testvectors[1]= 2'b00;    //Pasamos al estado e
    
    testvectors[2]= 2'b00;    // Volvemos al estado a
    
    end


    // Actualizacion de vector de entrada en flanco positivo de reloj
    always @(posedge clk) 
if(rst)
    begin
        $display ("Las salidas serian D=%b R=%b M=%b",D, R, M);
        vectornum= vectornum+1;
            if (vectornum==3)
                    #4 $finish;  
    end


// Definicion del reloj 
always 
        begin
            clk= 1; #4; clk=0; #4;
        end  

 // Bloque para leer las entradas en flanco decreciente
always @(negedge clk) 
    begin
    {W,Y}= testvectors[vectornum];  
    $display;
    $display ("Las entradas son W=%b Y=%b", W,Y);
    end

    
endmodule