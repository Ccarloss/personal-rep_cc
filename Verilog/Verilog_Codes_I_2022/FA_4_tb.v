/******************************************************

Nombre del archivo:  FA_4.v

Autor: Carlos Andrés Cordero Retana <correo: carlos.corderoretana@ucr.ac.cr>

Fecha:  2/06/2023

Descripcion: El siguiente programa es un test bench de un sumador completo de 4bits (Full adder)
usando structural design

******************************************************/
`include "FA_4.v"
`timescale 1ns/1ps
module F4_tb (
);

reg [0:3]a;
reg [0:3]b;
reg C0;
wire C4;
wire [3:0]S;

FA_4 FA4_1(S,C4,a,b,C0);

initial
begin
    a = 4'b0000; b =4'b0000; C0=0;
    #20;
    a = 4'b0000; b =4'b0100;
    #20;
    a = 4'b0110; b = 4'b0100;
    #20;
    a = 4'b1111; b = 4'b1111;
    #20;
end
initial
begin
    $monitor("Al sumar %b + %b = %b%b ",a,b,C4,S);
end

    
endmodule