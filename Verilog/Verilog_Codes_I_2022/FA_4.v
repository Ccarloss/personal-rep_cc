/******************************************************

Nombre del archivo:  FA_4.v

Autor: Carlos Andrés Cordero Retana <correo: carlos.corderoretana@ucr.ac.cr>

Fecha:  2/06/2023

Descripcion: El siguiente programa consiste en hacer un sumador completo de 4bits (Half adder)
usando structural design

******************************************************/
`include "FA_S.v"
module FA_4 (S,C4,a,b,C0
);
input C0;
output C4;
wire [2:0]C; //Variable interna
output wire [3:0]S;
input wire [3:0]a;
input wire [3:0]b;

FA_S FA1(S[0],C[0],a[0],b[0],C0 );

FA_S FA2(S[1],C[1],a[1],b[1],C[0] );

FA_S FA3(S[2],C[2],a[2],b[2],C[1] );

FA_S FA4(S[3],C4,a[3],b[3],C[2] );

    
endmodule