/******************************************************

Nombre del archivo:  FA_S.v

Autor: Carlos Andrés Cordero Retana <correo: carlos.corderoretana@ucr.ac.cr>

Fecha:  2/06/2023

Descripcion: El siguiente programa constituye un test bench de un sumador completo de 1bit (Full adder)
usando structural design

******************************************************/
`timescale 1ns/1ps
`include "FA_S.v" 

module FA_tb (
);
reg a,b,Ci;
wire S,Co;

FA_S FA1(S,Co, a,b,Ci);

initial
begin
    a=0 ; b=0; Ci=0;
    #20;
    a=0 ; b=1; Ci=0;
    #20;
    a=1 ; b=0; Ci=0;
    #20;
    a=1 ; b=1; Ci=0;
    #20;
    a=0 ; b=0; Ci=1;
    #20;
    a=0 ; b=1; Ci=1;
    #20;
    a=1 ; b=0; Ci=1;
    #20;
    a=1 ; b=1; Ci=1;
end

initial
begin
$monitor("The result Ci+a+b= %b+%b+%b of the sum is %b%b",Ci,a,b,Co,S);
end



endmodule