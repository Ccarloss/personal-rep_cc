/******************************************************

Nombre del archivo:  FA_S.v

Autor: Carlos Andrés Cordero Retana <correo: carlos.corderoretana@ucr.ac.cr>

Fecha:  2/06/2023

Descripcion: El siguiente programa consiste en hacer un sumador completo de 1bt (Half adder)
usando structural design

******************************************************/
`include "Gates.v"
module FA_S (S,Co, a,b,Ci
);
output S,Co;
input a,b,Ci;
//intern variables
wire o1,A1,A2,A3,o12;

//S= a^b^Ci
XOR XOR1(o1,a,b);
XOR XOR2(S,Ci,o1); 

//Co = ab + Cia +Cib
AND AND1(A1, a,b);
AND AND2(A2, Ci,b);
AND AND3(A3, Ci,a);

OR OR1(o12, A1,A2);
OR OR2(Co, o12,A3);

   
endmodule