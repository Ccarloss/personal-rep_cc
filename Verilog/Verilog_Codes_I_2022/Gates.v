/******************************************************

Nombre del archivo:  Gates.v

Autor: Carlos Andrés Cordero Retana <correo: carlos.corderoretana@ucr.ac.cr>

Fecha:  2/06/2023

Descripcion: Se muestran los modulos de diferentes compuertas

******************************************************/

module AND(o,a,b);
input a,b;
output o;

assign o = a&b;

endmodule


module OR(o,a,b);
input a,b;
output o;

assign o = a|b;

endmodule


module XOR(o,a,b);
input a,b;
output o;

assign o = a^b;

endmodule

module NAND(o,a,b);
input a,b;
output o;

assign o = ~(a&b);

endmodule

module NOR(o,a,b);
input a,b;
output o;

assign o = ~(a|b);

endmodule

module NOT(o,a);
input a;
output o;

assign o = ~a;

endmodule

